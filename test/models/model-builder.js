/* globals describe, it */

"use strict";

var expect = require('expect.js');
var ModelBuilder = require('../../server/lib/persistence/models/model-builder');

function CustomType() {

}

function testProperty() {
	it('should throw an error when omitting a required property', function() {
		var Model = new ModelBuilder('Test')
			.property(
				'numberProperty',
				{
					type: Number,
					required: true
				}
			)
			.build();

		expect(function() {
			new Model({
				numberProperty: undefined
			});
		}).to.throwException(/^The following properties are required for the Test model, but did not have values: /);
	});

	it('should not throw an error when including all required properties', function() {
		var Model = new ModelBuilder('Test')
			.property(
				'numberProperty',
				{
					type: Number,
					required: true
				}
			).property(
				'stringProperty',
				{
					type: String,
					required: true
				}
			).build();

		expect(function() {
			new Model({
				numberProperty: 2,
				stringProperty: 'test'
			});
		}).to.not.throwException();
	});

	it('should throw an error when assigning a non-number to a number property', function() {
		var Model = new ModelBuilder('Test')
			.property(
				'numberProperty',
				{
					type: Number
				}
			)
			.build();

		expect(function() {
			new Model({
				numberProperty: false
			});
		}).to.throwException(TypeError);
	});

	it('should throw an error when assigning a non-string to a string property', function() {
		var Model = new ModelBuilder('Test')
			.property(
				'stringProperty',
				{
					type: String
				}
			)
			.build();

		expect(function() {
			new Model({
				stringProperty: false
			});
		}).to.throwException(TypeError);
	});

	it('should throw an error when assigning a non-Date to a date property', function() {
		var Model = new ModelBuilder('Test')
			.property(
				'dateProperty',
				{
					type: String
				}
			)
			.build();

		expect(function() {
			new Model({
				dateProperty: false
			});
		}).to.throwException(TypeError);
	});

	it('should throw an error when assigning an incorrect type to a custom type property', function() {
		var Model = new ModelBuilder('Test')
			.property(
				'customProperty',
				{
					type: CustomType
				}
			)
			.build();

		expect(function() {
			new Model({
				customProperty: false
			});
		}).to.throwException(TypeError);
	});

	it('should not throw an error when assigning a correct type to a custom type property', function() {
		var Model = new ModelBuilder('Test')
			.property(
				'customProperty',
				{
					type: CustomType
				}
			)
			.build();

		expect(function() {
			new Model({
				customProperty: new CustomType()
			});
		}).to.not.throwException(TypeError);
	});
}

function testComputedProperty() {
	var firstName = 'Test';
	var lastName = 'Person';
	var fullName = firstName + ' ' + lastName;

	var Model = new ModelBuilder('Test')
		.property(
			'firstName',
			{
				type: String,
				required: true
			}
		).property(
			'lastName',
			{
				type: String,
				required: true
			}
		).computedProperty(
			'fullName',
			{
				getter: function() {
					return this.firstName + ' ' + this.lastName;
				}
			}
		).build();

	var instance = new Model({
		firstName: firstName,
		lastName: lastName
	});

	it('should have a computedProperty that returns the result of the getter', function() {
		expect(instance.fullName).to.be(fullName);
	});
}

describe('ModelBuilder', function() {
	it('should throw an error when initialized without a name', function() {
		expect(function() {
			new ModelBuilder();
		}).to.throwException(/^Must specify a name for the model$/);
	});

	it('should have a .toString() that prints its specified name in the standard [object <name>Model] format', function() {
		var Model = new ModelBuilder('Test')
			.build();

		var instance = new Model();

		expect(instance.toString()).to.be('[object TestModel]');
	});

	describe('property()', testProperty);
	describe('computedProperty()', testComputedProperty);
});
