/* globals describe, it */

"use strict";

var FacebookSyncClient = require('../server/lib/facebook-sync');
var expect = require('expect.js');

describe('FacebookSyncClient',
	function() {
		describe('getPosts',
			function() {
				it('should return a list of posts in the first week of December 2013',
					function(done) {
						FacebookSyncClient.instance.getPosts({
							start: new Date(2013, 12, 1),
							end: new Date(2013, 12, 7)
						}).done(
							function(posts) {
								expect(posts).to.not.be.empty();
								done();
							}
						);
					}
				);
			}
		);
	}
);
