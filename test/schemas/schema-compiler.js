/* globals describe, it, before, beforeEach, after, afterEach */

"use strict";

var path = require('path');
var expect = require('expect.js');
var fs = require('q-io/fs');
var Q = require('q');

var SchemaCompiler = require('../../server/lib/persistence/schemas/schema-compiler');


var compiler = new SchemaCompiler();

var inputPath = path.resolve(__dirname, 'test-schema-files/input');
var outputPath = path.resolve(__dirname, 'test-schema-files/expected');

function getPaths(testName) {
	return {
		input: path.join(inputPath, testName + '.json'),
		output: path.join(outputPath, testName + '.sql')
	};
}

function runGenerateTest(testName, description) {
	var paths = getPaths(testName);

	it(description, function(done) {
		Q.all([
			compiler.generateFromFile(paths.input),
			fs.read(paths.output, { encoding: 'utf-8' })
		]).spread(
			function(compiled, expected) {
				expect(compiled).to.be(expected);
				done();
			}
		).done(function() {
		});
	});
}

describe('SchemaCompiler', function() {
	describe('generate', function() {
		[
			{
				name: "column-types",
				description: "should generate a DDL script containing each of the column types"
			},
			{
				name: "columns-with-defaults",
				description: "should generate a DDL script containing columns with defaults"
			}
		].forEach(function(spec) {
			runGenerateTest(spec.name, spec.description);
		});
	});
});
