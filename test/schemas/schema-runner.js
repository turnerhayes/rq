/* globals describe, it, before, after, beforeEach, afterEach */

"use strict";

var Q = require('q');

var SchemaRunner = require('../../server/lib/persistence/schemas/schema-runner');

var dbConnectionString = 'postgres://tester:tester_pass@localhost/rq_test';

var knex = require('knex')({
	client: 'pg',
	connection: dbConnectionString,
	debug: true
});

function clearTestDatabase() {
	/* jshint newcap: false */

	console.log('cleaning up');
	Q(knex.select('table_name').from('information_schema.tables').where('table_schema', 'public'))
		.done(
			function(results) {
				console.log('rows: ', results);
			}
		);
}
