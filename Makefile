SELF_DIR := $(realpath $(dir $(lastword $(MAKEFILE_LIST))))

TESTS = $(shell find $(SELF_DIR)/test -name "*.js" -not -path "*/node_modules/*")

SCHEMA_TESTS_DIRECTORY = $(SELF_DIR)/test/schemas

MODEL_TESTS_DIRECTORY = $(SELF_DIR)/test/models

MOCHA_COMMAND = mocha

.PHONY: test install-client install-server

test:
	$(MAKE) test-server
	$(MOCHA_COMMAND) --recursive $(filter-out $(SELF_DIR)/test/node_modules/**, $(SELF_DIR)/test/**/*.js)

test-server:
	cd $(SELF_DIR)/server && npm test

test-schemas:
	$(MOCHA_COMMAND) $(SCHEMA_TESTS_DIRECTORY)/**/*.js

test-models:
	$(MOCHA_COMMAND) $(MODEL_TESTS_DIRECTORY)/**/*.js

install-client:
	cd $(SELF_DIR)/client && npm install

install-server:
	cd $(SELF_DIR)/server && npm install
