/* globals describe, it */

"use strict";

var expect = require('expect.js');
var qhttp  = require('q-io/http');
var _      = require('lodash');
var testUsers = require('../test-data/facebook-users.json');

var siteURL = 'http://localhost:3000';

var testData = {
	single: [
		testUsers[0]
	],
	multiple: testUsers
};

function makeRequest(data, done) {
	qhttp.request(
		{
			url: siteURL + '/users',
			method: 'POST',
			body: [
				JSON.stringify(data)
			],
			headers: {
				'Content-type': 'application/json',
				'Accept': 'application/json'
			}
		}
	).then(
		function(response) {
			return response.body.read().then(
				function(bodyString) {
					return {
						response: response,
						body: bodyString.toString('utf-8')
					};
				}
			);
		}
	).then(
		function(responseData) {
			var parsedBody;
			
			try {
				parsedBody = JSON.parse(responseData.body);
			}
			catch(ex) {}

			if (responseData.response.status >= 200 && responseData.response.status < 300) {
				if (!_.isUndefined(parsedBody) && _.size(parsedBody) === 0) {
					done();
					return;
				}
			}

			var error;

			if (parsedBody && parsedBody.error) {
				error = new Error(parsedBody.error.message);
				error.stack = parsedBody.error.stack;
			}
			else {
				error = new Error(responseData.body);
			}

			done(error);
		},
		function(responseData) {
			done(responseData.body);
		}
	);
}

describe('REST Endpoints', function() {
	describe('Users', function() {
		describe('Upload Users', function() {
			describe('One user', function() {
				it('Should insert nothing when trying to insert users that already have accounts (with their Facebook ID)', function(done) {
					makeRequest(testData.single, done);
				});
			});

			describe('Multiple users', function() {
				it('Should insert nothing when trying to insert users that already have accounts (with their Facebook ID)', function(done) {
					makeRequest(testData.multiple, done);
				});
			});
		});
	});
});
