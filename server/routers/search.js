"use strict";

var BaseRouter     = require('./base');
var Manchu         = require('../lib/manchu');
var PostsStore     = require('../lib/persistence/stores/posts');

function searchForm(req, res) {
	Manchu.getPartial('search/form').then(
		function(partial) {
			BaseRouter.renderIndex(
				req,
				res,
				{
					content: {
						middlePanel: partial({
							_req: {
								query: req.query
							}
						})
					}
				}
			);
		}
	);
}

function search(req, res) {
	var options;

	if ('search' in req.query) {
		// Search has been submitted
		options = {};

		if (req.query.searchTerm) {
			options.searchTerm = req.query.searchTerm;
		}

		if (req.query.postedBy) {
			options.postedBy = req.query.postedBy;
		}

		if (req.query.startDate) {
			options.startDate = req.query.startDate;
		}

		if (req.query.endDate) {
			options.endDate = req.query.endDate;
		}

		if (req.query.includeQuestions === 'true') {
			options.includeQuestions = true;
		}

		if (req.query.includeComments === 'true') {
			options.includeComments = true;
		}

		PostsStore.instance.searchPosts(options).done(
			function(posts) {
				res.json(posts);
			}
		);
	}
	else {
		searchForm(req, res);
	}
}

var router = new BaseRouter();

router.restrictedRoute(
	{
		loggedIn: true
	},
	'/'
).get(search);

exports = module.exports = router;
