"use strict";

var _                  = require('lodash');
var BaseRouter         = require('./base');
var NotificationsStore = require('../lib/persistence/stores/notifications');

function getUserNotifications(req, res) {
	var userId = req.param('userId') || (req.user ? req.user.id : undefined);
	var shallow = req.query.full === 'false';
	var unseenOnly = req.query.unseen === 'true';

	NotificationsStore.instance.getUserNotifications({
		userId: userId,
		full: !shallow,
		unseenOnly: unseenOnly
	}).then(
		function(notifications) {
			res.json(notifications);
		}
	).done(
		undefined,
		BaseRouter.renderError.bind(undefined, req, res)
	);
}

function markNotificationsSeen(req, res) {
	var notificationIds = _.map(
		req.param('ids'),
		function(id) {
			return parseInt(id, 10);
		}
	);

	NotificationsStore.instance.markNotificationsSeen({
		ids: notificationIds
	}).done(
		function() {
			res.send('');
		}
	);
}

var router = new BaseRouter();

router.restrictedRoute(
	{
		loggedIn: true
	},
	'/'
).get(getUserNotifications);

router.restrictedRoute(
	{
		loggedIn: true
	},
	'/mark-seen'
).patch(markNotificationsSeen);

router.restrictedRoute(
	{
		loggedIn: true
	},
	'/:userId'
).get(getUserNotifications);

exports = module.exports = router;
