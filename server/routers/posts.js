"use strict";

var _                  = require('lodash');
var q                  = require('q');
var BaseRouter         = require('./base');
var Manchu             = require('../lib/manchu');
var SocketManager      = require('../lib/socket-manager');
var PostModel          = require('../lib/persistence/models/post');
var PostsStore         = require('../lib/persistence/stores/posts');
var NotificationsStore = require('../lib/persistence/stores/notifications');

function _getPost(req, res, options) {
	options = options || {};

	var user = options.user;

	delete options.user;
	
	options.full = req.query.full === 'true';
	options.structured = req.query.structured !== 'false';

	// assume that HTML wants the full post (including children)
	if (req.accepts(['application/json', 'text/html']) === 'text/html') {
		options.full = true;
		options.structured = true;
	}

	if (options.structured)	{
		options.full = true;
	}

	var latest = options.latest;

	delete options.latest;

	var postPromise = latest ?
		PostsStore.instance.getLatestPost(options) :
		PostsStore.instance.getPost(options);

	postPromise.done(function(post) {
		if (!post) {
			res.status(404);
			res.json({error: {message: 'No such question'}});
			return;
		}

		function preparePostForRender(postObject) {
			var json = postObject.toJSON({
				postOnly: true
			});

			json.isLikedByUser = postObject.isLikedByUser(user);

			if (postObject.children && postObject.children.length > 0) {
				json.children = json.children || [];

				_.each(postObject.children, function(child) {
					json.children.push(preparePostForRender(child));
				});
			}

			return json;
		}

		var postJSON = _.isArray(post) ? _.map(post, preparePostForRender) : preparePostForRender(post);

		res.format({
			'application/json': function() {
				res.json(postJSON);
			},

			'text/html': function() {
				q.all(
					[
						Manchu.getPartial('questions/question-detail'),
						NotificationsStore.instance.getUserNotifications({
							userId: user.id
						})
					]
				).spread(
					function(
						partial,
						notifications
					) {
						BaseRouter.renderIndex(
							req,
							res,
							{
								notifications: notifications,
								header: {
									options: {
										showPostNavigationLinks: true,
										postNavigation: {
											previous: 1
										}
									}
								},
								content: {
									middlePanel: partial({
										question: postJSON,
										renderedOnServer: true,
										currentUser: req.user
									})
								}
							}
						);
					},
					BaseRouter.renderError.bind(undefined, req, res)
				).done();
			}
		});
	});
}

function askQuestion(req, res) {
	Manchu.getPartial('questions/ask').then(
		function(
			partial
		) {
			BaseRouter.renderIndex(
				req,
				res,
				{
					viewClasses: {
						middlePanel: 'questions/ask'
					},
					content: {
						middlePanel: partial()
					}
				}
			);
		}
	);
}

function getPosts(req, res) {
	var limit = req.param('limit');
	var fromID = req.param('fromID');
	var full = req.param('full') === 'true';
	var postedBy = req.query.postedBy;

	var options = {
		limit: limit,
		minID: fromID,
		full: full
	};

	if (!_.isUndefined(postedBy)) {
		options.filters = {
			postedBy: postedBy
		};
	}

	PostsStore.instance.getPosts(options).then(
		function(results) {
			res.json(results);
		}
	).done(
		undefined,
		BaseRouter.renderError.bind(undefined, req, res)
	);
}

function getPost(req, res) {
	var id = req.param('id');

	_getPost(req, res, {
		id: id,
		user: req.user
	});
}

function getLatestPost(req, res) {
	_getPost(req, res, {
		latest: true,
		user: req.user
	});	
}

function addPost(req, res) {
	var path = req.body.path;
	var body = req.body.body;
	var isComment = !!path;

	var postedBy = req.body.postedBy === null ? null : req.user;

	var post = new PostModel({
		path: path,
		postedBy: postedBy,
		body: body
	});

	PostsStore.instance.addPost(post).then(
		function(post) {
			if (isComment) {
				NotificationsStore.instance.notifyComment({
					post: post
				});
			}

			res.status(201);
			res.location('/posts/' + post.id);
			res.json(post);
		}
	).done(
		undefined,
		BaseRouter.renderError.bind(undefined, req, res)
	);
}

function addFacebookPosts(req, res) {
	var posts = req.body;

	_.forEach(
		posts,
		function(post) {
			post.postedBy = req.user;
		}
	);

	PostsStore.instance.addPosts(posts).then(
		function() {
			
		}
	);

	res.send(posts);
}

function deletePost(req, res) {
	PostsStore.instance.deletePost({
		id: req.param('id')
	}).done(
		function() {
			res.status(200);
			res.end();
		},
		BaseRouter.renderError.bind(undefined, req, res)
	);
}

function updatePost(req, res) {
	// TODO: implement
	res.end();
}

function likePost(req, res) {
	var postId = req.param('postId');

	PostsStore.instance.likePost({
		postID: postId,
		user: req.user
	}).then(
		function(likeData) {
			NotificationsStore.instance.notifyLike({
				postId: postId,
				userId: req.user.id
			}).done(
				function(notification) {
					SocketManager.instance.send('notification', {
						notification: notification.toJSON()
					});
				}
			);


			return likeData;
		}
	).then(
		function(likeData) {
			res.json(likeData);
		}
	).done(
		undefined,
		BaseRouter.renderError.bind(undefined, req, res)
	);
}

function unlikePost(req, res) {
	var postID = req.param('postId');

	PostsStore.instance.unlikePost({
		postID: postID,
		user: req.user
	}).then(
		function(likeData) {
			NotificationsStore.instance.removeNotification({
				type: 'like',
				postedBy: req.user.id,
				subjectPostId: postID,
				unseenOnly: true
			});

			return likeData;
		}
	).done(
		function(likeData) {
			res.json(likeData);
		},
		BaseRouter.renderError.bind(undefined, req, res)
	);
}

var router = new BaseRouter();

router.restrictedRoute(
	{
		loggedIn: true
	},
	'/ask'
).get(askQuestion);

router.restrictedRoute(
	{
		loggedIn: true
	},
	'/posts'
).get(getPosts)
.post(addPost);

router.restrictedRoute(
	{
		loggedIn: true
	},
	'/posts/latest'
).get(getLatestPost);

router.restrictedRoute(
	{
		loggedIn: true
	},
	'/posts/:id'
).get(getPost)
.delete(deletePost)
.patch(updatePost);

router.restrictedRoute(
	{
		loggedIn: true
	},
	'/posts/:postId/like'
).patch(likePost);

router.restrictedRoute(
	{
		loggedIn: true
	},
	'/posts/:postId/unlike'
).patch(unlikePost);

router.restrictedRoute(
	{
		loggedIn: true
	},
	'/posts/facebook'
).post(addFacebookPosts);

exports = module.exports = router;
