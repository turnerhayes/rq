"use strict";

var express = require('express');
var _       = require('lodash');
var path    = require('path');
var pathsConfig = require('../config/paths');
var appConfig = require('../config/app');
var SiteConfig = require(path.join(pathsConfig.static, 'js', 'config', 'site.json'));
var Manchu  = require('../lib/manchu');
var METHODS = require('methods');

function userNotLoggedIn(req, res) {
	var statusCode = 403;

	res.status(statusCode);

	BaseRouter.renderIndex(req, res, {
		content: {
			middlePanel: Manchu.renderFile(
				path.resolve(__dirname, '../error_templates/' + statusCode + '.manchu'),
				{
					logIn: {
						requestPath: req.path
					}
				}
			)
		}
	});
}

function userNotAdmin(req, res) {
	var statusCode = 403;

	res.status(statusCode);

	BaseRouter.renderIndex(req, res, {
		content: {
			middlePanel: Manchu.renderFile(
				path.resolve(__dirname, '../error_templates/' + statusCode + '.manchu'),
				{
					authorize: true
				}
			)
		}
	});
}

function checkRestrictions(restrictions, req, res) {
	restrictions.loggedIn = restrictions.loggedIn || restrictions.admin;

	if (restrictions.loggedIn) {
		if (!req.user) {
			userNotLoggedIn(req, res);

			return false;
		}
	}

	if (restrictions.admin) {
		if (!req.user.isAdmin) {
			userNotAdmin(req, res);

			return false;
		}
	}

	return true;
}

function mixinProperties(router) {
	Object.defineProperties(router, {
		restrictedRoute: {
			enumerable: true,
			value: function(restrictions, pattern) {
				var route = router.route(pattern);

				var wrappedRoute = {};

				_.each(
					METHODS,
					function(methodName) {
						wrappedRoute[methodName] = function() {
							var callbacks = [].slice.call(arguments, 0);

							return route[methodName].apply(
								route,
								_.map(
									callbacks,
									function(callback) {
										return function(req, res, next) {
											var passedCheck = checkRestrictions(restrictions, req, res);

											if (passedCheck) {
												callback(req, res, next);
											}
										};
									}
								)
							);
						};
					}
				);

				return wrappedRoute;
			}
		}
	});
}

function BaseRouter() {
	var router = new express.Router();

	mixinProperties(router);

	return router;
}

Object.defineProperties(BaseRouter, {
	requireLogin: {
		enumerable: true,
		value: function(callback) {
			return function(req, res, next) {
				if (!req.user) {
					userNotLoggedIn(req, res);
					return;
				}

				callback(req, res, next);
			};
		}
	},

	renderIndex: {
		enumerable: true,
		value: function renderIndex(req, res, context, options) {
			options = options || {};

			context.currentUser = context.currentUser || req.user;

			context.SiteConfig = context.SiteConfig || SiteConfig;

			context.environment = appConfig.environment;

			if (options.includeRequirejsBuild) {
				context.includeRequirejsBuild = true;
			}

			res.render('index', context);
		}
	},

	renderError: {
		enumerable: true,
		value: function renderError(req, res, err) {
			BaseRouter.renderIndex(req, res, {
				content: {
					middlePanel: Manchu.renderFile(
						path.resolve(__dirname, '../error_templates/500.manchu'),
						{
							error: {
								message: err.message,
								stack: err.stack
							}
						}
					)
				}
			});
		}
	}
});

exports = module.exports = BaseRouter;
