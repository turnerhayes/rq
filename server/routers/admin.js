"use strict";

var q           = require('q');
var BaseRouter  = require('./base');
var Manchu      = require('../lib/manchu');

function facebookSyncDashboard(req, res) {
	q.all(
		[
			Manchu.getPartial('admin/facebook-sync/dashboard')
		]
	).spread(
		function(partial) {
			BaseRouter.renderIndex(
				req,
				res,
				{
					main: {
						singleColumn: true
					},
					content: {
						middlePanel: partial({
						})
					}
				},
				{
					includeRequirejsBuild: true
				}
			);
		}
	).done(
		undefined,
		BaseRouter.renderError.bind(undefined, req, res)
	);
}

var router = new BaseRouter();

router.restrictedRoute(
	{
		admin: true
	},
	'/facebook-sync'
).get(facebookSyncDashboard);

exports = module.exports = router;
