"use strict";

var passport             = require('passport');
var BaseRouter           = require('./base');
var Manchu               = require('../lib/manchu');
var authenticationConfig = require('../config/authentication');

function login(req, res) {
	res.setHeader('Content-Type', 'text/html');
	Manchu.getPartial('login/login').then(
		function(partial) {
			BaseRouter.renderIndex(
				req,
				res,
				{
					content: {
						middlePanel: partial()
					},
					suppress_user_toolbar: true,
				}
			);
		}
	);
}

var router = new BaseRouter();

router.route('/login')
	.get(
		login
	)
	.post(
		passport.authenticate(
			'local',
			{
				successRedirect: '/',
				failureRedirect: '/login',
				failureMessage: true
			}
		)
	);

router.route('/logout')
	.get(
		function(req, res) {
			req.logout();
			res.redirect('/');
		}
	);

router.route('/auth/fb')
	.get(
		passport.authenticate('facebook', { "scope": authenticationConfig.facebook.scope })
	);

router.route(authenticationConfig.facebook.callbackURL)
	.get(
		passport.authenticate(
			'facebook',
			{
				successRedirect: '/',
				failureRedirect: '/login',
				failureFlash: true,
			}
		)
	);

exports = module.exports = router;
