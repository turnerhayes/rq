"use strict";

var _          = require('lodash');
var q          = require('q');
var BaseRouter = require('./base');
var Manchu     = require('../lib/manchu');
var UserModel  = require('../lib/persistence/models/user');
var UsersStore = require('../lib/persistence/stores/users');
var PostsStore = require('../lib/persistence/stores/posts');

function _getUserWithPosts(user, filters) {
	var userPromise = !user ? UsersStore.instance.getUser({ filters: filters }) : q(user);

	return userPromise.then(function(user) {
		return PostsStore.instance.getPosts({
			filters: {
				postedBy: user.id
			}
		}).then(
			function(posts) {
				return {
					user: user,
					userPosts: posts
				};
			}
		);
	});
}

function getUsers(req, res) {
	var ids = req.query.ids;

	if (!_.isUndefined(ids)) {
		if (req.get('Content-type') == 'application/json') {
			ids = JSON.parse(ids);
		}
		else {
			ids = ids.split(',');
		}

		if (!_.isArray(ids)) {
			ids = [ids];
		}
	}

	return UsersStore.instance.getUsers({
		ids: ids
	}).done(
			function(users) {
				var usersJSON = _.map(
					users,
					function(user) {
						return user.toJSON();
					}
				);

				var currentUser;
				
				if (req.user) {
					currentUser = _.findWhere(usersJSON, {id: req.user.id});

					if (currentUser) {
						currentUser.isCurrentUser = true;
					}
				}

				res.json(usersJSON);
			},
			BaseRouter.renderError.bind(undefined, req, res)
		);
}

function getUser(req, res) {
	var userId = req.param('userId') || req.query.id;
	var username = req.query.username;
	var filters = {};

	if (_.isUndefined(userId) && _.isUndefined(username)) {
		userId = req.user.id;
	}

	if (!_.isUndefined(userId)) {
		filters.id = userId; 
	}
	else {
		filters.username = username;
	}

	var includePosts = req.query.posts;

	var userPromise;

	if (includePosts) {
		if (_.isUndefined(username)) {
			userPromise = _getUserWithPosts(req.user);
		}
		else {
			userPromise = _getUserWithPosts(undefined, {username: username});
		}
	}
	else if (req.user && userId === req.user.id) {
		userPromise = q(req.user);
	}
	else {
		userPromise = UsersStore.instance.getUser({ filters: filters }).then(
			function(user) {
				return {
					user: user
				};
			}
		);
	}

	userPromise.done(
		function(user) {
			if (!user) {
				res.status(404);
				res.json({ error: { message: "No such user" } });

				return;
			}

			res.json(user);
		},
		BaseRouter.renderError.bind(undefined, req, res)
	);
}

function profile(req, res) {
	res.setHeader('Content-type', 'text/html');

	var username = req.param('username');

	var userPromise;

	if (_.isUndefined(username)) {
		userPromise = _getUserWithPosts(req.user);
	}
	else {
		userPromise = _getUserWithPosts(undefined, {username: username});
	}

	q.all([
		Manchu.getPartial('users/user-profile'),
		userPromise
	]).spread(
		function(
			partial,
			userData
		) {
			BaseRouter.renderIndex(
				req,
				res,
				{
					content: {
						middlePanel: partial(
							_.extend(
								userData,
								{
									user: userData.user.toJSON(),
									userPosts: _.map(userData.userPosts, function(post) {
										return post.toJSON();
									}),
									renderedOnServer: true
								}
							)
						)
					}
				}
			);
		}
	).done(
		undefined,
		BaseRouter.renderError.bind(undefined, req, res)
	);	
}

function uploadUsers(req, res) {
	UsersStore.instance.addUsers(
		_.map(
			req.body,
			function(user) {
				return new UserModel({
					firstName: user.name.first,
					middleName: user.name.middle,
					lastName: user.name.last,
					preferredDisplayName: user.name.display,
					username: user.username,
					facebookId: user.facebookId
				});
			}
		)
	).done(
		function(users) {
			res.send(users);
		},
		BaseRouter.renderError.bind(undefined, req, res)
	);
}

var router = new BaseRouter();

router.restrictedRoute(
	{
		loggedIn: true
	},
	'/users'
).get(getUsers)
.post(uploadUsers);

router.restrictedRoute(
	{
		loggedIn: true
	},
	'/users/:userId'
).get(getUser);

router.restrictedRoute(
	{
		loggedIn: true
	},
	'/user'
).get(getUser);

router.restrictedRoute(
	{
		loggedIn: true
	},
	'/profile'
).get(profile);

router.restrictedRoute(
	{
		loggedIn: true
	},
	'/profile/:username'
).get(profile);

exports = module.exports = router;
