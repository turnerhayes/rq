var credentials = require('../credentials.json');
var appConfig   = require('./app');

module.exports = {
	connectionString: "postgres://" + credentials.postgres.username + ":" +
		credentials.postgres.password + "@" + appConfig.database.host +
		(appConfig.database.port ? ":" + appConfig.database.port : "") +
		"/" + appConfig.database.databaseName
};
