"use strict";

var credentials = require('../credentials.json');
var pathsConfig = require('./paths');
var path = require('path');
var SiteConfig = require(path.join(pathsConfig.static, 'js', 'config', 'site.json'));

module.exports = {
	"facebook": {
		"appID": SiteConfig.facebook.appID,
		"appSecret": credentials.facebook.appSecret,
		"profileFields": [
			"id",
			"name",
			"displayName",
			"emails"
		],
		"scope": [
			"email"
		],
		"callbackURL": "/auth/fb/callback"
	}
};
