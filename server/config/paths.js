var path = require('path');

var staticDir = path.resolve(__dirname, '../../client/static');
var serverRoot = path.resolve(__dirname, '..');

module.exports = {
	'static': staticDir,
	'templates': path.join(staticDir, 'templates'),
	'logs': path.join(serverRoot, 'logs'),
};
