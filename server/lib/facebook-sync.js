"use strict";

var q = require('q');
var FB = require('fb');
var authenticationConfig = require('../config/authentication');

function _getAccessToken() {
	var deferred = q.defer();

	FB.api('oauth/access_token', {
		client_id: authenticationConfig.facebook.appID,
		client_secret: authenticationConfig.facebook.appSecret,
		grant_type: 'client_credentials'
	}, function (res) {
		if(!res || res.error) {
			deferred.reject(res ? res.error : null);
			return;
		}

		var access_token = res.access_token;

		console.log('access_token: ', access_token);

		FB.setAccessToken(access_token);

		deferred.resolve(access_token);
	});

	return deferred.promise;
}

function FacebookSyncClient() {
	var client = this;

	client._accessTokenPromise = _getAccessToken();
}

FacebookSyncClient.prototype = Object.create(Object.prototype, {
	getPosts: {
		enumerable: true,
		value: function(options) {
			var client = this;

			options = options || {};

			return client._accessTokenPromise.then(
				function() {
					var deferred = q.defer();

					var apiOptions = {};

					if (options.start) {
						apiOptions.since = options.start.toISOString();
					}

					if (options.end) {
						apiOptions.until = options.end.toISOString();
					}

					console.log('options: ', apiOptions);

					FB.api('/2210394628/feed',
						'GET',
						apiOptions,
						function(response) {
							if (response.error) {
								deferred.reject(response.error);
								return;
							}

							console.log('posts response: ', response);

							deferred.resolve(response.data);
						}
					);

					return deferred.promise;
				}
			);
		}
	}
});

var _instance = new FacebookSyncClient();

Object.defineProperties(
	FacebookSyncClient,
	{
		instance: {
			enumerable: true,
			get: function() {
				return _instance;
			}
		}
	}
);

module.exports = FacebookSyncClient;
