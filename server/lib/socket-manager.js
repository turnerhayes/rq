"use strict";

var socketio = require('socket.io');
var _ = require('lodash');

var io;

function SocketManager() {
	var manager = this;

	io.on('connection', _.bind(manager._onConnection, manager));

	Object.defineProperties(
		manager,
		{
			_listeners: {
				value: {}
			},
			socketsByUser: {
				value: {}
			}
		}
	);

	Object.seal(manager);
}

SocketManager.prototype = Object.create(Object.prototype, {
	listen: {
		enumerable: true,
		value: function(eventName, handler) {
			var manager = this;

			io.sockets.on(eventName, handler);

			manager._listeners[eventName] = manager._listeners[eventName] || [];

			manager._listeners[eventName].push(handler);
		}
	},

	unlisten: {
		enumerable: true,
		value: function(eventName, handler) {
			var manager = this;

			io.sockets.removeListener(eventName, handler);

			_.pull(
				manager._listeners[eventName],
				[
					handler
				]
			);
		}
	},

	send: {
		enumerable: true,
		value: function(eventName, data) {
			io.sockets.emit(eventName, data);
		}
	},

	_onConnection: {
		value: function(socket) {
			var manager = this;

			manager.socketsByUser[socket.handshake.query.username] = socket;

			_.forEach(
				manager._listeners,
				function(listeners, eventName) {
					_.forEach(
						listeners,
						function(listener) {
							socket.on(eventName, listener);
						}
					);
				}
			);
		}
	}
});

var _instance;

Object.defineProperties(SocketManager, {
	instance: {
		enumerable: true,
		get: function() {
			return _instance;
		}
	}
});

SocketManager.Initialize = function(server) {
	io = socketio.listen(server);

	_instance = new SocketManager();
};

exports = module.exports = SocketManager;
