"use strict";

var Handlebars = require('handlebars');
var moment = require('moment');
var fs = require('fs');
var path = require('path');
var readdir = require('recursive-readdir');
var q = require('q');
var _ = require('lodash');
var pathsConfig = require('../config/paths');

var manchuBuilder = require(path.join(pathsConfig.static, 'js', 'lib', 'manchu-builder'));

var templatesDir = path.join(pathsConfig.static, 'templates');

var SiteConfig = require(path.join(pathsConfig.static, 'js', 'config', 'site.json'));

var Manchu = manchuBuilder({
	_: _,
	Handlebars: Handlebars,
	moment: moment,
	q: q,
	SiteConfig: SiteConfig,
	getTemplate: function getTemplate(name) {
		var deferred = q.defer();

		fs.readFile(
			path.join(templatesDir, name + '.manchu'),
			{
				encoding: "utf8"
			},
			function(err, text) {
				if (err) {
					console.error(err);
					deferred.reject(err);
					return;
				}

				console.log(text);

				deferred.resolve(Manchu.compile(text));
			}
		);

		return deferred.promise;
	}
});

Object.defineProperties(Manchu, {
	__express: {
		enumerable: true,
		value: function(filePath, options, callback) {
			fs.readFile(filePath, { encoding: 'utf8' }, function(err, text) {
				if (err) {
					console.error('Error reading file ' + filePath + ': ', err);
					return;
				}

				var template = Handlebars.compile(text);

				var rendered = template(options);

				callback(null, rendered);
			});
		}
	},

	renderFile: {
		enumerable: true,
		value: function(filePath, context) {
			var fileContents = fs.readFileSync(filePath, { encoding: 'utf8' });

			return Manchu.compile(fileContents)(context);
		}
	}
});

readdir(templatesDir, function(err, files) {
	if (!err) {
		(files || []).forEach(function(filePath) {
			var relativePath = path.relative(templatesDir, filePath);

			relativePath = relativePath.split(path.sep);

			relativePath[relativePath.length - 1] = path.basename(relativePath[relativePath.length - 1], '.manchu');

			relativePath = path.join.apply(null, relativePath);

			var text = fs.readFileSync(filePath, { encoding: 'utf8' });

			Manchu.registerPartial(relativePath, Manchu.compile(text));
		});
	}
});

module.exports = Manchu;
