"use strict";

var _ = require('lodash');

function BaseModel() {
}

BaseModel.prototype = Object.create(Object.prototype, {
	toJSON: {
		enumerable: true,
		configurable: true,
		value: function toJSON() {
			var args = arguments;

			return _.reduce(
				this.__properties,
				function(result, value, propertyName) {
					if (_.isDate(value)) {
						value = value.getTime();
					}

					if (value && 'toJSON' in Object(value)) {
						result[propertyName] = value.toJSON.apply(value, args);
						return result;
					}

					result[propertyName] = _.cloneDeep(value);
					return result;
				},
				{}
			);
		}
	}
});

module.exports = BaseModel;
