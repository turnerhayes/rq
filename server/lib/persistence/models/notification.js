"use strict";

var ModelBuilder = require('./model-builder');
var PostModel = require('./post');
var UserModel = require('./user');

var NotificationModel = new ModelBuilder('Notification')
	.property(
		'id',
		{
			type: Number,
			required: true,
			readOnly: true,
			deferrable: true
		}
	).property(
		'type',
		{
			type: String,
			required: true
		}
	).property(
		'subjectPost',
		{
			type: PostModel
		}
	).property(
		'relatedPost',
		{
			type: PostModel
		}
	).property(
		'timestamp',
		{
			type: Date
		}
	).property(
		'postedBy',
		{
			type: UserModel
		}
	).property(
		'seen',
		{
			type: Boolean
		}
	).build();

module.exports = NotificationModel;
