"use strict";

var mongoose = require('mongoose');
var PostSchema = require('../schemas/post');

var PostModel = mongoose.model("Post", PostSchema);

module.exports = PostModel;
