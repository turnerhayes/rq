"use strict";

var ModelBuilder = require('./model-builder');

var UserModel = new ModelBuilder('User')
	.property(
		'id',
		{
			type: Number,
			required: true,
			readOnly: true,
			deferrable: true
		}
	).property(
		'username',
		{
			type: String,
			readOnly: true
		}
	).property(
		'firstName',
		{
			type: String,
			required: true
		}
	).property(
		'middleName',
		{
			type: String
		}
	).property(
		'lastName',
		{
			type: String,
			required: true
		}
	).property(
		'email',
		{
			type: String,
		}
	).property(
		'profilePhotoURL',
		{
			type: String
		}
	).property(
		'preferredDisplayName',
		{
			type: String
		}
	).property(
		'facebookId',
		{
			type: Number
		}
	).property(
		'createdOn',
		{
			type: Date
		}
	).property(
		'deletedOn',
		{
			type: Date
		}
	).property(
		'updatedOn',
		{
			type: Date
		}
	).property(
		'hasLoggedIn',
		{
			type: Boolean
		}
	).computedProperty(
		'displayName',
		{
			type: String,
			getter: function () {
				var model = this;

				return model.preferredDisplayName ?
					model.preferredDisplayName :
					model.firstName +
						(
							model.middleName ?
								' ' + model.middleName :
								''
						) +
						' ' + model.lastName;
			}
		}
	).property(
		'isAdmin',
		{
			type: Boolean
		}
	).build();

module.exports = UserModel;
