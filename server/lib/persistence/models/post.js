"use strict";

var _ = require('lodash');
var ModelBuilder = require('./model-builder');
var UserModel = require('./user');

var PostModel = new ModelBuilder('Post')
	.property(
		'id',
		{
			type: Number,
			required: true,
			readOnly: true,
			deferrable: true
		}
	).property(
		'body',
		{
			type: String,
			required: true
		}
	).property(
		'postedBy',
		{
			type: UserModel
		}
	).property(
		'postedOn',
		{
			type: Date
		}
	).property(
		'createdOn',
		{
			type: Date
		}
	).property(
		'deletedOn',
		{
			type: Date
		}
	).property(
		'updatedOn',
		{
			type: Date
		}
	).property(
		'path',
		{
			type: String
		}
	).property(
		'facebookId',
		{
			type: Number
		}
	).property(
		'likes',
		{
			type: Array
		}
	).computedProperty(
		'parentId',
		{
			type: Number,
			getter: function() {
				var matches = /(\d+),$/.exec(this.path);

				if (matches) {
					return parseInt(matches[1], 10);
				}

				return undefined;
			}
		}
	).computedProperty(
		'ancestorIds',
		{
			type: Number,
			getter: function() {
				var model = this;

				if (!model.path) {
					return undefined;
				}

				return _.map(
					model.path.replace(/^,|,$/, '').split(','),
					function(id) {
						return parseInt(id, 10);
					}
				);
			}
		}
	).build();

var toJSON = PostModel.prototype.toJSON;

Object.defineProperties(PostModel.prototype, {
	toJSON: {
		enumerable: true,
		configurable: true,
		value: function(options) {
			var model = this;

			var args = [].splice.call(arguments, 0);

			if (!options) {
				args[0] = options = {};
			}


			var json = toJSON.apply(model, args);

			if (model.children && !options.postOnly) {
				json.children = _.map(model.children, function(child) {
					return child.toJSON(args);
				});
			}

			return json;
		}
	},

	isLikedByUser: {
		enumerable: true,
		value: function(user) {
			if (!user) {
				return false;
			}
			
			return _.contains(this.likes, user.id);
		}
	}
});


module.exports = PostModel;
