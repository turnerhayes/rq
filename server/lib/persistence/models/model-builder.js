"use strict";

var _ = require('lodash');
var BaseModel = require('./base');

function validateType(value, expectedType) {
	var wrongType = false;

	if (!_.isNull(expectedType)) {
		if (expectedType === String) {
			if (!_.isString(value)) {
				wrongType = true;
			}
		}
		else if (expectedType === Number) {
			if (!_.isNumber(value)) {
				wrongType = true;
			}
		}
		else if (expectedType === Date) {
			// If this is an invalid date, then _.isDate() will return true, but
			// .getTime() will be NaN--the second part checks if it's an invalid date
			if (!(_.isDate(value) && value.getTime() === value.getTime())) {
				wrongType = true;
			}
		}
		else if (expectedType === Array) {
			if (!_.isArray(value)) {
				wrongType = true;
			}
		}
		else if (expectedType === Boolean) {
			if (!_.isBoolean(value)) {
				wrongType = true;
			}
		}
		else {
			if (!(value instanceof expectedType)) {
				wrongType = true;
			}
		}
	}

	return !wrongType;
}

function typeValidationError(propertyName, value, expectedType) {
	var error;

	if (!validateType(value, expectedType)) {
		error = new TypeError('Property ' + propertyName + ' is of the wrong type');
		error.expectedType = expectedType;
		error.actualType = typeof value;


		return error;
	}

	return undefined;
}

function ModelBuilder(modelName) {
	var builder = this;

	if (!modelName) {
		throw new Error('Must specify a name for the model');
	}

	Object.defineProperties(builder, {
		__modelName: {
			enumerable: false,
			value: modelName
		},
		__properties: {
			enumerable: false,
			value: {}
		},
		__computedProperties: {
			enumerable: false,
			value: {}
		}
	});
}

ModelBuilder.prototype = Object.create(Object.prototype, {
	property: {
		enumerable: true,
		value: function(propertyName, options) {
			var builder = this;

			options = options || {};

			builder.__properties[propertyName] = {
				type: options.type,
				required: 'required' in options ? options.required : false,
				deferrable: 'deferrable' in options ? options.deferrable : false
			};

			return builder;
		}
	},

	computedProperty: {
		enumerable: true,
		value: function(propertyName, options) {
			var builder = this;

			builder.__computedProperties[propertyName] = {
				type: options.type,
				getter: options.getter,
				setter: options.setter
			};

			return builder;
		}
	},

	build: {
		enumerable: false,
		value: function() {
			var builder = this;

			function Model(initData) {
				BaseModel.call(this, initData);
				initialize(this, initData);
			}

			var props = {};

			var __propertyStore = {};

			function initialize(model, initData) {
				Object.defineProperties(model, {
					__properties: {
						value: _.cloneDeep(__propertyStore)
					}
				});

				_.forIn(initData, function(value, propertyName) {
					if (propertyName in model.__properties) {
						if (
							initData[propertyName] !== undefined &&
							initData[propertyName] !== null
						) {
							var error = typeValidationError(
								propertyName,
								value,
								builder.__properties[propertyName].type
							);

							if (error) {
								throw error;
							}
						}

						model.__properties[propertyName] = value;
					}
				});

				var missingRequiredProperties = [];

				_.forIn(model.__properties, function(propertyValue, propertyName) {
					if (
						builder.__properties[propertyName].required &&
						!builder.__properties[propertyName].deferrable &&
						!initData[propertyName]
					) {
						missingRequiredProperties.push(propertyName);
					}


					model.__properties[propertyName] = initData[propertyName];
				});

				_.forIn(
					builder.__computedProperties,
					function(buildProperties, propertyName) {
						if (!_.isUndefined(buildProperties.setter)) {
							if (
								!initData[propertyName] &&
								buildProperties.required
							) {
								missingRequiredProperties.push(propertyName);
								return;
							}

							model[propertyName] = initData[propertyName];
						}
					}
				);

				if (missingRequiredProperties.length > 0) {
					throw new Error('The following properties are required for the ' + builder.__modelName + ' model, but did not have values: ' +
							missingRequiredProperties.join(', '));
				}
			}

			_.forOwn(builder.__properties, function(propertyValue, propertyName) {
				__propertyStore[propertyName] = undefined;

				props[propertyName] = {
					enumerable: true,
					get: function() {
						return this.__properties[propertyName];
					}
				};

				if (!builder.__properties[propertyName].readOnly) {
					props[propertyName].set = function(value) {
						var expectedType = builder.__properties[propertyName].type;
						var error;

						if (expectedType && !(value === null || value === undefined)) {
							error = typeValidationError(propertyName, value, expectedType);

							if (error) {
								throw error;
							}
						}

						this.__properties[propertyName] = value;
					};
				}
			});

			_.forOwn(builder.__computedProperties, function(propertyValue, propertyName) {
				props[propertyName] = {
					enumerable: true,
					get: function() {
						return propertyValue.getter.call(this);
					}
				};

				if (propertyValue.setter) {
					props[propertyName].set = function(value) {
						propertyValue.setter.call(this, value);
					};
				}
			});

			props.toJSON = {
				enumerable: true,
				configurable: true,
				value: function(options) {
					var model = this;

					options = options || {};

					var json = BaseModel.prototype.toJSON.call(model);

					if (!options.excludeComputed) {
						Object.keys(builder.__computedProperties).forEach(function(key) {
							json[key] = model[key];
						});
					}

					return json;
				}
			};

			props.toString = {
				enumerable: true,
				configurable: true,
				value: function() {
					return '[object ' + builder.__modelName + 'Model]';
				}
			};

			props.validate = {
				enumerable: true,
				configurable: true,
				value: function() {
					var model = this;
					var property, value;

					var errors = [];

					for (property in model.__properties) {
						value = model[property];

						if (
							builder.__properties[property].required &&
							!builder.__properties[property].deferrable &&
							(value === null || value === undefined)
						) {
							errors.push('Property ' + property + ' is missing');
						}

						if (!(value === null || value === undefined)) {
							var typeError = typeValidationError(property, value, builder.__properties[property].type);

							if (
								typeError
							) {
								errors.push(typeError.message);
							}
						}
					}

					if (errors.length === 0) {
						return undefined;
					}

					return errors;
				}
			};

			Model.prototype = Object.create(BaseModel.prototype, props);

			return Model;
		}
	}
});

module.exports = ModelBuilder;
