"use strict";

var q = require('q');
var _ = require('lodash');
var BaseStore = require('./base');
var UsersStore = require('./users');
var PostsStore = require('./posts');
var NotificationModel = require('../models/notification');
var NotificationSchema = require('../schemas/notification');

function _notificationModelFromResult(result) {
	result.id = parseInt(result.id, 10);

	return new NotificationModel(result);
}

function _processResults(results, options) {
	var userIDs = _.pluck(results, 'postedBy');
	var postIDs = _.reduce(
		results,
		function(accumulator, result) {
			if (!_.isUndefined(result.subjectPostId)) {
				accumulator.push(result.subjectPostId);
			}

			if (!_.isUndefined(result.relatedPostId)) {
				accumulator.push(result.relatedPostId);
			}

			return accumulator;
		},
		[]
	);

	options = options || {};

	if (options.full) {
		return q.all(
			[
				UsersStore.instance.getUsers(
					{
						ids: userIDs
					}
				),
				PostsStore.instance.getPosts(
					{
						ids: postIDs
					}
				)
			]
		).spread(
			function(users, posts) {
				var postsById = _.reduce(
					posts,
					function(postsObject, post) {
						postsObject[post.id] = post;

						return postsObject;
					},
					{}
				);

				var usersById = _.reduce(
					users,
					function(usersObject, user) {
						usersObject[user.id] = user;

						return usersObject;
					},
					{}
				);

				return _.map(
					results,
					function(result) {
						if (!_.isUndefined(result.postedBy)) {
							result.postedBy = usersById[result.postedBy];
						}

						if (!_.isUndefined(result.subjectPostId)) {
							result.subjectPost = postsById[result.subjectPostId];
						}

						if (!_.isUndefined(result.relatedPostId)) {
							result.relatedPost = postsById[result.relatedPostId];
						}

						return _notificationModelFromResult(result);
					}
				);
			}
		);
	}

	return q(
		_.map(
			results,
			_notificationModelFromResult
		)
	);

}

function _addNotification(store, options) {
	options = options || {};

	var postIds = [];

	if (options.subjectPostId) {
		postIds.push(options.subjectPostId);
	}

	if (options.relatedPostId) {
		postIds.push(options.relatedPostId);
	}

	return q.all(
		[
			store.find({
				includeDeleted: true,
				tableSource: BaseStore._knex.raw(
					'notifications_add_notification(?, ?, ?, ?, ?)',
					[
						options.type,
						options.subjectPostId,
						options.relatedPostId,
						options.postedBy,
						BaseStore._knex.raw("'{" + options.usersToNotify.join(',') + "}'::bigint[]")
					]
				)
			}),
			options.postedBy ?
				UsersStore.instance.getUser({ id: options.postedBy }) :
				undefined,
			postIds.length > 0 ?
				PostsStore.instance.getPosts({ ids: postIds }) :
				undefined
		]
	).spread(
		function(notificationData, postedBy, posts) {
			var result = notificationData[0];

			if (!_.isUndefined(result.subjectPostId)) {
				result.subjectPostId = parseInt(result.subjectPostId, 10);
			}

			if (!_.isUndefined(result.relatedPostId)) {
				result.relatedPostId = parseInt(result.relatedPostId, 10);
			}

			result.postedBy = postedBy;

			if (result.subjectPostId) {
				result.subjectPost = _.find(posts, { id: result.subjectPostId });
			}

			if (result.relatedPostId) {
				result.relatedPost = _.find(posts, { id: result.relatedPostId });
			}

			return _notificationModelFromResult(result);
		}
	);
}

function NotificationsStore() {}

NotificationsStore.prototype = Object.create(BaseStore.prototype, {
	ModelClass: {
		enumerable: true,
		value: NotificationModel
	},

	Schema: {
		enumerable: true,
		value: NotificationSchema
	},

	tableName: {
		enumerable: true,
		value: 'notifications'
	},

	getUserNotifications: {
		enumerable: true,
		value: function(options) {
			var store = this;

			options = options || {};

			var columns;

			if (_.isUndefined(options.full)) {
				options.full = true;
			}

			if (!options.full) {
				columns = [
					'id',
					'type',
					'createdOn',
					'deletedOn',
					'seen'
				];
			}

			var filters;

			if (options.unseenOnly) {
				filters = {
					seen: false
				};
			}

			return q(
				store.find({
					columns: columns,
					tableSource: BaseStore._knex.raw('notifications_get_user_notifications(?)', options.userId),
					filters: filters
				})
			).then(
				function(results) {
					return _processResults(
						results,
						{
							full: options.full
						}
					);
				}
			);
		}
	},

	notifyComment: {
		enumerable: true,
		value: function(options) {
			var store = this;

			options = options || {};

			return _addNotification(store, {
				type: 'comment',
				subjectPostId: options.post.parentId,
				relatedPostId: options.post.id,
				postedBy: options.post.postedBy,
				usersToNotify: [
					options.post.postedBy
				]
			});
		}
	},

	notifyLike: {
		enumerable: true,
		value: function(options) {
			var store = this;

			options = options || {};

			return _addNotification(store, {
				type: 'like',
				subjectPostId: options.post ? options.post.id : options.postId,
				postedBy: options.userId,
				usersToNotify: [options.userId]
			});
		}
	},

	removeNotification: {
		enumerable: true,
		value: function(options) {
			var store = this;

			options = options || {};

			return q(
				store.find({
					includeDeleted: true,
					tableSource: BaseStore._knex.raw(
						'notifications_remove_notification(?, ?, ?, ?, ?, ?)',
						[
							options.notificationId,
							options.type,
							options.postedBy,
							options.subjectPostId,
							options.relatedPostId,
							options.unseenOnly
						]
					)
				})
			);
		}
	},

	markNotificationsSeen: {
		enumerable: true,
		value: function(options) {
			var store = this;

			options = options || {};

			var query = BaseStore._knex('userNotifications').update({
				seen: true
			});

			if (options.id) {
				query.where('notificationId', options.id);
			}

			if (options.ids) {
				query.whereIn('notificationId', options.ids);
			}

			return q(query);
		}
	}
});


var _instance;

Object.defineProperties(NotificationsStore, {
	instance: {
		enumerable: true,
		get: function() {
			if (!_instance) {
				_instance = new NotificationsStore();
			}

			return _instance;
		}
	}
});

exports = module.exports = NotificationsStore;
