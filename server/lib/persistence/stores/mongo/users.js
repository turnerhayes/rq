"use strict";

var BaseStore = require('./base');
var UserModel = require('../models/user');

function UserStore() {

}

UserStore.prototype = Object.create(BaseStore.prototype, {
	ModelClass: {
		enumerable: true,
		value: UserModel
	},

	addUser: {
		enumerable: true,
		value: function addUser(user) {
			return this.add(user);
		}
	},

	getUsers: {
		enumerable: true,
		value: function getUsers(options) {
			return this.find(options);
		}
	},

	getUser: {
		enumerable: true,
		value: function getUser(options) {
			return this.findItem(options);
		}
	},

	getUserByID: {
		enumerable: true,
		value: function getUserByID(id) {
			return this.getUser({ id: id });
		}
	},

	deleteUser: {
		enumerable: true,
		value: function deleteUser(model, hard) {
			if (hard) {
				return this.remove(model);
			}

			return this.softDelete(model);
		}
	},

	undeleteUser: {
		enumerable: true,
		value: function undeleteUser(model) {
			return this.softUndelete(model);
		}
	}
});


var _instance;

Object.defineProperties(UserStore, {
	instance: {
		enumerable: true,
		get: function() {
			if (!_instance) {
				_instance = new UserStore();
			}

			return _instance;
		}
	}
});

module.exports = UserStore;
