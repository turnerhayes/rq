"use strict";

var BaseStore = require('./base');
var PostModel = require('../models/post');

function PostStore() {}

function structureComments(comments, root) {
	var commentsByParent = {};
	var i, comment;

	console.log('root', root);

	comments.forEach(function(comment) {
		var parentID = comment.parent._id;

		commentsByParent[parentID] = commentsByParent[parentID] || [];
		commentsByParent[parentID].push(comment);
	});

	comments.unshift(root);


	for (i = comments.length - 1; i >= 0; i--) {
		comment = comments[i];
		if (commentsByParent[comment._id]) {
			comment.comments = commentsByParent[comment._id];
			// console.log('Setting comments to ' + comment.id);
		}
	}

	console.log('structured comments: ', comments);

	return root;
}

PostStore.prototype = Object.create(BaseStore.prototype, {
	ModelClass: {
		enumerable: true,
		value: PostModel
	},

	addPost: {
		enumerable: true,
		value: function addPost(post) {
			var store = this;

			var parentID = post.parentID;
			delete post.parentID;

			if (parentID) {
				return this.getPost({
					id: parentID
				}).then(
					function(parent) {
						post.parent = parent;
						post.root = parent.root || parent;

						return store.add(post);
					}
				);
			}

			return this.add(post);
		}
	},

	getPosts: {
		enumerable: true,
		value: function getPosts(options) {
			options = options || {};

			if (options.populate !== false) {
				options.populate = ['posted_by', 'parent', 'root'];
			}
			else {
				delete options.populate;
			}

			return this.find(options).then(function(posts) {
				return posts;
			});
		}
	},

	getPost: {
		enumerable: true,
		value: function getPost(options) {
			options = options || {};
			options.filters = options.filters || {};

			options.limit = 1;

			if (!('sortBy' in options)) {
				options.sortBy = { _id: -1 };
			}

			return this.getPosts(options);
		}
	},

	getQuestion: {
		enumerable: true,
		value: function getQuestion(options) {
			// options = options || {};
			// options.filters = options.filters || {};

			// options.filters.parent = null;

			// return this.getPost(options);

			var store = this;

			options = options || {};

			options.filters.parent = null;

			var full = options.full;

			delete options.full;

			if (full) {
				delete options.populate;
			}

			if (options.populate !== false) {
				options.populate = ['posted_by', 'parent', 'root'];
			}
			else {
				delete options.populate;
			}

			return store.getPost(options).then(
				function(post) {
					if (post && full) {
						return store.getComments({
							root: post,
							sortBy: { _id: 1 } // sort by creation date (TODO: is this reliable enough? maybe have real creation date column?)
						}).then(
							function(comments) {
								structureComments(comments, post);

								return post;
							}
						);
					}

					return post;
				}
			);
		}
	},

	getQuestions: {
		enumerable: true,
		value: function getQuestions(options) {
			var store = this;

			options = options || {};

			options.filters.parent = null;

			var full = options.full;

			delete options.full;

			if (full) {
				delete options.populate;
			}

			if (options.populate !== false) {
				options.populate = ['posted_by', 'parent', 'root'];
			}
			else {
				delete options.populate;
			}

			return store.findItem(options).then(
				function(post) {
					if (post && full) {
						return store.getComments({
							root: post,
							sortBy: { _id: 1 } // sort by creation date (TODO: is this reliable enough? maybe have real creation date column?)
						}).then(
							function(comments) {
								structureComments(comments, post);

								return post;
							}
						);
					}

					return post;
				}
			);
		}
	},

	getComments: {
		enumerable: true,
		value: function getComments(options) {
			options = options || {};
			options.filters = options.filters || {};

			if (options.parent) {
				options.filters.parent = options.parent;
			}
			else if (options.parentID) {
				options.filters.parent = new mongoose.Types.ObjectId(options.parentID);
			}
			else if (options.root) {
				options.filters.root = options.root;
			}
			else if (options.rootID) {
				options.filters.root = new mongoose.Types.ObjectId(options.rootID);
			}

			return this.getPosts(options);
		}
	},

	deletePost: {
		enumerable: true,
		value: function deletePost(model, hard) {
			if (hard) {
				return this.remove(model);
			}

			return this.softDelete(model);
		}
	},

	undeletePost: {
		enumerable: true,
		value: function undeletePost(model) {
			return this.softUndelete(model);
		}
	}
});


var _instance;

Object.defineProperties(PostStore, {
	instance: {
		enumerable: true,
		get: function() {
			if (!_instance) {
				_instance = new PostStore();
			}

			return _instance;
		}
	}
});

module.exports = PostStore;
