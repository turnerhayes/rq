"use strict";

var mongoose = require('mongoose');
var mongooseConfig = require('../../../config/mongoose');
var q = require('q');
var connection;

function connectToMongoDB() {
	if (connection) {
		return;
	}

	connection = mongoose.connect(mongooseConfig.connectionString);
}

connectToMongoDB();


var Store = function(options) {
	this.initialize(options);
};

Store.prototype = {
	initialize: function initialize(options) {

	},

	find: function(options) {
		options = options || {};

		var filters = options.filters || {};

		var store = this;
		var deferred = q.defer();

		var minID = options.minID;

		if (minID && Object.prototype.toString.call(id) === '[object String]') {
			minID = new mongoose.Types.ObjectId(minID);
		}

		var id = options.id;

		if (id && Object.prototype.toString.call(id) === '[object String]') {
			id = new mongoose.Types.ObjectId(id);
		}

		if (!options.includeDeleted) {
			filters.deleted_date = null;
		}

		var query;

		if (id) {
			query = store.ModelClass.findById(id);
		} else if (options.limit === 1) {
			query = store.ModelClass.findOne(filters);
		} else {
			query = store.ModelClass.find(filters);
		}

		if (options.populate && options.populate.length > 0) {
			query = query.populate(options.populate);
		}

		if (options.limit) {
			query.limit(options.limit);
		}

		if (options.minID) {
			query.where('_id').gt(options.minID);
		}

		if (options.sortBy) {
			query.sort(options.sortBy);
		}

		query.exec(function(err, results) {
				if (err) {
					deferred.reject(err);
					return;
				}

				deferred.resolve(results);
			});

		return deferred.promise;
	},

	findItem: function(options) {
		var store = this;

		options = options || {};

		if (!options.id) {
			options.limit = 1;
		}
		
		return store.find(options);
	},

	add: function add(data) {
		var store = this;
		var deferred = q.defer();

		var model = new store.ModelClass(data);

		model.save(function(err, results) {
			if (err) {
				deferred.reject(err);
				return;
			}

			deferred.resolve(results);
		});

		return deferred.promise;
	},

	remove: function remove(model) {
		var deferred = q.defer();

		model.remove(function(err, result) {
			if (err) {
				deferred.reject(err);
				return;
			}

			deferred.resolve(result);
		});

		return deferred.promise;
	},

	removeByID: function removeByID(id) {
		var store = this;
		var deferred = q.defer();

		store.ModelClass.findByIdAndRemove(id, function(err, result) {
			if (err) {
				deferred.reject(err);
				return;
			}

			deferred.resolve(result);
		});

		return deferred.promise;
	},

	softDelete: function softDelete(model) {
		var deferred = q.defer();

		model.deleted_date = Date.now();

		model.save(function(err, result) {
			if (err) {
				deferred.reject(err);
				return;
			}

			deferred.resolve(result);
		});

		return deferred.promise;
	},

	softDeleteByID: function softDeleteByID(id) {
		var store = this;
		var deferred = q.defer();

		store.ModelClass.update({ _id: id }, { deleted_date: Date.now() }, function(err, result) {
			if (err) {
				deferred.reject(err);
				return;
			}

			deferred.resolve(result);
		});

		return deferred.promise;
	},

	softUndelete: function softUndelete(model) {
		var deferred = q.defer();

		model.deleted_date = null;

		model.save(function(err, result) {
			if (err) {
				deferred.reject(err);
				return;
			}

			deferred.resolve(result);
		});

		return deferred.promise;
	},

	softUndeleteByID: function softUndeleteByID(id) {
		var store = this;
		var deferred = q.defer();

		store.ModelClass.update({ _id: id }, { deleted_date: null }, function(err, result) {
			if (err) {
				deferred.reject(err);
				return;
			}

			deferred.resolve(result);
		});

		return deferred.promise;
	},
};

module.exports = Store;
