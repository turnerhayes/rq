"use strict";

var pgConfig = require('../../../config/pg');
var pg = require('knex')({
	client: 'pg',
	connection: pgConfig.connectionString,
	debug: !!process.env.RQ_DEBUG_DB
});
var q = require('q');
var _ = require('lodash');
var assert = require('assert');
var SchemaUtils = require('../schemas/utils');

function _filterDataBySchemaColumns(data, schema) {
	var filtered = {};

	_.forEach(data, function(value, key) {
		// Filter out any column not in the schema (e.g. computed properties in model)
		if (!(key in schema.columns)) {
			return;
		}

		// If the value is undefined, simply don't specify it in the insert query
		if (_.isUndefined(data[key])) {
			return;
		}

		filtered[key] = value;
	});

	return filtered;
}

var Store = function() {
};

Store.prototype = Object.create(Object.prototype, {
	Schema: {
		enumerable: true,
		configurable: true,
		get: function() {
			throw new Error('Store classes must specify their Schema object');
		}
	},

	tableName: {
		enumerable: true,
		configurable: true,
		get: function() {
			throw new Error('Store classes must specify their tableName');
		}
	},

	find: {
		enumerable: true,
		configurable: true,
		value: function find(options) {
			options = options || {};

			var filters = options.filters || {};

			var store = this;

			var tableSource = options.tableSource || store.tableName;

			var tableAlias = 'tbl';

			if (typeof options.id !== 'undefined') {
				filters.id = options.id;
			}

			if (!options.includeDeleted) {
				filters.deletedOn = null;
			}

			_.forIn(filters, function(filterValue, filterName) {
				if (filterName.indexOf(tableAlias + '.') !== 0) {
					filters[tableAlias + '.' + filterName] = filterValue;
					delete filters[filterName];
				}
			});

			var query = pg.select(options.columns || undefined)
				.from(pg.raw(tableSource + ' AS ' + tableAlias))
				.where(filters);

			if (!_.isUndefined(options.ids)) {
				query = query.whereIn('id', _.unique(options.ids));
			}

			if (!_.isUndefined(options.idQuery)) {
				query = query.whereIn('id', options.idQuery);
			}

			if (options.minID) {
				query = query.where(tableAlias + '.id', '>', options.minID);
			}

			if (options.limit) {
				query = query.limit(options.limit);
			}

			if (options.sortBy) {
				_.forIn(options.sortBy, function(value, key) {
					query = query.orderBy(key, value < 0 ? 'desc' : 'asc');
				});
			}

			return q(query).then(function(results) {
				if (results && results.length) {
					return results;
				}

				return undefined;
			});
		}
	},

	findItem: {
		enumerable: true,
		configurable: true,
		value: function findItem(options) {
			var store = this;

			options = options || {};

			if (!options.id) {
				options.limit = 1;
			}
			
			return store.find(options).then(function(results) {
				if (results && results.length > 0) {
					return results[0];
				}

				return undefined;
			});
		}
	},

	add: {
		enumerable: true,
		configurable: true,
		value: function add(data, options) {
			var store = this;
			var dataLength, selectClause, columnNames;

			if (_.isArray(data)) {
				dataLength = _.size(data);

				if (dataLength === 0) {
					return q([]);
				}

				if (dataLength === 1) {
					data = data[0];
				}
			}

			options = options || {};

			var whereNotExists = options.filters && options.filters.whereNotExists ?
				options.filters.whereNotExists :
				undefined;

			if (_.isArray(data)) {
				data = _.map(
					data,
					function(item) {
						return _filterDataBySchemaColumns(item, store.Schema);
					}
				);
			}
			else {
				data = _filterDataBySchemaColumns(data, store.Schema);
			}

			var query;

			function dataToSelectFields(d) {
				return _.map(
					columnNames,
					function(columnName) {
						var res;
						var value = d[columnName];
						var schemaColumn = store.Schema.columns[columnName];

						if (_.isUndefined(value) || _.isNull(value)) {
							res = 'NULL';
						}
						else {
							res = "'" + value + "'";
						}

						return res + '::' + SchemaUtils.DBTypeForSchemaType(schemaColumn.type) + ' AS "' + columnName + '"';
					}
				).join(', ');
			}

			function dataItemToSelectValuesQuery(queryClause, dataItem) {
				var selectData = _.defaults(
					_.clone(dataItem),
					_.zipObject(
						columnNames,
						[]
					)
				);

				var select = pg.select(
					pg.raw(
						dataToSelectFields(selectData)
					)
				);

				if (queryClause) {
					return queryClause.unionAll(select);
				}

				return select;
			}

			if (!_.isUndefined(whereNotExists)) {
				if (_.isArray(data)) {
					columnNames = _.unique(
						_.reduce(
							data,
							function(allColumns, dataItem) {
								return allColumns.concat(_.keys(dataItem));
							},
							[]
						)
					).sort();

					selectClause = _.reduce(
						data,
						dataItemToSelectValuesQuery,
						undefined
					);
				}
				else {
					columnNames = Object.keys(data).sort();

					selectClause = dataItemToSelectValuesQuery(undefined, data);
				}

				selectClause = pg.select().distinct()
					.from(selectClause.as("insert_values"));

				whereNotExists = _.intersection(whereNotExists, columnNames);

				if (!_.isUndefined(whereNotExists) && _.size(whereNotExists) > 0) {
					selectClause = selectClause.whereNotExists(
						_.reduce(
							whereNotExists,
							function(selectQuery, notExistsColumn) {
								return selectQuery.whereRaw('"' + store.tableName + '"."' + notExistsColumn + '" = "insert_values"."' + notExistsColumn + '"');
							},
							pg.select(1)
								.from(store.tableName)
						)
					);
				}

				query = q(
					pg.raw(
						'insert into "' + store.tableName + '" (' +
						_.map(
							columnNames,
							function(columnName) {
								return '"' + columnName + '"';
							}
						).join(', ') +
						") ? returning *",
						[
							selectClause
						]
					)
				).then(
					function(results) {
						return results.rows;
					}
				);
			}
			else {
				query = q(
					pg.insert(data)
						.into(store.tableName)
						.returning('*')
				);

			}

			return query;
		}
	},

	remove: {
		enumerable: true,
		configurable: true,
		value: function remove(options) {
			var store = this;

			options = options || {};

			assert(!_.isEmpty(options.filters), 'No filters specified');

			return pg.del()
				.from(store.tableName)
				.where(options.filters);
		}
	},

	removeModel: {
		enumerable: true,
		configurable: true,
		value: function remove(model) {
			var store = this;

			return store.removeByID(model.id);
		}
	},

	removeByID: {
		enumerable: true,
		configurable: true,
		value: function removeByID(id) {
			var store = this;

			return pg.del()
				.from(store.tableName)
				.where({id: id});
		}
	},

	softDelete: {
		enumerable: true,
		configurable: true,
		value: function softDelete(model) {
			var store = this;

			return store.softDeleteByID(model.id);
		}
	},

	softDeleteByID: {
		enumerable: true,
		configurable: true,
		value: function softDeleteByID(id) {
			var store = this;

			return store.update({
				id: id,
				newData: {
					deletedOn: Date.now()
				}
			});
		}
	},

	softUndelete: {
		enumerable: true,
		configurable: true,
		value: function softUndelete(model) {
			var store = this;

			return store.softUndeleteByID(model.id);
		}
	},

	softUndeleteByID: {
		enumerable: true,
		configurable: true,
		value: function softUndeleteByID(id) {
			var store = this;

			return store.update({
				id: id,
				newData: {
					deletedOn: null
				}
			});
		}
	},

	update: {
		enumerable: true,
		configurable: true,
		value: function (data) {
			var store = this;

			data = data || {};

			if (_.isEmpty(data)) {
				return q(undefined);
			}

			var id = data.id;

			data = _filterDataBySchemaColumns(data);

			delete data.id;

			var query = pg(store.tableName).update(data);

			if (id) {
				query = query.where('id', id);
			}

			return query;
		}
	}
});

Object.defineProperties(
	Store,
	{
		_knex: {
			value: pg
		}
	}
);

module.exports = Store;
