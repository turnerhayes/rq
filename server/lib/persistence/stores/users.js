"use strict";

var q = require('q');
var _ = require('lodash');
var BaseStore = require('./base');
var UserSchema = require('../schemas/user.json');
var UserModel = require('../models/user');

function _resultToUserModel(result) {
	if (!result) {
		return undefined;
	}

	result.id = parseInt(result.id, 10);

	if (result.facebookId) {
		result.facebookId = parseInt(result.facebookId, 10);
	}

	return new UserModel(result);
}

function UserStore() {

}

UserStore.prototype = Object.create(BaseStore.prototype, {
	Schema: {
		enumerable: true,
		value: UserSchema
	},

	tableName: {
		enumerable: true,
		value: 'users'
	},

	addUser: {
		enumerable: true,
		value: function addUser(user) {
			var userData = user.toJSON({excludeComputed: true});

			return q(
				this.add(userData)
			).then(
				function(users) {
					if (_.size(users) > 0) {
						return users[0];
					}

					return users;
				}
			);
		}
	},

	addUsers: {
		enumerable: true,
		value: function(users, options) {
			options = options || {};

			options.filters = options.filters || {};
			options.filters.whereNotExists = options.filters.whereNotExists || [];

			options.filters.whereNotExists.push('facebookId');

			var usersJSON = _.map(
				users,
				function(user) {
					return user.toJSON();
				}
			);

			return q(
				this.add(usersJSON, options)
			);
		}
	},

	getUsers: {
		enumerable: true,
		value: function getUsers(options) {
			return q(this.find(options)).then(function(results) {
				if (!results) {
					return undefined;
				}

				return _.map(results, _resultToUserModel);
			});
		}
	},

	getUser: {
		enumerable: true,
		value: function getUser(options) {
			return q(this.findItem(options)).then(_resultToUserModel);
		}
	},

	getUserByID: {
		enumerable: true,
		value: function getUserByID(id) {
			return this.getUser({ id: id });
		}
	},

	deleteUser: {
		enumerable: true,
		value: function deleteUser(model, hard) {
			if (hard) {
				return this.remove(model);
			}

			return q(this.softDelete(model));
		}
	},

	undeleteUser: {
		enumerable: true,
		value: function undeleteUser(model) {
			return q(this.softUndelete(model));
		}
	},

	updateUser: {
		enumerable: true,
		value: function updateUser(model) {
			return q(this.update(model.toJSON()));
		}
	}
});


var _instance;

Object.defineProperties(UserStore, {
	instance: {
		enumerable: true,
		get: function() {
			if (!_instance) {
				_instance = new UserStore();
			}

			return _instance;
		}
	}
});

module.exports = UserStore;
