"use strict";

var BaseStore = require('./base');
var UsersStore = require('./users');
var PostSchema = require('../schemas/post.json');
var PostModel = require('../models/post');
var q = require('q');
var _ = require('lodash');

function structureComments(comments) {
	var structured = [];

	var commentsById = {};

	comments.forEach(function(comment) {
		commentsById[comment.id] = comment;
	});

	comments.forEach(function(comment) {
		var path = comment.path;

		if (path) {
			var ancestorIDs = path.split(',');

			var parentID;

			do {
				parentID = ancestorIDs.pop();
			} while (parentID === '');

			if (!(parentID in commentsById)) {
				structured.push(comment);
				return;
			}

			comment.parentID = parentID;

			commentsById[parentID].children = commentsById[parentID].children || [];

			commentsById[parentID].children.push(comment);
		}
		else {
			structured.push(comment);
		}
	});

	return structured;
}

function setPostIsLiked(args) {
	return q(
		BaseStore._knex.select()
			.from(
				BaseStore._knex.raw(
					'posts_set_liked(?, ?, ?) AS "totalLikes"',
					[
						args.userID,
						args.postID,
						args.liked
					]
				)
			)
	).then(function(results) {
		return {
			isLiked: args.liked,
			totalLikes: results[0].totalLikes
		};
	});
}

function postModelFromResult(result) {
	return new PostModel(
		_.extend(
			result,
			{
				id: parseInt(result.id),
				likes: result.likes
			}
		)
	);
}

function populateResults(results) {
	return q.all([
		UsersStore.instance.getUsers(
			{
				ids: _.pluck(results, 'postedBy'),
				includeDeleted: true
			}
		),
		BaseStore._knex.select(
			'postId',
			'likedBy'
		).from('postLikes')
		.whereIn('postLikes.postId', _.unique(_.pluck(results, 'id')))
	]).spread(function(users, likes) {
		var usersByID = {};

		_.forEach(
			users,
			function(user) {
				usersByID[user.id] = user;
			}
		);

		_.forEach(results, function(post) {
			post.postedBy = usersByID[post.postedBy];
			post.likes = _.map(
				_.pluck(
					_.filter(
						likes,
						{postId: post.id}
					),
					'likedBy'
				),
				function (id) {
					return parseInt(id, 10);
				}
			);
		});

		return _.map(results, postModelFromResult);
	});
}

function PostStore() {}

PostStore.prototype = Object.create(BaseStore.prototype, {
	Schema: {
		enumerable: true,
		value: PostSchema
	},
	
	tableName: {
		enumerable: true,
		value: 'posts'
	},

	addPost: {
		enumerable: true,
		value: function addPost(post, options) {
			var store = this;

			options = options || {};

			var error, errors, postData = post;

			if (_.isFunction(post.validate)) {
				errors = post.validate();

				if (errors) {
					error = new Error('Post model is invalid: ' + errors.join("\n"));
					error.errors = errors;

					throw error;
				}

				postData = post.toJSON();
			}

			if (post.postedBy) {
				postData.postedBy = post.postedBy.id;
			}

			options.filters = options.filters || {};

			options.filters.whereNotExists = options.filters.whereNotExists || [];

			if (!_.contains(options.filters.whereNotExists, 'facebookId')) {
				options.filters.whereNotExists.push('facebookId');
			}

			return store.add(postData, options)
				.then(
					function(posts) {
						return posts && posts.length > 0 ? posts[0] : undefined;
					}
				);
		}
	},

	addPosts: {
		enumerable: true,
		value: function(posts, options) {
			var store = this;

			options = options || {};

			return q.all(
				_.map(
					posts,
					function(post) {
						return store.addPost(post);
					}
				)
			);
		}
	},

	searchPosts: {
		enumerable: true,
		value: function(options) {
			var store = this;

			var query = BaseStore._knex.select([
				store.tableName + '.id',
				store.tableName + '.body',
				store.tableName + '.createdOn',
				store.tableName + '.postedOn',
				store.tableName + '.deletedOn',
				store.tableName + '.updatedOn',
				store.tableName + '.postedBy',
				store.tableName + '.path',
				store.tableName + '.facebookId'
			]).from(
				BaseStore._knex.raw(
					'posts_search_posts(' +
					(
						options.searchTerm ?
						"'" + options.searchTerm.replace("'", "''") + "'" :
						"NULL"
					) + ", " +
					(
						'postedBy' in options ?
						options.postedBy :
						"-1"
					) + ", " +
					(
						options.startDate ?
						"'" + options.startDate + "'" :
						"NULL"
					) + ", " +
					(
						options.endDate ?
						"'" + options.endDate + "'" :
						"NULL"
					) + ", " +
					(
						options.includeQuestions ?
						"true" :
						"false"
					) + ", " +
					(
						options.includeComments ?
						"true" :
						"false"
					) +
					") AS " + store.tableName
				)
			);

			return q(query).then(populateResults);
		}
	},

	getPosts: {
		enumerable: true,
		value: function getPosts(options) {
			var store = this;

			options = options || {};
			options.filters = options.filters || {};

			var orderBy = options.orderBy || {'postedOn': 1};

			var full = options.full;

			delete options.full;

			var structured = options.structured;

			delete options.structured;

			var id = options.id;

			// If we want to get all thread posts, we don't want to limit it to one ID.
			if (full) {
				delete options.id;
			}

			var query = BaseStore._knex.select([
				store.tableName + '.id',
				store.tableName + '.body',
				store.tableName + '.createdOn',
				store.tableName + '.postedOn',
				store.tableName + '.deletedOn',
				store.tableName + '.updatedOn',
				store.tableName + '.postedBy',
				store.tableName + '.path',
				store.tableName + '.facebookId'
			]);

			var sourceTable;

			if (full) {
				query = query.column(store.tableName + '.postedBy');

				var idsToFind = '';

				if (id) {
					idsToFind = "'{" + id + "}'::bigint[]";
				}
				else {
					idsToFind = 'NULL';

					if (options.maxQuestions) {
						idsToFind += ', ' + options.maxQuestions;
					}
				}

				sourceTable = BaseStore._knex.raw('posts_get_posts_full(' + idsToFind + ') AS ' + store.tableName);
			}
			else {
				sourceTable = store.tableName;

				if (id) {
					query.where(store.tableName + '.id', id);
				}
				else {
					if (options.maxQuestions) {
						query.limit(options.maxQuestions);
					}
				}
			}

			query = query.from(sourceTable);

			if ('parentID' in options) {
				if (options.parentID) {
					query = query.where(store.tableName + '.path', 'like', '%,' + options.parentID);
				}
				else if (!full) {
					// No parent ID, we want all, but we only want the initial posts, not their comments
					query = query.whereNull(store.tableName + '.path');
				}
			}

			if (!options.includeDeleted) {
				query = query.whereNull(store.tableName + '.deletedOn');
			}

			_.forIn(options.filters, function(filterValue, filterName) {
				query = query.where(store.tableName + '.' + filterName, filterValue);
			});

			if (options.ids) {
				query = query.whereIn('id', options.ids);
			}

			if (orderBy) {
				_.forIn(orderBy, function(orderDirection, orderColumn) {
					query = query.orderBy(orderColumn, orderColumn < 0 ? 'desc' : 'asc');
				});
			}

			return q(query).then(populateResults).then(
				function(results) {
					return full && structured ? structureComments(results) : results;
				}
			);
		}
	},

	getPost: {
		enumerable: true,
		value: function getPost(options) {
			options = options || {};
			options.filters = options.filters || {};

			if (!('sortBy' in options)) {
				options.sortBy = { id: -1 };
			}

			if (!options.id) {
				options.maxQuestions = 1;
			}

			return this.getPosts(options).then(function(results) {
				// If unstructured, return all results
				if (!options.structured) {
					return results;
				}

				if (results && results.length > 0) {
					return results[0];
				}

				return undefined;
			});
		}
	},

	getLatestPost: {
		enumerable: true,
		value: function(options) {
			var store = this;

			options = options || {};

			delete options.id;

			return store.getPost(options);
		}
	},

	getQuestion: {
		enumerable: true,
		value: function getQuestion(options) {
			var store = this;

			options = options || {};

			options.parentID = null;

			return store.getPost(options);
		}
	},

	getQuestions: {
		enumerable: true,
		value: function getQuestions(options) {
			var store = this;

			options = options || {};

			options.parentID = null;

			return store.getPosts(options);
		}
	},

	getComments: {
		enumerable: true,
		value: function getComments(options) {
			options = options || {};
			options.filters = options.filters || {};

			if (options.parent) {
				options.filters.parent = options.parent;
			}
			else if (options.parentID) {
				options.filters.parentID = options.parentID;
			}
			else if (options.rootID) {
				options.filters.rootID = options.rootID;
			}

			return this.getPosts(options);
		}
	},

	deletePost: {
		enumerable: true,
		value: function deletePost(model, hard) {
			if (hard) {
				return this.remove(model);
			}

			return q(this.softDelete(model));
		}
	},

	undeletePost: {
		enumerable: true,
		value: function undeletePost(model) {
			return this.softUndelete(model);
		}
	},

	likePost: {
		enumerable: true,
		value: function likePost(options) {
			return setPostIsLiked({
				userID: options.user.id,
				postID: options.postID,
				liked: true
			});
		}
	},

	unlikePost: {
		enumerable: true,
		value: function(options) {
			return setPostIsLiked({
				userID: options.user.id,
				postID: options.postID,
				liked: false
			});
		}
	},

	toggleLikePost: {
		enumerable: true,
		value: function(options) {
			return q(
				BaseStore._knex.select()
					.from(BaseStore._knex.raw('posts_toggle_post_like(?, ?)', [options.user.id, options.postID]))
			).then(function(results) {
				return {
					isLiked: results[0].isLiked,
					totalLikes: results[0].totalLikes
				};
			});
		}
	},

	getLikes: {
		enumerable: true,
		value: function(options) {
			var store = this;

			var query = BaseStore._knex.select('likedBy')
				.from('postLikes')
				.where('postLikes.postId', options.postID);

			if (options.idsOnly) {
				return q(query).then(function(results) {
					return _.map(
						_.pluck(results, 'likedBy'),
						function(id) {
							return parseInt(id, 10);
						}
					);
				});
			}

			return UsersStore.instance.getUsers({
				idQuery: query
			});
		}
	}
});


var _instance;

Object.defineProperties(PostStore, {
	instance: {
		enumerable: true,
		get: function() {
			if (!_instance) {
				_instance = new PostStore();
			}

			return _instance;
		}
	}
});

module.exports = PostStore;
