"use strict";

module.exports = {
	"sex": {
		type: String,
			enum: {
			values: ["M", "F"],
			message: "enum validator failed for path `{PATH}` with value `{VALUE}` (must be 'M' or 'F')",
		},
	},
};
