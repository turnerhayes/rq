"use strict";

var BaseSchema = require('./base');
var Enumerations = require('./enumerations');

var userSchema = new BaseSchema(
	{
		name: {
			first: {
				type: String,
				required: true,
			},
			middle: {
				type: String,
			},
			last: {
				type: String,
			},
		},
		username: {
			type: String,
		},
		preferredDisplayName: {
			type: String,
			"default": null
		},
		sex: Enumerations.sex,
		email: {
			type: String,
			match: [/[^@]+@.+/, "{VALUE} is not a valid email address"],
		},
		profilePhotoURL: {
			type: String,
		},
		facebookId: {
			type: String,
		},
	},
	{
		dateFields: true
	}
);

userSchema.virtual('displayName').get(
	function getDisplayName() {
		if (this.preferredDisplayName) {
			return this.preferredDisplayName;
		}

		return this.name.first + ' ' + this.name.last;
	}
);

module.exports = userSchema;

