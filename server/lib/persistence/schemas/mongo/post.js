"use strict";

var mongoose = require('mongoose');
var BaseSchema = require('./base');

var postSchema = new BaseSchema(
	{
		title: {
			type: String
		},
		body: {
			type: String,
			required: true
		},
		posted_by: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'User',
			required: true
		},
		parent: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Post',
			"default": null
		},
		root: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Post',
			"default": null
		},
	},
	{
		dateFields: true
	}
);

postSchema.virtual('posted_date').get(function() {
	return this.created_date;
});

postSchema.virtual('comments')
	.get(function() {
		return this._comments || [];
	}
).set(
	function(value) {
		this._comments = value;
	}
);

module.exports = postSchema;
