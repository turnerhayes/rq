"use strict";

var mongoose = require('mongoose');

function BaseSchema(schemaDefinition, options) {
	var schema = this;

	options = options || {};

	options.toObject = {
		virtuals: true
	};

	options.toJSON = {
		virtuals: true
	};

	var dateFields = options.dateFields;

	delete options.dateFields;

	if (dateFields) {
		if (!('updated_date' in schemaDefinition)) {
			schemaDefinition.updated_date = {
				type: Date,
				"default": Date.now
			};

			schemaDefinition.deleted_date = {
				type: Date,
				"default": null
			};
		}
	}

	mongoose.Schema.call(schema, schemaDefinition, options);

	if (dateFields) {
		schema.pre('save', function(next) {
			this.updated_date = Date.now();

			next();
		});

		schema.virtual('created_date').get(function() {
			return this._id.getTimestamp();
		});
	}
}

BaseSchema.prototype = Object.create(mongoose.Schema.prototype);

module.exports = BaseSchema;
