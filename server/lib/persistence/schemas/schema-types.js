"use strict";

module.exports = {
	"Long": "bigint",
	"Integer": "int",
	"String": "text",
	"Date": "timestamp with timezone",
};
