"use strict";

var _ = require('lodash');
var SchemaTypes = require('./schema-types');

var SchemaUtils = {};

Object.defineProperties(
	SchemaUtils,
	{
		DBTypeForSchemaType: {
			enumerable: true,
			value: function(schemaType) {
				var type = SchemaTypes[schemaType];
				var referencedSchema;

				if (!_.isString(type)) {
					referencedSchema = require('./' + schemaType.schema);
					schemaType = referencedSchema.columns[schemaType.remoteColumn].type;
					type = SchemaTypes[schemaType];
				}

				if (_.isUndefined(type)) {
					throw new Error('Schema type "' + schemaType + '" is not defined');
				}

				return type;
			}
		}
	}
);

exports = module.exports = SchemaUtils;
