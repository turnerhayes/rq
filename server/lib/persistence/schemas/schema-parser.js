"use strict";

var _ = require('lodash');
var fs = require('q-io/fs');
var SchemaTypes = require('./schema-types');

var _schemaTypeToKnexMethod = {};

_schemaTypeToKnexMethod[SchemaTypes.Long] = {
	method: 'bigInteger'
};

_schemaTypeToKnexMethod[SchemaTypes.Integer] = {
	method: 'integer'
};

_schemaTypeToKnexMethod[SchemaTypes.String] = {
	method: 'text'
};

_schemaTypeToKnexMethod[SchemaTypes.Date] = {
	method: 'date'
};

_schemaTypeToKnexMethod[SchemaTypes.Timestamp] = {
	method: 'timestamp'
};

function _defaultValueToSQL(column, type, deflt) {
	/* jshint validthis: true */

	var parser = this;

	if ([SchemaTypes.Date, SchemaTypes.Timestamp].indexOf(type) >= 0) {
		if (deflt === "<now>") {
			column.defaultTo(parser.knex.raw('now()'));
			return;
		}
	}

	column.defaultTo(deflt);
}

function SchemaParser(options) {
	var parser = this;

	options = options || {};

	Object.defineProperties(parser, {

	});
}

SchemaParser.prototype = Object.create(Object.prototype, {
	_toKnexQuery: {
		value: function(knex, schema) {
			var parser = this;

			var query = knex.schema.createTable(schema.name, function(table) {
				_.forOwn(schema.columns, function(columnDefinition, columnName) {
					var typeMap = _schemaTypeToKnexMethod[columnDefinition.type];

					var args = _.clone(typeMap.args) || [];

					args.unshift(columnName);

					var column = table[typeMap.method].apply(table, args);

					if (columnDefinition.required) {
						column.notNullable();
					}

					if (columnDefinition["default"]) {
						_defaultValueToSQL.call(parser, column, columnDefinition.type, columnDefinition["default"]);
					}
				});
			});

			return query;
		}
	},

	_getSchemaFromFile: {
		value: function (filename) {
			return fs.read(filename).then(function(data) {
				var schema = JSON.parse(data);

				_.forOwn(schema.columns, function(columnDefinition) {
					columnDefinition.type = SchemaTypes[columnDefinition.type];
				});

				return schema;
			});
		}
	}
});

module.exports = SchemaParser;
