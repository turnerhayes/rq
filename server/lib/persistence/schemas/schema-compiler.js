"use strict";

var Q = require('q');
var knexBuilder = require('knex');
var SchemaParser = require('./schema-parser');

function SchemaCompiler(options) {
	var compiler = this;

	options = options || {};

	var _knex = knexBuilder({
		client: 'pg',
		connection: options.connection,
		debug: !!options.debug
	});

	Object.defineProperties(compiler, {
		knex: {
			value: _knex
		}
	});

	return Object.seal(compiler);
}

SchemaCompiler.prototype = Object.create(SchemaParser.prototype, {
	generate: {
		enumerable: true,
		value: function(schema) {
			/* jshint newcap: false */

			return Q(this._toKnexQuery(this.knex, schema).toSQL()[0].sql);
		}
	},
	generateFromFile: {
		enumerable: true,
		value: function(filename) {
			var compiler = this;

			return compiler._getSchemaFromFile(filename).then(compiler.generate.bind(compiler));
		}
	}
});

module.exports = SchemaCompiler;
