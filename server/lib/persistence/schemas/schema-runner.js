"use strict";

var knexBuilder = require('knex');
var Q = require('q');
var SchemaParser = require('./schema-parser');

function SchemaRunner(options) {
	var runner = this;

	options = options || {};

	var _knex = knexBuilder({
		client: 'pg',
		connection: options.connection,
		debug: !!options.debug
	});

	Object.defineProperties(runner, {
		knex: {
			value: _knex
		}
	});

	return Object.seal(runner);
}

SchemaRunner.prototype = Object.create(SchemaParser.prototype, {
	execute: {
		enumerable: true,
		value: function(schema) {
			/* jshint newcap: false */

			var runner = this;

			// if (!compiler.knex.connection()) {
			// 	throw new Error('No connection specified for this schema compiler; cannot execute schema');
			// }

			return Q(runner._toKnexQuery(runner.knex, schema));
		}
	},

	executeFromFile: {
		enumerable: true,
		value: function(filename) {
			var runner = this;

			return runner._getSchemaFromFile(filename).then(runner.execute.bind(runner));
		}
	}
});

module.exports = SchemaRunner;
