"use strict";

var	fs           = require('fs');
var path         = require('path');
var express      = require('express');
var serveStatic  = require('serve-static');
var errorhandler = require('errorhandler');
var morgan       = require('morgan');
var log4js       = require('log4js');
var pathsConfig  = require('../config/paths');
var Manchu       = require('../lib/manchu');


var app = express();

var manchuMimeType = 'text/plain+handlebars';

var customMime = {};

customMime[manchuMimeType] = ['manchu'];

express.static.mime.define(customMime);

app.use(
	morgan(
		'combined',
		{
			stream: fs.createWriteStream(path.join(pathsConfig.logs, 'static-access.log'), { flags: 'a' })
		}
	)
);

app.use(
	function(req, res, next) {
		res.set({
			"Access-Control-Allow-Origin": "*",
			"Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept",
			"Access-Control-Allow-Methods": "GET,OPTIONS"
		});

		next();
	}
);

app.use(serveStatic(pathsConfig.static));
app.use(errorhandler());

app.route(/template-compiled\:(.+)(?:\.js)?$/)
	.get(
		function(req, res, next) {
			var templateName = req.param(0);

			Manchu.getPartial(templateName).done(
				function(partial) {
					res.type('text/javascript');
					res.send(partial.toString());
				},
				function(err) {
					res.status(500);
					res.send('' + err);
				}
			);
		}
	);


app.route(/\.manchu\.js$/)
	.get(
		function(req, res) {
			var filePath = path.join(pathsConfig.static, req.path.replace(/\.js$/, ''));

			res.type(manchuMimeType);
			res.sendFile(filePath);
		}
	);

app.route('*')
	.options(
		function(req, res, next) {
			res.set({
				"Allow": "GET,OPTIONS"
			});
			res.send('GET,OPTIONS');

			next();
		}
	);

log4js.configure(path.resolve(__dirname, '../config/log4js.json'), { cwd: pathsConfig.logs });

exports = module.exports = {
	app: app
};
