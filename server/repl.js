/* jshint node: true */

"use strict";

var repl = require('repl');

var replServer = repl.start({
	prompt: "rq-server> "
});

replServer.context.UserModel = require('./lib/persistence/models/user');
replServer.context.PostModel = require('./lib/persistence/models/post');
replServer.context.UserSchema = require('./lib/persistence/schemas/user');
replServer.context.PostSchema = require('./lib/persistence/schemas/post');
replServer.context.UserStore = require('./lib/persistence/stores/users');
replServer.context.PostsStore = require('./lib/persistence/stores/posts');
