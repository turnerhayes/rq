// jshint node: true

"use strict";

var https = require('https');
var FB = require('fb');

var request = require('request');
var requestify = require('requestify');
var jar = request.jar();

request = request.defaults({
	jar: jar
});

var cookies = [
	{
		"domain": ".developers.facebook.com",
		"expirationDate": 1495656420,
		"hostOnly": false,
		"httpOnly": false,
		"name": "_ga",
		"path": "/",
		"secure": false,
		"session": false,
		"storeId": "0",
		"value": "GA1.3.1584698822.1419626992",
		"id": 1
	},
	{
		"domain": ".developers.facebook.com",
		"expirationDate": 1432585020,
		"hostOnly": false,
		"httpOnly": false,
		"name": "_gat",
		"path": "/",
		"secure": false,
		"session": false,
		"storeId": "0",
		"value": "1",
		"id": 2
	},
	{
		"domain": ".facebook.com",
		"hostOnly": false,
		"httpOnly": false,
		"name": "act",
		"path": "/",
		"secure": true,
		"session": true,
		"storeId": "0",
		"value": "1432584428813%2F0",
		"id": 3
	},
	{
		"domain": ".facebook.com",
		"expirationDate": 1440360428.627765,
		"hostOnly": false,
		"httpOnly": false,
		"name": "c_user",
		"path": "/",
		"secure": true,
		"session": false,
		"storeId": "0",
		"value": "4200280",
		"id": 4
	},
	{
		"domain": ".facebook.com",
		"expirationDate": 1440360428.627656,
		"hostOnly": false,
		"httpOnly": false,
		"name": "csm",
		"path": "/",
		"secure": true,
		"session": false,
		"storeId": "0",
		"value": "2",
		"id": 5
	},
	{
		"domain": ".facebook.com",
		"expirationDate": 1488913112.282768,
		"hostOnly": false,
		"httpOnly": true,
		"name": "datr",
		"path": "/",
		"secure": false,
		"session": false,
		"storeId": "0",
		"value": "FfV_VN8flLwplBTZiClW5d8K",
		"id": 6
	},
	{
		"domain": ".facebook.com",
		"expirationDate": 1440360428.62771,
		"hostOnly": false,
		"httpOnly": true,
		"name": "fr",
		"path": "/",
		"secure": true,
		"session": false,
		"storeId": "0",
		"value": "06rQI813HnIZKMi6h.AWW95QzKhulEYIFgno9L_PY7r6s.BU_JvW.bR.AAA.0.AWVGki9L",
		"id": 7
	},
	{
		"domain": ".facebook.com",
		"expirationDate": 1433184030.487308,
		"hostOnly": false,
		"httpOnly": false,
		"name": "locale",
		"path": "/",
		"secure": true,
		"session": false,
		"storeId": "0",
		"value": "en_US",
		"id": 8
	},
	{
		"domain": ".facebook.com",
		"expirationDate": 1495652808.34901,
		"hostOnly": false,
		"httpOnly": true,
		"name": "lu",
		"path": "/",
		"secure": true,
		"session": false,
		"storeId": "0",
		"value": "gAi-ixhnl4FvMYyNZy15ya5g",
		"id": 9
	},
	{
		"domain": ".facebook.com",
		"hostOnly": false,
		"httpOnly": false,
		"name": "p",
		"path": "/",
		"secure": true,
		"session": true,
		"storeId": "0",
		"value": "-2",
		"id": 10
	},
	{
		"domain": ".facebook.com",
		"hostOnly": false,
		"httpOnly": false,
		"name": "presence",
		"path": "/",
		"secure": true,
		"session": true,
		"storeId": "0",
		"value": "EM432583112EuserFA24200280A2EstateFDsb2F0Et2F_5b_5dElm2FnullEuct2F1432582366BEtrFnullEtwF658588981EatF143258298B5G432583112311CEchFDp_5f4200280F9CC",
		"id": 11
	},
	{
		"domain": ".facebook.com",
		"expirationDate": 1440360428.6276,
		"hostOnly": false,
		"httpOnly": true,
		"name": "s",
		"path": "/",
		"secure": true,
		"session": false,
		"storeId": "0",
		"value": "Aa78VPwSdQNm5j5o.BVY3LI",
		"id": 12
	},
	{
		"domain": ".facebook.com",
		"hostOnly": false,
		"httpOnly": false,
		"name": "wd",
		"path": "/",
		"secure": true,
		"session": true,
		"storeId": "0",
		"value": "1855x971",
		"id": 13
	},
	{
		"domain": ".facebook.com",
		"expirationDate": 1440360428.627387,
		"hostOnly": false,
		"httpOnly": true,
		"name": "xs",
		"path": "/",
		"secure": true,
		"session": false,
		"storeId": "0",
		"value": "10%3A_0n9eeBXFGEjHw%3A2%3A1432580807%3A17627",
		"id": 14
	}
];

// for (var i = 0, len = cookies.length; i < len; i++) {
// 	var cookie = cookies[i];

// 	jar.setCookie(cookie);
// }

var options = {
	hostname: 'www.facebook.com',
	method: 'GET',
	path: 'app_scoped_id/10103194369939089',
	followRedirect: true,
	headers: {
		'User-Agent': 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13'
	}
};

requestify.get("https://www.facebook.com/app_scoped_id/10103194369939089", {
	cookies: cookies,
	headers: {
		'User-Agent': 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13'
	}
}).then(
	function(response) {
		console.log(response.body);
	}
);

// request(
// 	options,
// 	function(error, response, body) {
// 		if (error) {
// 			throw new Error(error);
// 		}

// 		console.log(body);
// 	}
// );

// var req = https.request(options, function(res) {
// 	console.log("statusCode: ", res.statusCode);
// 	console.log("headers: ", res.headers);

// 	res.on('data', function(d) {
// 		process.stdout.write(d);
// 	});
// });

// req.on('error', function(e) {
// 	console.error(e);
// });

// req.end();
