--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: adminpack; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS adminpack WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION adminpack; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION adminpack IS 'administrative functions for PostgreSQL';


--
-- Name: pldbgapi; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS pldbgapi WITH SCHEMA public;


--
-- Name: EXTENSION pldbgapi; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pldbgapi IS 'server-side support for debugging PL/pgSQL functions';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: notifications; Type: TABLE; Schema: public; Owner: rq; Tablespace: 
--

CREATE TABLE notifications (
    id bigint NOT NULL,
    type text NOT NULL,
    "subjectPostId" bigint,
    "relatedPostId" bigint,
    "createdOn" timestamp with time zone DEFAULT now() NOT NULL,
    "postedBy" bigint,
    "deletedOn" timestamp with time zone
);


ALTER TABLE public.notifications OWNER TO rq;

--
-- Name: notifications_add_notification(text, bigint, bigint, bigint, bigint[]); Type: FUNCTION; Schema: public; Owner: rq
--

CREATE FUNCTION notifications_add_notification(notification_type text, subject_post_id bigint DEFAULT NULL::bigint, related_post_id bigint DEFAULT NULL::bigint, posted_by bigint DEFAULT NULL::bigint, users_to_notify bigint[] DEFAULT '{}'::bigint[]) RETURNS SETOF notifications
    LANGUAGE plpgsql
    AS $$

DECLARE
	notification_id bigint;
	"user_id" bigint;
BEGIN

notification_id = (
	SELECT
		"notifications"."id"
	FROM
		"notifications"
	WHERE
		"notifications"."type" = "notification_type"
		AND (
			("notifications"."subjectPostId" IS NULL AND "subject_post_id" IS NULL)
			OR "notifications"."subjectPostId" = "subject_post_id"
		)
		AND (
			("notifications"."relatedPostId" IS NULL AND "related_post_id" IS NULL)
			OR ("notifications"."relatedPostId" = "related_post_id")
		)
		AND (
			("notifications"."postedBy" IS NULL AND "posted_by" IS NULL)
			OR "notifications"."postedBy" = "posted_by"
		)
);

IF "notification_id" IS NULL
THEN	
	INSERT INTO "notifications"
	(
		"type",
		"subjectPostId",
		"relatedPostId",
		"postedBy"
	)
	VALUES
	(
		"notification_type",
		"subject_post_id",
		"related_post_id",
		"posted_by"
	)
	RETURNING "id" INTO "notification_id"
	;
END IF;

FOREACH "user_id" IN ARRAY "users_to_notify"
LOOP
	INSERT INTO "userNotifications"
	(
		"userId",
		"notificationId"
	)
	SELECT
		"user_id",
		"notification_id"
	WHERE NOT EXISTS (
		SELECT
			1
		FROM
			"userNotifications"
		WHERE
			"userNotifications"."userId" = "user_id"
			AND "userNotifications"."notificationId" = "notification_id"
	);
END LOOP;

RETURN QUERY SELECT
	"notifications".*
FROM
	"notifications"
WHERE
	"notifications"."id" = "notification_id"
;

END;

$$;


ALTER FUNCTION public.notifications_add_notification(notification_type text, subject_post_id bigint, related_post_id bigint, posted_by bigint, users_to_notify bigint[]) OWNER TO rq;

--
-- Name: notifications_get_user_notifications(bigint); Type: FUNCTION; Schema: public; Owner: rq
--

CREATE FUNCTION notifications_get_user_notifications(user_id bigint) RETURNS TABLE(id bigint, type text, "subjectPostId" bigint, "relatedPostId" bigint, "createdOn" timestamp with time zone, "deletedOn" timestamp with time zone, "postedBy" bigint, seen boolean)
    LANGUAGE plpgsql
    AS $$

BEGIN

RETURN QUERY
	SELECT
		"notifications"."id",
		"notifications"."type",
		"notifications"."subjectPostId",
		"notifications"."relatedPostId",
		"notifications"."createdOn",
		"notifications"."deletedOn",
		"notifications"."postedBy",
		"userNotifications"."seen"
	FROM
		"notifications"
		INNER JOIN "userNotifications"
			ON "userNotifications"."notificationId" = "notifications"."id"
	WHERE
		"userNotifications"."userId" = "user_id"
;

END;

$$;


ALTER FUNCTION public.notifications_get_user_notifications(user_id bigint) OWNER TO rq;

--
-- Name: notifications_remove_notification(bigint, text, bigint, bigint, bigint, boolean); Type: FUNCTION; Schema: public; Owner: rq
--

CREATE FUNCTION notifications_remove_notification(notification_id bigint DEFAULT NULL::bigint, notification_type text DEFAULT NULL::text, posted_by bigint DEFAULT NULL::bigint, subject_post_id bigint DEFAULT NULL::bigint, related_post_id bigint DEFAULT NULL::bigint, unseen_only boolean DEFAULT true) RETURNS boolean
    LANGUAGE plpgsql
    AS $$

BEGIN

IF notification_id IS NULL
THEN
	notification_id = (
		SELECT
			"id"
		FROM
			"notifications"
		WHERE
			"notifications"."type" = notification_type
			AND (
				("notifications"."postedBy" IS NULL AND posted_by IS NULL)
				OR "notifications"."postedBy" = posted_by
			)
			AND (
				("notifications"."subjectPostId" IS NULL AND subject_post_id IS NULL)
				OR "notifications"."subjectPostId" = subject_post_id
			)
			AND (
				("notifications"."relatedPostId" IS NULL AND related_post_id IS NULL)
				OR "notifications"."relatedPostId" = related_post_id
			)
	);
END IF;

IF notification_id IS NULL
THEN
	RETURN false;
END IF;


DELETE FROM "userNotifications"
WHERE "userNotifications"."notificationId" = notification_id
	AND (
		NOT "unseen_only"
		OR (NOT "userNotifications"."seen")
	)
;

IF (
	SELECT
		COUNT(*)
	FROM
		"userNotifications"
	WHERE
		"userNotifications"."notificationId" = notification_id
) = 0
THEN
	DELETE FROM "notifications"
	WHERE
		"notifications"."id" = notification_id
	;

	RETURN true;
END IF;

RETURN FALSE;

END;

$$;


ALTER FUNCTION public.notifications_remove_notification(notification_id bigint, notification_type text, posted_by bigint, subject_post_id bigint, related_post_id bigint, unseen_only boolean) OWNER TO rq;

--
-- Name: notifications_remove_user_notification(bigint, bigint, bigint, text, bigint, bigint); Type: FUNCTION; Schema: public; Owner: rq
--

CREATE FUNCTION notifications_remove_user_notification(user_id bigint, notification_id bigint DEFAULT NULL::bigint, posted_by bigint DEFAULT NULL::bigint, notification_type text DEFAULT NULL::text, subject_post_id bigint DEFAULT NULL::bigint, related_post_id bigint DEFAULT NULL::bigint) RETURNS boolean
    LANGUAGE plpgsql
    AS $$

BEGIN

IF notification_id IS NULL
THEN
	notification_id = (
		SELECT
			"id"
		FROM
			"notifications"
		WHERE
			"notifications"."type" = notification_type
			AND (
				("notifications"."subjectPostId" IS NULL AND "subject_post_id" IS NULL)
				OR "notifications"."subjectPostId" = subject_post_id
			)
			AND (
				("notifications"."relatedPostId" IS NULL AND related_post_id IS NULL)
				OR "notifications"."relatedPostId" = related_post_id
			)
			AND (
				("notifications"."postedBy" IS NULL AND posted_by IS NULL)
				OR "notifications"."postedBy" = posted_by
			)
	);
END IF;

IF notification_id IS NULL
THEN
	RETURN false;
END IF;

DELETE FROM "userNotifications"
WHERE "userNotifications"."notificationId" = notification_id
	AND "userNotifications"."userId" = user_id
;

IF (
	SELECT
		COUNT(*)
	FROM
		"userNotifications"
	WHERE
		"userNotifications"."notificationId" = notification_id
) = 0
THEN
	DELETE FROM "notifications"
	WHERE
		"notifications"."id" = notification_id
	;

	RETURN true;
END IF;

RETURN FALSE;

END;

$$;


ALTER FUNCTION public.notifications_remove_user_notification(user_id bigint, notification_id bigint, posted_by bigint, notification_type text, subject_post_id bigint, related_post_id bigint) OWNER TO rq;

--
-- Name: posts; Type: TABLE; Schema: public; Owner: rq; Tablespace: 
--

CREATE TABLE posts (
    id bigint NOT NULL,
    body text NOT NULL,
    "postedBy" bigint,
    "deletedOn" timestamp with time zone,
    "createdOn" timestamp with time zone DEFAULT now() NOT NULL,
    "updatedOn" timestamp with time zone,
    "postedOn" timestamp with time zone,
    path text,
    "facebookId" bigint,
    body_tsvector tsvector,
    CONSTRAINT posts_path_valid_format CHECK (((path ~ '^,(\d+,)+$'::text) OR (path IS NULL)))
);


ALTER TABLE public.posts OWNER TO rq;

--
-- Name: posts_add_posts(posts[]); Type: FUNCTION; Schema: public; Owner: rq
--

CREATE FUNCTION posts_add_posts(posts_to_insert posts[]) RETURNS SETOF posts
    LANGUAGE plpgsql
    AS $$
BEGIN
	INSERT INTO "posts"
		("body", "postedBy", "deletedOn", "createdOn", "updatedOn", "postedOn", "path", "facebookId")
	SELECT
		"body",
		"postedBy",
		"deletedOn",
		"createdOn",
		"updatedOn",
		"postedOn",
		"path",
		"facebookId"
	FROM
		"posts_to_insert"
	WHERE
		NOT EXISTS (
			SELECT
				1
			FROM
				"posts"
			WHERE
				"posts"."facebookId" = "posts_to_insert"."facebookId"
		)
	RETURNING *
	;
END;
$$;


ALTER FUNCTION public.posts_add_posts(posts_to_insert posts[]) OWNER TO rq;

--
-- Name: posts_get_posts_full(bigint[], integer); Type: FUNCTION; Schema: public; Owner: rq
--

CREATE FUNCTION posts_get_posts_full(postids bigint[] DEFAULT NULL::bigint[], maxquestions integer DEFAULT NULL::integer) RETURNS SETOF posts
    LANGUAGE plpgsql
    AS $$

DECLARE questionIDs bigint[];

BEGIN

IF postIDs IS NULL
THEN
	questionIDs := array(
		SELECT
			posts.id
		FROM
			posts
		WHERE
			posts.path IS NULL
			AND (
				postIDs IS NULL
				OR posts.id = ANY(postIDs)
			)
		LIMIT
			maxQuestions
	);
ELSE
	questionIDs := postIDs;
END IF;


RETURN QUERY SELECT
	posts.*
FROM
	posts
WHERE
	posts.id = ANY(questionIDs)
	OR (string_to_array(trim(both ',' from posts.path), ',')::bigint[] && questionIDs)
;

END;
$$;


ALTER FUNCTION public.posts_get_posts_full(postids bigint[], maxquestions integer) OWNER TO rq;

--
-- Name: posts_search_posts(text, bigint, date, date, boolean, boolean); Type: FUNCTION; Schema: public; Owner: rq
--

CREATE FUNCTION posts_search_posts(search_term text DEFAULT NULL::text, posted_by bigint DEFAULT (-1), start_date date DEFAULT NULL::date, end_date date DEFAULT NULL::date, include_questions boolean DEFAULT true, include_comments boolean DEFAULT true) RETURNS SETOF posts
    LANGUAGE plpgsql
    AS $$
BEGIN

RETURN QUERY SELECT
	*
FROM
	"posts"
WHERE
	"posts"."deletedOn" IS NULL
	AND (
		"search_term" IS NULL
		OR "posts"."body" @@ to_tsquery("search_term")
	)
	AND (
		"posted_by" = -1
		OR "posts"."postedBy" = "posted_by"
		OR (
			"posts"."postedBy" IS NULL AND "posted_by" IS NULL
		)
	)
	AND (
		"start_date" IS NULL
		OR "posts"."postedOn"::date >= "start_date"
	)
	AND (
		"end_date" IS NULL
		OR "posts"."postedOn"::date <= "end_date"
	)
	AND (
		(
			"include_questions" = true
			AND "posts"."path" IS NULL
		)
		OR (
			"include_comments" = true
			AND "posts"."path" IS NOT NULL
		)
	)
;

END
$$;


ALTER FUNCTION public.posts_search_posts(search_term text, posted_by bigint, start_date date, end_date date, include_questions boolean, include_comments boolean) OWNER TO rq;

--
-- Name: posts_set_liked(bigint, bigint, boolean); Type: FUNCTION; Schema: public; Owner: rq
--

CREATE FUNCTION posts_set_liked(user_id bigint, post_id bigint, liked boolean) RETURNS integer
    LANGUAGE plpgsql
    AS $$

BEGIN

IF "liked"
THEN
	INSERT INTO "postLikes"
		("likedBy", "postId")
	SELECT
		"user_id",
		"post_id"
	WHERE
		NOT EXISTS (
			SELECT
				1
			FROM
				"postLikes"
			WHERE
				"postLikes"."postId" = "post_id"
				AND "postLikes"."likedBy" = "user_id"
		)
	;
ELSE
	DELETE FROM "postLikes"
	WHERE
		"postLikes"."postId" = "post_id"
		AND "postLikes"."likedBy" = "user_id";
END IF;

RETURN (SELECT COUNT(*) FROM "postLikes" WHERE "postLikes"."postId" = "post_id");

END;

$$;


ALTER FUNCTION public.posts_set_liked(user_id bigint, post_id bigint, liked boolean) OWNER TO rq;

--
-- Name: posts_toggle_post_like(bigint, bigint); Type: FUNCTION; Schema: public; Owner: rq
--

CREATE FUNCTION posts_toggle_post_like(user_id bigint, post_id bigint) RETURNS TABLE("isLiked" boolean, "totalLikes" integer)
    LANGUAGE plpgsql
    AS $$

DECLARE
	"wasDeleted" BOOLEAN;
BEGIN

WITH results AS (
	DELETE FROM "postLikes"
	WHERE
		"postLikes"."likedBy" = "user_id"
		AND "postLikes"."postId" = "post_id"
	RETURNING 1
)
SELECT COUNT(*) = 1 FROM results INTO "wasDeleted";

-- Nothing was deleted; we need to add the like
IF NOT "wasDeleted"
THEN
	INSERT INTO "postLikes"
		("postId", "likedBy")
	VALUES
		(post_id, user_id)
	;
END IF;

RETURN QUERY (
	SELECT
		NOT "wasDeleted",
		COUNT("postLikes".*)::INTEGER
	FROM
		"postLikes"
	WHERE
		"postLikes"."postId" = "post_id"
);

END;

$$;


ALTER FUNCTION public.posts_toggle_post_like(user_id bigint, post_id bigint) OWNER TO rq;

--
-- Name: trigger_posts_default_posted_on(); Type: FUNCTION; Schema: public; Owner: rq
--

CREATE FUNCTION trigger_posts_default_posted_on() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
IF NEW."postedOn" IS NULL AND NEW."createdOn" IS NOT NULL THEN
	NEW."postedOn" = NEW."createdOn";
END IF;

RETURN NEW;
END;
$$;


ALTER FUNCTION public.trigger_posts_default_posted_on() OWNER TO rq;

--
-- Name: notifications_id_seq; Type: SEQUENCE; Schema: public; Owner: rq
--

CREATE SEQUENCE notifications_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.notifications_id_seq OWNER TO rq;

--
-- Name: notifications_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rq
--

ALTER SEQUENCE notifications_id_seq OWNED BY notifications.id;


--
-- Name: postLikes; Type: TABLE; Schema: public; Owner: rq; Tablespace: 
--

CREATE TABLE "postLikes" (
    "postId" bigint NOT NULL,
    "likedBy" bigint NOT NULL
);


ALTER TABLE public."postLikes" OWNER TO rq;

--
-- Name: TABLE "postLikes"; Type: COMMENT; Schema: public; Owner: rq
--

COMMENT ON TABLE "postLikes" IS 'Maps likes to posts';


--
-- Name: posts_id_seq; Type: SEQUENCE; Schema: public; Owner: rq
--

CREATE SEQUENCE posts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.posts_id_seq OWNER TO rq;

--
-- Name: posts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rq
--

ALTER SEQUENCE posts_id_seq OWNED BY posts.id;


--
-- Name: userNotifications; Type: TABLE; Schema: public; Owner: rq; Tablespace: 
--

CREATE TABLE "userNotifications" (
    "userId" bigint NOT NULL,
    "notificationId" bigint NOT NULL,
    seen boolean DEFAULT false NOT NULL
);


ALTER TABLE public."userNotifications" OWNER TO rq;

--
-- Name: TABLE "userNotifications"; Type: COMMENT; Schema: public; Owner: rq
--

COMMENT ON TABLE "userNotifications" IS 'Maps pending notifications to users';


--
-- Name: users; Type: TABLE; Schema: public; Owner: rq; Tablespace: 
--

CREATE TABLE users (
    id bigint NOT NULL,
    username text,
    "deletedOn" timestamp with time zone,
    "firstName" text NOT NULL,
    "middleName" text,
    "lastName" text NOT NULL,
    "facebookId" bigint,
    email text,
    "createdOn" timestamp with time zone DEFAULT now() NOT NULL,
    "updatedOn" time with time zone,
    "profilePhotoURL" text,
    "preferredDisplayName" text,
    "isAdmin" boolean DEFAULT false NOT NULL,
    "hasLoggedIn" boolean DEFAULT false NOT NULL
);


ALTER TABLE public.users OWNER TO rq;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: rq
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO rq;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rq
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rq
--

ALTER TABLE ONLY notifications ALTER COLUMN id SET DEFAULT nextval('notifications_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rq
--

ALTER TABLE ONLY posts ALTER COLUMN id SET DEFAULT nextval('posts_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: rq
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Data for Name: notifications; Type: TABLE DATA; Schema: public; Owner: rq
--

COPY notifications (id, type, "subjectPostId", "relatedPostId", "createdOn", "postedBy", "deletedOn") FROM stdin;
1	comment	1	2	2015-02-26 20:53:17.960439-05	2	\N
2	like	1	\N	2015-02-27 01:28:52.768624-05	2	\N
54	like	3	\N	2015-03-01 22:14:17.186136-05	2	\N
75	like	4	\N	2015-03-08 14:59:18.48964-04	2	\N
78	like	2	\N	2015-03-09 22:24:21.65965-04	2	\N
80	like	9	\N	2015-03-21 11:08:06.434441-04	2	\N
81	comment	\N	10	2015-03-21 12:33:53.295343-04	3	\N
83	comment	\N	22	2015-05-30 19:36:06.599951-04	2	\N
84	comment	\N	23	2015-05-30 19:42:32.568617-04	2	\N
85	comment	\N	24	2015-05-30 19:47:12.951444-04	2	\N
86	comment	\N	25	2015-05-30 19:51:26.605889-04	2	\N
87	comment	\N	26	2015-05-30 20:09:27.081335-04	2	\N
88	comment	\N	27	2015-05-30 20:09:46.57092-04	2	\N
89	comment	\N	28	2015-05-30 20:11:23.412607-04	2	\N
90	comment	\N	29	2015-05-30 20:11:39.364278-04	2	\N
91	comment	\N	30	2015-05-30 20:12:48.331121-04	2	\N
92	comment	\N	31	2015-05-30 20:13:00.959466-04	2	\N
93	comment	\N	32	2015-06-11 22:52:47.357841-04	2	\N
94	comment	\N	47	2015-06-14 17:23:15.692504-04	\N	\N
95	comment	\N	48	2015-06-14 17:24:51.406357-04	\N	\N
96	comment	\N	50	2015-06-14 17:27:45.384206-04	\N	\N
97	comment	\N	51	2015-06-14 17:28:39.300662-04	\N	\N
98	comment	\N	52	2015-06-14 17:31:02.254144-04	\N	\N
99	comment	\N	59	2015-06-28 20:43:57.665278-04	2	\N
\.


--
-- Name: notifications_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rq
--

SELECT pg_catalog.setval('notifications_id_seq', 99, true);


--
-- Data for Name: postLikes; Type: TABLE DATA; Schema: public; Owner: rq
--

COPY "postLikes" ("postId", "likedBy") FROM stdin;
9	2
2	2
1	2
1	1
2	1
4	2
\.


--
-- Data for Name: posts; Type: TABLE DATA; Schema: public; Owner: rq
--

COPY posts (id, body, "postedBy", "deletedOn", "createdOn", "updatedOn", "postedOn", path, "facebookId", body_tsvector) FROM stdin;
10	Tester is replying	3	\N	2015-03-21 12:33:53.286056-04	\N	2015-03-21 12:33:53.286056-04	,1,	\N	'repli':3 'tester':1
8	Second level comment	2	\N	2015-01-28 21:55:31.914578-05	\N	2015-01-28 21:55:31.914578-05	,6,	\N	'comment':3 'level':2 'second':1
11	testing another question	2	\N	2015-05-27 01:23:58.321849-04	\N	2015-05-27 01:23:58.321849-04	\N	\N	'anoth':2 'question':3 'test':1
12	2 testing another question	2	\N	2015-05-28 00:58:55.897137-04	\N	2015-05-28 00:58:55.897137-04	\N	\N	'2':1 'anoth':3 'question':4 'test':2
13	testing the test	2	\N	2015-05-29 01:35:26.295656-04	\N	2015-05-29 01:35:26.295656-04	\N	\N	'test':1,3
14	testing the test2	2	\N	2015-05-30 19:15:26.799112-04	\N	2015-05-30 19:15:26.799112-04	\N	\N	'test':1 'test2':3
15	3testt	2	\N	2015-05-30 19:17:28.07075-04	\N	2015-05-30 19:17:28.07075-04	\N	\N	'3testt':1
16	4test	2	\N	2015-05-30 19:18:43.502283-04	\N	2015-05-30 19:18:43.502283-04	\N	\N	'4test':1
17	tt_6_Test	2	\N	2015-05-30 19:21:14.953848-04	\N	2015-05-30 19:21:14.953848-04	\N	\N	'6':2 'test':3 'tt':1
18	5TEST	2	\N	2015-05-30 19:30:12.407306-04	\N	2015-05-30 19:30:12.407306-04	\N	\N	'5test':1
19	5TEST	2	\N	2015-05-30 19:31:08.608685-04	\N	2015-05-30 19:31:08.608685-04	\N	\N	'5test':1
20	6TEST	2	\N	2015-05-30 19:32:10.878208-04	\N	2015-05-30 19:32:10.878208-04	\N	\N	'6test':1
22	nother reply	2	\N	2015-05-30 19:36:06.591556-04	\N	2015-05-30 19:36:06.591556-04	,20,	\N	'nother':1 'repli':2
23	nother nother reply	2	\N	2015-05-30 19:42:32.560651-04	\N	2015-05-30 19:42:32.560651-04	,20,22,	\N	'nother':1,2 'repli':3
24	oh reply	2	\N	2015-05-30 19:47:12.946945-04	\N	2015-05-30 19:47:12.946945-04	,20,22,	\N	'oh':1 'repli':2
25	more rply	2	\N	2015-05-30 19:51:26.598429-04	\N	2015-05-30 19:51:26.598429-04	,20,22,	\N	'rpli':2
26	1	2	\N	2015-05-30 20:09:27.073723-04	\N	2015-05-30 20:09:27.073723-04	,20,22,23,	\N	'1':1
27	2	2	\N	2015-05-30 20:09:46.56791-04	\N	2015-05-30 20:09:46.56791-04	,20,22,23,	\N	'2':1
28	3	2	\N	2015-05-30 20:11:23.407445-04	\N	2015-05-30 20:11:23.407445-04	,20,22,23,27,	\N	'3':1
1	First Post	1	\N	2014-12-29 20:19:03.262449-05	\N	2014-12-29 20:19:03.262449-05	\N	\N	'first':1 'post':2
2	First comment on first post	1	\N	2014-12-29 20:19:30.271582-05	\N	2014-12-29 20:19:30.271582-05	,1,	\N	'comment':2 'first':1,4 'post':5
3	First reply to first comment on first post	1	\N	2014-12-29 20:20:04.067238-05	\N	2014-12-29 20:20:04.067238-05	,1,2,	\N	'comment':5 'first':1,4,7 'post':8 'repli':2
4	Second comment on first post	1	\N	2014-12-29 20:20:22.931663-05	\N	2014-12-29 20:20:22.931663-05	,1,	\N	'comment':2 'first':4 'post':5 'second':1
5	First reply on second comment on first post	1	\N	2014-12-29 20:20:53.004192-05	\N	2014-12-29 20:20:53.004192-05	,1,4,	\N	'comment':5 'first':1,7 'post':8 'repli':2 'second':4
6	Second post	1	\N	2014-12-31 02:03:50.864653-05	\N	2014-12-31 02:03:50.864653-05	\N	\N	'post':2 'second':1
29	4	2	\N	2015-05-30 20:11:39.357898-04	\N	2015-05-30 20:11:39.357898-04	,20,22,23,27,	\N	'4':1
30	5	2	\N	2015-05-30 20:12:48.325608-04	\N	2015-05-30 20:12:48.325608-04	,20,22,23,27,29,	\N	'5':1
31	6	2	\N	2015-05-30 20:13:00.950483-04	\N	2015-05-30 20:13:00.950483-04	,20,22,23,27,29,30,	\N	'6':1
32	grr	2	\N	2015-06-11 22:52:47.33713-04	\N	2015-06-11 22:52:47.33713-04	,20,22,25,	\N	'grr':1
33	anon1	2	\N	2015-06-14 16:23:21.522407-04	\N	2015-06-14 16:23:21.522407-04	\N	\N	'anon1':1
34	anon3	2	\N	2015-06-14 16:25:38.487415-04	\N	2015-06-14 16:25:38.487415-04	\N	\N	'anon3':1
35	anon4	2	\N	2015-06-14 16:27:49.666546-04	\N	2015-06-14 16:27:49.666546-04	\N	\N	'anon4':1
36	anon4	2	\N	2015-06-14 16:28:47.440277-04	\N	2015-06-14 16:28:47.440277-04	\N	\N	'anon4':1
37	anon8	2	\N	2015-06-14 16:31:57.431761-04	\N	2015-06-14 16:31:57.431761-04	\N	\N	'anon8':1
38	anon8	2	\N	2015-06-14 16:34:46.75511-04	\N	2015-06-14 16:34:46.75511-04	\N	\N	'anon8':1
39	anon8	2	\N	2015-06-14 16:35:37.952663-04	\N	2015-06-14 16:35:37.952663-04	\N	\N	'anon8':1
40	anon8	2	\N	2015-06-14 16:36:34.43065-04	\N	2015-06-14 16:36:34.43065-04	\N	\N	'anon8':1
41	anon8	2	\N	2015-06-14 16:39:14.57491-04	\N	2015-06-14 16:39:14.57491-04	\N	\N	'anon8':1
42	anon8	2	\N	2015-06-14 16:41:32.554789-04	\N	2015-06-14 16:41:32.554789-04	\N	\N	'anon8':1
43	anon8	2	\N	2015-06-14 16:43:47.706314-04	\N	2015-06-14 16:43:47.706314-04	\N	\N	'anon8':1
45	anon9	\N	\N	2015-06-14 16:49:25.376888-04	\N	2015-06-14 16:49:25.376888-04	\N	\N	'anon9':1
46	anon10	\N	\N	2015-06-14 17:22:53.354162-04	\N	2015-06-14 17:22:53.354162-04	\N	\N	'anon10':1
47	anonReply	\N	\N	2015-06-14 17:23:15.68822-04	\N	2015-06-14 17:23:15.68822-04	,46,	\N	'anonrepli':1
48	this is anonymous	\N	\N	2015-06-14 17:24:51.398845-04	\N	2015-06-14 17:24:51.398845-04	,46,	\N	'anonym':3
49	anon11	\N	\N	2015-06-14 17:27:36.921419-04	\N	2015-06-14 17:27:36.921419-04	\N	\N	'anon11':1
50	anon11-1	\N	\N	2015-06-14 17:27:45.381657-04	\N	2015-06-14 17:27:45.381657-04	,49,	\N	'-1':2 'anon11':1
51	anon-11-1-1	\N	\N	2015-06-14 17:28:39.297877-04	\N	2015-06-14 17:28:39.297877-04	,49,50,	\N	'-1':3,4 '-11':2 'anon':1
52	anon-11-1-2	\N	\N	2015-06-14 17:31:02.250331-04	\N	2015-06-14 17:31:02.250331-04	,49,50,	\N	'-1':3 '-11':2 '-2':4 'anon':1
53	Not Anonymous	2	\N	2015-06-14 17:31:28.230153-04	\N	2015-06-14 17:31:28.230153-04	\N	\N	'anonym':2
54	Again not anonymous	2	\N	2015-06-14 17:32:33.614343-04	\N	2015-06-14 17:32:33.614343-04	\N	\N	'anonym':3
55	Again not anonymous	2	\N	2015-06-14 17:33:59.906014-04	\N	2015-06-14 17:33:59.906014-04	\N	\N	'anonym':3
7	commenting via web on second post	2	\N	2015-01-28 21:55:10.350886-05	\N	2015-01-28 21:55:10.350886-05	,6,	\N	'comment':1 'post':6 'second':5 'via':2 'web':3
56	not anonymous	2	\N	2015-06-14 17:45:50.381052-04	\N	2015-06-14 17:45:50.381052-04	\N	\N	'anonym':2
9	Turner is replying to the very first post	2	\N	2015-01-31 18:39:33.233835-05	\N	2015-01-31 18:39:33.233835-05	,1,	\N	'first':7 'post':8 'repli':3 'turner':1
57	notanon	2	\N	2015-06-14 17:47:13.130585-04	\N	2015-06-14 17:47:13.130585-04	\N	\N	'notanon':1
58	test 11	2	\N	2015-06-20 05:00:00.721741-04	\N	2015-06-20 05:00:00.721741-04	\N	\N	'11':2 'test':1
59	replyyy	2	\N	2015-06-28 20:43:57.65707-04	\N	2015-06-28 20:43:57.65707-04	,11,	\N	'replyyi':1
60	This is to test the fill text search stuff	\N	\N	2015-07-03 20:07:28.925328-04	\N	2015-07-03 20:07:28.925328-04	\N	\N	'fill':6 'search':8 'stuff':9 'test':4 'text':7
\.


--
-- Name: posts_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rq
--

SELECT pg_catalog.setval('posts_id_seq', 60, true);


--
-- Data for Name: userNotifications; Type: TABLE DATA; Schema: public; Owner: rq
--

COPY "userNotifications" ("userId", "notificationId", seen) FROM stdin;
1	1	t
2	1	t
2	75	t
2	54	t
2	78	t
2	2	t
3	81	f
2	80	t
2	83	t
2	84	t
2	85	t
2	86	t
2	87	t
2	88	t
2	89	t
2	90	t
2	91	t
2	92	t
2	93	t
2	99	f
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: rq
--

COPY users (id, username, "deletedOn", "firstName", "middleName", "lastName", "facebookId", email, "createdOn", "updatedOn", "profilePhotoURL", "preferredDisplayName", "isAdmin", "hasLoggedIn") FROM stdin;
1	test	\N	Test	\N	User	\N	test@example.com	2014-12-28 18:21:14.829799-05	\N	\N	\N	f	t
2	turner.hayes	\N	Turner	\N	Hayes	947729430503	thayes@wesleyan.edu	2015-01-17 19:10:24.100951-05	\N	https://graph.facebook.com/947729430503/picture?type=square	\N	t	t
3	wxurxuu_wongsky_1415742634@tfbnw.net	\N	Helen	Amhdaieagbif	Wongsky	1390466587910645	wxurxuu_wongsky_1415742634@tfbnw.net	2015-03-21 12:22:09.550866-04	\N	\N	\N	f	t
\.


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rq
--

SELECT pg_catalog.setval('users_id_seq', 3, true);


--
-- Name: pk_notifications; Type: CONSTRAINT; Schema: public; Owner: rq; Tablespace: 
--

ALTER TABLE ONLY notifications
    ADD CONSTRAINT pk_notifications PRIMARY KEY (id);


--
-- Name: pk_postsLiked; Type: CONSTRAINT; Schema: public; Owner: rq; Tablespace: 
--

ALTER TABLE ONLY "postLikes"
    ADD CONSTRAINT "pk_postsLiked" PRIMARY KEY ("postId", "likedBy");


--
-- Name: pk_posts_id; Type: CONSTRAINT; Schema: public; Owner: rq; Tablespace: 
--

ALTER TABLE ONLY posts
    ADD CONSTRAINT pk_posts_id PRIMARY KEY (id);


--
-- Name: pk_userNotifications; Type: CONSTRAINT; Schema: public; Owner: rq; Tablespace: 
--

ALTER TABLE ONLY "userNotifications"
    ADD CONSTRAINT "pk_userNotifications" PRIMARY KEY ("userId", "notificationId");


--
-- Name: pk_users_id; Type: CONSTRAINT; Schema: public; Owner: rq; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT pk_users_id PRIMARY KEY (id);


--
-- Name: unique_users_facebookID; Type: CONSTRAINT; Schema: public; Owner: rq; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT "unique_users_facebookID" UNIQUE ("facebookId");


--
-- Name: unique_users_username; Type: CONSTRAINT; Schema: public; Owner: rq; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT unique_users_username UNIQUE (username);


--
-- Name: fki_post_likes_post; Type: INDEX; Schema: public; Owner: rq; Tablespace: 
--

CREATE INDEX fki_post_likes_post ON "postLikes" USING btree ("postId");


--
-- Name: fki_post_likes_users; Type: INDEX; Schema: public; Owner: rq; Tablespace: 
--

CREATE INDEX fki_post_likes_users ON "postLikes" USING btree ("likedBy");


--
-- Name: fki_posts_posted_by_users_id; Type: INDEX; Schema: public; Owner: rq; Tablespace: 
--

CREATE INDEX fki_posts_posted_by_users_id ON posts USING btree ("postedBy");


--
-- Name: index_posts_path; Type: INDEX; Schema: public; Owner: rq; Tablespace: 
--

CREATE INDEX index_posts_path ON posts USING btree (path NULLS FIRST);


--
-- Name: ix_posts_body_tsvector; Type: INDEX; Schema: public; Owner: rq; Tablespace: 
--

CREATE INDEX ix_posts_body_tsvector ON posts USING gin (body_tsvector);


--
-- Name: trigger_posts_body_tsvector; Type: TRIGGER; Schema: public; Owner: rq
--

CREATE TRIGGER trigger_posts_body_tsvector BEFORE INSERT OR UPDATE ON posts FOR EACH ROW EXECUTE PROCEDURE tsvector_update_trigger('body_tsvector', 'pg_catalog.english', 'body');


--
-- Name: trigger_posts_default_posted_on; Type: TRIGGER; Schema: public; Owner: rq
--

CREATE TRIGGER trigger_posts_default_posted_on BEFORE INSERT ON posts FOR EACH ROW WHEN ((new."postedOn" IS NULL)) EXECUTE PROCEDURE trigger_posts_default_posted_on();


--
-- Name: fk_notifications_postedBy; Type: FK CONSTRAINT; Schema: public; Owner: rq
--

ALTER TABLE ONLY notifications
    ADD CONSTRAINT "fk_notifications_postedBy" FOREIGN KEY ("postedBy") REFERENCES users(id);


--
-- Name: fk_notifications_relatedPost; Type: FK CONSTRAINT; Schema: public; Owner: rq
--

ALTER TABLE ONLY notifications
    ADD CONSTRAINT "fk_notifications_relatedPost" FOREIGN KEY ("relatedPostId") REFERENCES posts(id);


--
-- Name: fk_notifications_subjectPost; Type: FK CONSTRAINT; Schema: public; Owner: rq
--

ALTER TABLE ONLY notifications
    ADD CONSTRAINT "fk_notifications_subjectPost" FOREIGN KEY ("subjectPostId") REFERENCES posts(id);


--
-- Name: fk_post_likes_post; Type: FK CONSTRAINT; Schema: public; Owner: rq
--

ALTER TABLE ONLY "postLikes"
    ADD CONSTRAINT fk_post_likes_post FOREIGN KEY ("postId") REFERENCES posts(id);


--
-- Name: fk_post_likes_users; Type: FK CONSTRAINT; Schema: public; Owner: rq
--

ALTER TABLE ONLY "postLikes"
    ADD CONSTRAINT fk_post_likes_users FOREIGN KEY ("likedBy") REFERENCES users(id);


--
-- Name: fk_posts_users; Type: FK CONSTRAINT; Schema: public; Owner: rq
--

ALTER TABLE ONLY posts
    ADD CONSTRAINT fk_posts_users FOREIGN KEY ("postedBy") REFERENCES users(id);


--
-- Name: fk_userNotifications_notifications; Type: FK CONSTRAINT; Schema: public; Owner: rq
--

ALTER TABLE ONLY "userNotifications"
    ADD CONSTRAINT "fk_userNotifications_notifications" FOREIGN KEY ("notificationId") REFERENCES notifications(id);


--
-- Name: fk_userNotifications_users; Type: FK CONSTRAINT; Schema: public; Owner: rq
--

ALTER TABLE ONLY "userNotifications"
    ADD CONSTRAINT "fk_userNotifications_users" FOREIGN KEY ("userId") REFERENCES users(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

