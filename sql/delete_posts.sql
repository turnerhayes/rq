﻿delete from "userNotifications"
where "notificationId" in (
	select distinct "id" from "notifications"
	where (
		"notifications"."subjectPostId" >= 10
		or "notifications"."relatedPostId" >= 10
	)
)
;

delete from "notifications"
where (
	"notifications"."subjectPostId" >= 10
	or "notifications"."relatedPostId" >= 10
)
;

delete from "postLikes"
where "postLikes"."postId" >= 10
;

delete from posts where id >= 10;

SELECT setval('notifications_id_seq', (SELECT MAX(id) FROM "notifications"));

SELECT setval('posts_id_seq', (SELECT MAX(id) FROM "posts"));