"use strict";

var fs                  = require('fs');
var http                = require('http');
var path                = require('path');
var express             = require('express');
var session             = require('express-session');
var vhost               = require('vhost');
var MongoSessionStore   = require('connect-mongostore')(session);
var bodyParser          = require('body-parser');
var Cookies             = require('cookies');
var errorhandler        = require('errorhandler');
var morgan              = require('morgan');
var log4js              = require('log4js');
var postsRouter         = require('./routers/posts');
var usersRouter         = require('./routers/users');
var notificationsRouter = require('./routers/notifications');
var adminRouter         = require('./routers/admin');
var authRouter          = require('./routers/authentication');
var appConfig           = require('./config/app.dev');
var pathsConfig         = require('./config/paths');
var authentication      = require('./authentication');
var SocketManager       = require('./lib/socket-manager');
var Manchu              = require('./lib/manchu');

var app = module.exports = express();

var server = http.createServer(app);

SocketManager.Initialize(server);

// Configuration
app.set('views', __dirname + '/views');
app.set('view engine', 'manchu');
app.engine('manchu', Manchu.__express);
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({}));
app.use(Cookies.express(appConfig.app_secret));

app.use(
	morgan(
		appConfig.logging.format || 'combined',
		{
			stream: fs.createWriteStream(path.join(pathsConfig.logs, 'access.log'), {flags: 'a'})
		}
	)
);

app.use(
	session({
		store: new MongoSessionStore({
			"host": appConfig.session.store.host,
			"port" : appConfig.session.store.port,
			"db": appConfig.session.store.database,
		}),
		secret: appConfig.app_secret,
		saveUninitialized: true,
		resave: true
	})
);

app.use(errorhandler());

app.use(vhost(appConfig.static.host, require('./apps/static').app));
// app.use('/static', require('./apps/static').app);

log4js.configure(path.resolve(__dirname, 'config/log4js.json'), { cwd: pathsConfig.logs });

authentication(app);

// Routes
app.route('/')
	.get(
		function(req, res) {
			if (req.user) {
				res.redirect('/posts/latest');
				return;
			}

			res.redirect('/login');
			return;
		}
	);

// As far as I can tell, workers cannot be created from cross-domain scripts. So we need to serve
// worker scripts from this domain, rather than the static domain
app.route('/static/workers/:path')
	.get(
		function(req, res) {
			res.sendFile(path.join(pathsConfig.static, 'js', 'workers', req.params.path));
		}
	);

app.use(postsRouter);

app.use(usersRouter);

app.use(authRouter);

app.use('/notifications', notificationsRouter);

app.use('/admin', adminRouter);


server.listen(appConfig.address.port, function(){
	console.log("Express server listening on port %d in %s mode", this.address().port, app.settings.env);
});
