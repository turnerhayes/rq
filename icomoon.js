#!/usr/bin/node

// jshint node: true

"use strict";

var icomoon = require('icomoon-build');
var path = require('path');
var util = require('util');

var projectFile = path.join(__dirname, 'client', 'icomoon.json');

var r = icomoon.buildProject(projectFile, function(err, result) {
	if (err) {
		throw err;
	}

	console.log(util.inspect(result));
});

console.log(util.inspect(r));
