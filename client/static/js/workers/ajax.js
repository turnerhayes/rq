(function(
undefined) {
	"use strict";

	function serializeData(data) {
		if (data === undefined) {
			return '';
		}

		var propertyName, propertyValue;

		var serialized = [];


		for (propertyName in data) {
			if (!data.hasOwnProperty(propertyName)) {
				continue;
			}

			if (data[propertyName] === undefined) {
				continue;
			}

			if (typeof data[propertyName] === 'function') {
				continue;
			}

			propertyValue = data[propertyName];

			serialized.push(encodeURIComponent(propertyName) + '=' + encodeURIComponent(propertyValue));
		}

		return serialized.join('&');
	}

	self.onmessage = function(message) {
		var xhr = new XMLHttpRequest();

		var data = message.data;

		if (data.options === undefined) {
			data.options = {};
		}

		if (data.options.type === undefined) {
			data.options.type = 'GET';
		}

		data.options.type = data.options.type.toUpperCase();

		data.options.dataType = data.options.dataType || 'application/json';

		var requestData = data.options.data;

		var url = data.url;

		var isJSON = data.options.dataType === 'application/json';

		var body;

		var headerName;

		if (requestData) {
			if (data.options.type === 'GET') {
				url += '?' + serializeData(requestData);
			}
			else {
				if (isJSON) {
					body = JSON.stringify(requestData);
				}
				else {
					body = serializeData(requestData);
				}
			}
		}

		if (isJSON) {
			data.options.headers = data.options.headers || {};
			data.options.headers.Accept = 'application/json; */*';
			data.options.headers['Content-type'] = 'application/json';
		}

		xhr.open(data.options.type, url, true);

		if (data.options.headers) {
			for (headerName in data.options.headers) {
				if (!data.options.headers.hasOwnProperty(headerName)) {
					continue;
				}

				xhr.setRequestHeader(headerName, data.options.headers[headerName]);
			}
		}

		xhr.onreadystatechange = function() {
			var responseType, isJSON, responseObject;

			if (xhr.readyState === 4) {
				responseType = xhr.getResponseHeader('Content-type');

				if (/application\/json/.test(responseType)) {
					responseObject = JSON.parse(xhr.responseText);
				}

				self.postMessage({
					status: xhr.status,
					statusText: xhr.statusText,
					responseText: xhr.responseText,
					responseObject: responseObject
				});

				self.close();
			}
		};

		xhr.send(body);

	};
}());
