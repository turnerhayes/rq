/* jshint browser: true */

define(
[
	'wrappers/backbone/router',
	'js/apps/posts',
	'template!login/login',
	'template!questions/question-detail',
	'template!questions/ask',
	'template!users/user-profile'
],
function(
	Router,
	PostsApp,
	loginTemplate,
	questionDetailTemplate,
	askQuestionTemplate,
	userProfileTemplate,
undefined) {
	"use strict";

	var GlobalRouter = Router.extend({
		routes: {
			"": "showQuestion",
			"login": "login",
			"ask": "askQuestion",
			"profile": "showProfile",
			"profile/:username": "showProfile",
			"posts/:post": "showPost"
		},

		login: function login() {
			this.updateLeftPanel('');
			this.updateRightPanel('');
			this.updateMiddlePanel(loginTemplate());
		},

		askQuestion: function askQuestion() {
			this.updateMiddlePanel(askQuestionTemplate());
		},

		showProfile: function showProfile(username) {
			var user = { username: username };

			this.updateMiddlePanel(userProfileTemplate({
				user: user
			}));
		},

		showPost: function showPost(postId) {
			PostsApp.instance.getPostById(postId)
				.then(
					function(post) {
						this.updateMiddlePanel(
							questionDetailTemplate({
								question: post.toJSON()
							})
						);
					}
				);
		}
	});

	var _instance = new GlobalRouter();

	Object.defineProperties(GlobalRouter, {
		instance: {
			enumerable: true,
			get: function getGlobalRouterInstance() {
				return _instance;
			}
		}
	});

	return GlobalRouter;
});
