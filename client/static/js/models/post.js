define(
[
	'underscore',
	'request',
	'wrappers/backbone/model'
],
function(
	_,
	Request,
	Model,
undefined) {
	"use strict";

	var PostModel = Model.extend({
		constructor: function(attributes) {
			var model = this;

			if (attributes.isAnonymous) {
				attributes.postedBy = null;
			}

			delete attributes.isAnonymous;

			return Model.apply(model, arguments);
		},

		initialize: function() {
			var model = this;

			if (!_.isUndefined(model.get('parentId'))) {
				if (!model.get('path') || model.get('path').indexOf(',' + model.get('parentId') + ',') < 0) {
					model.set('path', (model.get('path') || ',') + model.get('parentId') + ',');
					model.unset('parentId', {silent: true});
				}
			}

			if (!_.isUndefined(model.get('postedOn')) && !_.isDate(model.get('postedOn'))) {
				model.set('postedOn', new Date(model.get('postedOn')));
			}

			if (!_.isUndefined(model.get('createdOn')) && !_.isDate(model.get('createdOn'))) {
				model.set('createdOn', new Date(model.get('createdOn')));
			}

			if (!_.isUndefined(model.get('updatedOn')) && !_.isDate(model.get('updatedOn'))) {
				model.set('updatedOn', new Date(model.get('updatedOn')));
			}

			if (!_.isUndefined(model.get('deletedOn')) && !_.isDate(model.get('deletedOn'))) {
				model.set('deletedOn', new Date(model.get('deletedOn')));
			}

			Model.prototype.initialize.apply(model, arguments);
		},

		toJSON: function() {
			var model = this;

			var json = Model.prototype.toJSON.apply(model, arguments);

			if (json.postedBy && _.isFunction(json.postedBy.toJSON)) {
				json.postedBy = json.postedBy.toJSON();
			}

			if (json.children && json.children.length > 0) {
				json.children = _.map(
					json.children,
					function(child) {
						return child.toJSON();
					}
				);
			}

			return json;
		},

		like: function() {
			var model = this;

			return Request.patch(model.urlRoot + model.get('id') + '/like')
				.then(
					function(response) {
						return response.responseObject;
					}
				);
		},

		unlike: function() {
			var model = this;

			return Request.patch(model.urlRoot + model.get('id') + '/unlike')
				.then(
					function(response) {
						return response.responseObject;
					}
				);
		},

		urlRoot: '/posts/'
	});

	Object.defineProperties(PostModel.prototype, {
		isAnonymous: {
			enumerable: true,
			get: function() {
				var model = this;

				return !model.get('postedBy');
			}
		}
	});

	return PostModel;
});
