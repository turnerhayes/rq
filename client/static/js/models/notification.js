define(
[
	'wrappers/backbone/model'
],
function(
	Model,
undefined) {
	"use strict";

	return Model.extend({
		initialize: function(args) {
			var model = this;

			Model.prototype.initialize.apply(model, arguments);
		},

		urlRoot: '/notifications/'
	});
});
