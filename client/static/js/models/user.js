define(
[
	'underscore',
	'wrappers/backbone/model'
],
function(
	_,
	Model,
undefined) {
	"use strict";

	return Model.extend({
		initialize: function(args) {
			var model = this;

			if (!_.isObject(model.get('name'))) {
				model.set('name', {
					first: model.get('firstName'),
					middle: model.get('middleName'),
					last: model.get('lastName'),
					display: model.get('preferredDisplayName')
				}, { silent: true });
			}

			model.unset('firstName', { silent: true });
			model.unset('middleName', { silent: true });
			model.unset('lastName', { silent: true });
			model.unset('preferredDisplayName', { silent: true });

			Model.prototype.initialize.apply(model, arguments);
		},

		urlRoot: '/users/'
	});
});
