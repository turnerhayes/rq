(function() {
	"use strict";

	document.documentElement.classList.remove('no-js');

	function initialize() {
		require(
			[
				'backbone',
				'dombot',
				'js/lib/view-starter',
				'js/routers/global',
				'bootstrap'
			],
			function(
				Backbone,
				$,
				viewStarter,
				GlobalRouter,
				Bootstrap,
			undefined) {
				// Remove the ugly Facebook appended hash
				// <https://github.com/jaredhanson/passport-facebook/issues/12>
				(function removeFacebookAppendedHash() {
					if (!window.location.hash || window.location.hash !== '#_=_') {
						return;
					}

					if (window.history && window.history.replaceState) {
						return window.history.replaceState("", document.title, window.location.pathname);
					}

					// Prevent scrolling by storing the page's current scroll offset
					var scroll = {
						top: document.body.scrollTop,
						left: document.body.scrollLeft
					};
					window.location.hash = "";
					// Restore the scroll offset, should be flicker free
					document.body.scrollTop = scroll.top;
					document.body.scrollLeft = scroll.left;
				}());

				$(document).on('click.global--backbone-routing', 'a:not([data-request-page])', function(event) {
					var path = this.getAttribute('href');
					if (/^(?:#)|(?:https?:)/.test(path)) {
						return;
					}

					event.preventDefault();

					Backbone.history.navigate(path, { trigger: true });
				});

				viewStarter({
					$el: $(document)
				});

				Backbone.history.start({
					silent: true,
					pushState: true
				});
			}
		);
	}

	// Need to specify path because the baseUrl has not been configured at this point
	require([window.RQ.Config["static-path"] + '/js/config/requirejs.js'], function(requireConfigObject) {
		requireConfigObject.baseUrl = window.RQ.Config["static-path"];
		require.config(requireConfigObject);

		initialize();
	});
})();