/* jshint browser: true, node: true */

(function(factory) {
	"use strict";

	if (typeof define === "function" && define.amd) {
		define([], factory);
	} else if (typeof exports === "object") {
		module.exports = factory();
	}
}(function(
undefined) {
	"use strict";

	var mockNetwork;

	if (typeof localStorage !== 'undefined') {
		mockNetwork = !!localStorage.getItem('RQ_CONFIG_MOCK_NETWORK');
	}

	var config = {
		"paths": {
			"wrappers"       : "js/third_party_wrappers",
			"handlebars"     : "bower_components/handlebars/handlebars.min",
			"jquery"         : "bower_components/jquery/dist/jquery.min",
			"request"        : "js/lib/request",
			"manchu"         : "js/lib/manchu",
			"backbone"       : "js/third_party_wrappers/backbone/backbone",
			"bootstrap"      : "bower_components/bootstrap/dist/js/bootstrap.min",
			"moment"         : "bower_components/moment/moment",
			"underscore"     : "bower_components/underscore/underscore-min",
			"text"           : "bower_components/requirejs-text/text",
			"json"           : "bower_components/requirejs-plugins/src/json",
			"Q"              : "bower_components/q/q",
			"socket-io"      : "bower_components/socket.io-client/socket.io",
			"template"       : "js/lib/template-loader",
			"dombot"         : "js/third_party_wrappers/jquery/dombot",
			"facebook"       : "//connect.facebook.net/en_US/all",
			"facebook-access": "js/lib/facebook-access"
		},
		"shim": {
			"jquery": {
				"exports": "jQuery"
			},
			"facebook": {
				"exports": "FB"
			},
			"bootstrap": {
				"deps": ["jquery"]
			}
		},
		"config": {
			"text": {
				"useXhr": function(xhr, url) {
					return true;
				}
			}
		}
	};

	if (mockNetwork) {
		config.paths["facebook-access"] = 'js/mock/facebook-access';
	}

	return config;
}));