
/* jshint browser: true */

define(
[
	'js/views/widgets/modal-dialog',
	'js/apps/users',
	'template!dialogs/global-nav-dialog'
],
function(
	ModalDialogView,
	UsersApp,
	globalNavDialogTemplate,
undefined) {
	"use strict";

	return ModalDialogView.extend({
		initialize: function() {
			var view = this;

			var options = {
				title: 'RQ Navigation',
				dialogType: 'dialogs/global-nav-dialog'
			};

			ModalDialogView.prototype.initialize.call(view, options);
		},

		render: function() {
			var view = this;

			UsersApp.instance.getCurrentUser()
				.then(
					function(currentUser) {
						view.setContent(
							globalNavDialogTemplate({
								user: currentUser.toJSON()
							})
						);
					}
				);

			return view;
		}
	});
});
