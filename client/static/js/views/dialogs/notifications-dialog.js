/* jshint browser: true */

define(
[
	'underscore',
	'js/views/widgets/modal-dialog',
	'js/apps/notifications',
	'template!dialogs/notifications-dialog'
],
function(
	_,
	ModalDialogView,
	NotificationsApp,
	notificationsDialogTemplate,
undefined) {
	"use strict";

	return ModalDialogView.extend({
		initialize: function(options) {
			var view = this;

			options = options || {};

			var content = notificationsDialogTemplate({
				notifications: view._notifications
			});


			options.title = 'Notifications';
			options.content = content;
			options.dialogType = 'dialogs/notifications-dialog';

			ModalDialogView.prototype.initialize.call(view, options);
		},

		open: function() {
			var view = this;

			NotificationsApp.instance.getUserNotifications()
				.done(
					function(notifications) {
						var content = notificationsDialogTemplate({
							notifications: notifications.toJSON()
						});
						
						view.setContent(content);

						ModalDialogView.prototype.open.call(view);

						_.delay(
							_.bind(NotificationsApp.instance.markAllNotificationsSeen, NotificationsApp.instance),
							3 * 1000
						);
					}
				);
		}
	});
});
