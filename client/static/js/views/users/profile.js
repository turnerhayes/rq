/* jshint browser: true */

define(
[
	'dombot',
	'Q',
	'underscore',
	'wrappers/backbone/view',
	'js/apps/users',
	'js/apps/posts',
	'template!users/user-profile'
],
function(
	$,
	q,
	_,
	View,
	UsersApp,
	PostsApp,
	userProfileTemplate,
undefined) {
	"use strict";

	return View.extend({
		events: {
		},

		renderedOnServer: false,

		initialize: function(options) {
			var view = this;

			View.prototype.initialize.call(view, options);

			view.username = options.username;
			view.userID = options.id ? +options.id : undefined;

			view.renderedOnServer = view.el.hasAttribute('data-view-rendered-on-server');
			view.el.removeAttribute('data-view-rendered-on-server');
		},

		render: function() {
			var view = this;

			var userPromise;

			if (!_.isUndefined(view.userID)) {
				userPromise = UsersApp.instance.getUserByID(view.userID);
			}
			else if (!_.isUndefined(view.username)) {
				userPromise = UsersApp.instance.getUserByUsername(view.username);
			}
			else {
				userPromise = UsersApp.instance.getCurrentUser();
			}

			userPromise.then(
				function(user) {
					return PostsApp.instance.getPostsByUser(user)
						.then(
							function(posts) {
								return {
									user: user.toJSON(),
									userPosts: _.map(
										posts,
										function(post) {
											return post.toJSON();
										}
									)
								};
							}
						);
				}
			).done(
				function(
					context
				) {
					if (!view.renderedOnServer) {
						view.$el.replaceWith(userProfileTemplate(context));
						view.setElement(view.$el);
					}

					view.renderedOnServer = false;				
				}
			);

			return view;
		}
	});
});
