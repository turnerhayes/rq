define(
[
	'dombot',
	'Q',
	'underscore',
	'wrappers/backbone/view',
	'js/apps/posts',
	'js/apps/users',
	'js/lib/view-starter',
	'template!questions/post'
],
function(
	$,
	q,
	_,
	View,
	PostsApp,
	UsersApp,
	ViewStarter,
	postTemplate,
undefined) {
	"use strict";

	/**
	 * The debounce duration for toggling the like button, in milliseconds
	 */
	var LIKE_DEBOUNCE_DURATION = 170;

	var PostView = View.extend({
		renderedOnServer: false,

		initialize: function(options) {
			var view = this;
			options = options || {};

			View.prototype.initialize.call(view, options);

			view.postId = options.postId;

			if (options.post) {
				view.post = options.post;
			}

			if (_.isUndefined(view.postId)) {
				view.postId = view.$el.data('post-id') || undefined;
			}

			// view.renderedOnServer = view.el.hasAttribute('data-view-rendered-on-server');
			// view.el.removeAttribute('data-view-rendered-on-server');
		},

		getPost: function() {
			var view = this;


			if (!view._postPromise) {
				if (view.post) {
					view._postPromise = q(view.post);
					delete view.post;
				}
				else {
					view._postPromise =	_.isUndefined(view.postId) ?
						PostsApp.instance.getLatestPost({structured: true}) :
						PostsApp.instance.getPostById(view.postId, {structured: true});

					delete view.postId;
				}
			}

			return view._postPromise;
		},

		render: function() {
			var view = this;

			view.getPost().then(
				function(post) {
					var $el;

					if (!view.el || view.$el.children().length === 0) {
						$el = $(
							postTemplate({
								post: post.toJSON()
							})
						);

						if (view.$el) {
							view.$el.replaceWith($el);
						}

						view.setElement($el.get(0));
					}

					view._bindEvents();
				}
			);

			return view;
		},

		_bindEvents: function() {
			var view = this;

			// Can't use the events hash because there are other post views nested
			// within this view (in the comments). We need to restrict listeners
			// to DOM elements within this view only
			view.$el.find('.show-add-comment-form-button').first()
				.off('.post-detail-view')
				.on(
					'click.post-detail-view',
					_.bind(view._handleReplyButtonClick, view)
				);

			view.$el.find('.add-comment-form').first()
				.off('.post-detail-view')
				.on(
					'submit.post-detail-view',
					_.bind(view._handleCommentSubmit, view)
				);

			view.$el.find('.like-post-button').first()
				.off('.post-detail-view')
				.on(
					'click.post-detail-view',
					_.bind(view._handleLikeButtonClick, view)
				);
		},

		_handleCommentSubmit: function(event) {
			var view = this;

			event.preventDefault();

			var $form = $(event.target);

			var $body = $form.find('[name="body"]');

			$body.attr('readonly', true);

			view.getPost().done(
				function(post) {
					var commentData = {
						parentId: post.get('id'),
						path: $form.find('[name="path"]').val(),
						body: $form.find('[name="body"]').val(),
						isAnonymous: post.isAnonymous
					};

					PostsApp.instance.addComment(commentData).done(
						function(comment) {
							var $item = $('<li></li>');
							var $el = $(
								postTemplate({
									post: comment.toJSON()
								})
							);

							$item.append($el);

							var commentView = new PostView({
								post: comment,
								el: $el.get(0)
							}).render();

							view.$el.children('.comment-list').prepend($item);

							$form.removeClass('opened');
							view._clearCommentForm($form);
							$body.removeAttr('readonly');
						}
					);
				}
			);

			return false;
		},

		_clearCommentForm: function($form) {
			$form.get(0).reset();
		},

		_handleReplyButtonClick: function(event) {
			var view = this;

			var $targetForm = $(event.target).closest('.comment-form-container').find('.add-comment-form');

			if ($targetForm.hasClass('opened')) {
				return;
			}

			view.$el.find('.add-comment-form.opened').removeClass('opened');

			$targetForm.addClass('opened');

			$targetForm.find('[name="body"]').focus();
		},

		_debouncedLikeChanger: _.debounce(
			function($button) {
				var view = this;

				view.getPost().then(
					function(post) {
						post[$button.hasClass('is-liked') ? 'like' : 'unlike']()
							.done(
								function(result) {
									$button.toggleClass('is-liked', result.isLiked);	
									$button.attr('data-post-like-count', result.totalLikes);
								}
							);
					}
				);
			},
			LIKE_DEBOUNCE_DURATION
		),

		_handleLikeButtonClick: function(event) {
			var view = this;

			var $button = $(event.target);

			$button.toggleClass('is-liked');			

			view._debouncedLikeChanger($button);
		}
	});

	return PostView;
});
