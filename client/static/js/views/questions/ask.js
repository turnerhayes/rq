define(
[
	'dombot',
	'backbone',
	'js/apps/posts',
	'js/routers/global',
	'wrappers/backbone/view',
	'js/views/questions/post',
	'template!questions/question-detail'
],
function(
	$,
	Backbone,
	PostsApp,
	Router,
	View,
	PostView,
	questionDetailTemplate,
undefined) {
	"use strict";

	return View.extend({
		events: {
			"submit.ask-question-view .ask-question-form": "_handleFormSubmit"
		},

		_handleFormSubmit: function(event) {
			event.preventDefault();

			var $form = $(event.target);

			var postData = {
				body: $form.find('[name="body"]').val(),
				isAnonymous: $form.find('[name="post-anonymously"]').is(':checked')
			};

			PostsApp.instance.addQuestion(postData).done(
				function(post) {
					Router.instance.updateMiddlePanel(
						questionDetailTemplate({
							question: post.toJSON()
						})
					);

					Router.instance.navigate('/posts/' + post.get('id'));
				}
			);

			return false;
		},

		_clearCommentForm: function($form) {
			$form.get(0).reset();
		},
	});
});
