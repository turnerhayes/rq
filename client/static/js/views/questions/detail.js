define(
[
	'dombot',
	'Q',
	'underscore',
	'wrappers/backbone/view',
	'js/apps/posts',
	'template!questions/question-detail'
],
function(
	$,
	q,
	_,
	View,
	PostsApp,
	questionDetailTemplate,
undefined) {
	"use strict";

	var LIKE_DEBOUNCE_DURATION = 170;

	function getQuestion(id) {
		return PostsApp.instance.getPostById(id);
	}

	return View.extend({
		/**
		 * The debounce duration for toggling the like button, in milliseconds
		 */

		events: {
			"click.question-detail-view .show-add-comment-form-button": "_handleReplyButtonClick",
			"submit.question-detail-view .add-comment-form": "_handleCommentSubmit",
			"click.question-detail-view .like-post-button": "_handleLikeButtonClick"
		},

		renderedOnServer: false,

		initialize: function(options) {
			var view = this;

			View.prototype.initialize.call(view, options);

			view.questionID = options.questionID;

			view.renderedOnServer = view.el.hasAttribute('data-view-rendered-on-server');
			view.el.removeAttribute('data-view-rendered-on-server');
		},

		render: function() {
			var view = this;

			getQuestion(view.questionID).then(
				function(question) {
					if (!view.renderedOnServer) {
						view.$el.replaceWith(questionDetailTemplate({ question: question.toJSON() }));
						view.setElement(view.$el);

						view.renderedOnServer = false;
					}

					view.question = question;
				}
			);

			return view;
		},

		_handleCommentSubmit: function(event) {
			var view = this;

			event.preventDefault();

			var $form = $(event.target);

			var $body = $form.find('[name="body"]');

			$body.attr('readonly', true);

			PostsApp.instance.addComment({
				parentId: $form.find('[name="parentId"]').val(),
				path: $form.find('[name="path"]').val(),
				body: $form.find('[name="body"]').val()
			});

			return false;
		},

		_clearCommentForm: function($form) {
			$form.get(0).reset();
		},

		_handleReplyButtonClick: function(event) {
			var view = this;

			var $targetForm = $(event.target).closest('.comment-form-container').find('.add-comment-form');

			if ($targetForm.hasClass('opened')) {
				return;
			}

			view.$el.find('.add-comment-form.opened').removeClass('opened');

			$targetForm.addClass('opened');
		},

		_debouncedLikeChanger: _.debounce(
			function($button) {
				var view = this;

				PostsApp.instance.getPostById($button.data('postId'))
					.then(
						function(post) {
							return post[$button.hasClass('is-liked') ? 'like' : 'unlike']();
						}
					).done(
						function(result) {
							$button.toggleClass('is-liked', result.isLiked);	
							$button.attr('data-post-like-count', result.totalLikes);
						}
					);
			},
			LIKE_DEBOUNCE_DURATION
		),

		_handleLikeButtonClick: function(event) {
			var view = this;

			var $button = $(event.target);

			$button.toggleClass('is-liked');			

			view._debouncedLikeChanger($button);
		}
	});
});
