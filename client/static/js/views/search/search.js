define(
[
	'dombot',
	'wrappers/backbone/view',
	'js/apps/posts',
	'js/views/widgets/modal-dialog',
	'template!dialogs/search/results'
],
function(
	$,
	View,
	PostsApp,
	ModalDialogView,
	searchResultsTemplate,
undefined) {
	"use strict";

	return View.extend({
		events: {
			'submit.search-view .search-form': '_handleSubmit',
			'change': '_handleChange'
		},

		initialize: function() {
			var view = this;

			view.$form = view.$el.find('.search-form');

			view.$noResultsCategory = view.$el.find('.no-results-message');

			View.prototype.initialize.apply(view, arguments);

			return view;
		},

		_handleSubmit: function(event) {
			var view = this;

			event.preventDefault();

			var options = {};

			var postedBy         = view.$form.find('[name="post-author"]').val();
			var startDate        = view.$form.find('[name="start-date"]').val();
			var endDate          = view.$form.find('[name="end-date"]').val();
			var searchTerm       = view.$form.find('[name="post-contains"]').val();
			var includeQuestions = view.$form.find('[name="include-questions"]').prop('checked');
			var includeComments  = view.$form.find('[name="include-comments"]').prop('checked');

			if (postedBy) {
				options.postedBy = Number(postedBy);
			}

			if (startDate) {
				options.startDate = startDate;
			}

			if (endDate) {
				options.endDate = endDate;
			}

			if (searchTerm) {
				options.searchTerm = searchTerm;
			}

			options.includeQuestions = includeQuestions;
			options.includeComments = includeComments;

			PostsApp.instance.searchPosts(options).done(
				function(posts) {
					if (_.size(posts) === 0) {
						view.$noResultsCategory.addClass('visible');

						return;
					}

					var $results = $(
						searchResultsTemplate({
							results: _.map(
								posts,
								function(post) {
									return post.toJSON();
								}
							)
						})
					);


					$(document.body).append($results);

					$results.modal({
						show: true
					});
				}
			);
		},

		_handleChange: function() {
			var view = this;

			view.$noResultsCategory.removeClass('visible');
		}
	});
});
