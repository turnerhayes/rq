define(
[
	'dombot',
	'wrappers/backbone/view',
	'template!login/login'
],
function(
	$,
	View,
	loginTemplate,
undefined) {
	"use strict";

	return View.extend({
		events: {
			'click.login-view .login-button': 'doLogin',
		},

		initialize: function initialize() {
			var view = this;

			View.prototype.initialize.apply(view, arguments);
		},

		render: function render() {
			var view = this;

			view.$el.html(loginTemplate());

			return view;
		},

		doLogin: function doLogin(event) {
			var $target = $(event.target);

			if ($target.hasClass('facebook')) {
				document.location.href = '/auth/fb';
			}
		}
	});
});
