/* jshint browser: true */

define(
[
	'Q',
	'underscore',
	'backbone',
	'wrappers/backbone/view',
	'js/models/post',
	'js/apps/users',
	'facebook-access',
	'manchu'
],
function(
	Q,
	_,
	Backbone,
	View,
	PostModel,
	UsersApp,
	Facebook,
	Manchu,
undefined) {
	"use strict";

	return View.extend({
		events: {
			'click.facebook-sync-dashboard-view .get-posts-button': '_handleGetPostsButtonClick'
		},

		initialize: function() {
			var view = this;

			view.$form = view.$el.find('.facebook-sync-form');

			view.$resultsContainer = view.$el.find('.results-container');

			view._loginPromise = Facebook.login().then(
				function() {
					view.$el.addClass('facebook-logged-in');
					view.$form.find('.get-posts-button').removeAttr('disabled');
				}
			);

			View.prototype.initialize.apply(view, arguments);
		},

		_handleGetPostsButtonClick: function(event) {
			var view = this;

			var $button = view.$(event.target);

			if ($button.prop('disabled')) {
				return;
			}

			view._loginPromise.then(
				function() {
					$button.prop('disabled', true);
					return Facebook.getPosts();
				}
			).then(
				function(posts) {
					var userIds = _.map(
						posts,
						function(post) {
							return +post.get('postedBy').facebookId;
						}
					);

					return Facebook.getUsers({
						userIds: userIds
					}).then(
						function(users) {
							return {
								posts: posts,
								facebookUsers: users,
								userIds: userIds
							};
						}
					);
				}
			).then(
				function(results) {
					return Q.all([
						view._uploadUsers(results.facebookUsers),
						Manchu.getPartial('questions/post'),
						UsersApp.instance.getUsers()
					]).spread(
						function(uploadedUsers, partial, users) {
							view.$resultsContainer.html(
								_.map(
									results.posts,
									function(result) {
										return view._displayPost(result, partial, users);
									}
								).join('')
							);
						}
					);
				}
			).finally(
				function() {
					$button.prop('disabled', false);
				}
			).done();
		},

		_uploadUsers: function(users) {
			var view = this;

			if (_.size(users) === 0) {
				return Q();
			}

			return UsersApp.instance.addUsers(users);
		},

		_displayPost: function(post, partial, userList) {
			var view = this;

			var postJSON = post.toJSON();

			var user = _.find(userList, {facebookId: postJSON.postedBy.facebookId});

			return partial({
				post: postJSON,
				indentationLevel: postJSON.parentID ? 1 : 0
			});
		}
	});
});
