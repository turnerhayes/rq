/* jshint browser: true */

define(
[
	'dombot',
	'underscore',
	'wrappers/backbone/view'
],
function(
	$,
	_,
	View,
undefined) {
	"use strict";

	return View.extend({
		initialize: function(options) {
			var view = this;

			options = options || {};

			var content = options.content;

			var title = options.title;

			delete options.title;

			delete options.content;

			view.$body = $(document.body);

			View.prototype.initialize.apply(view, arguments);

			view.id = _.uniqueId('modal-dialog-view-');

			view.$backdrop = $('<div></div>').addClass('modal-dialog-backdrop');

			view.$dialog = $('<div></div>').addClass('modal-dialog');

			if (title) {
				view.$dialogTitle = $('<header></header>').addClass('modal-dialog--header');
				view.$dialogTitle
					.append(
						$('<h3></h3>').text(title)
					).append(
						$('<button class="close-button glyphicon glyphicon-remove"></button>')
					);
			}

			view.$dialogBody = $('<div></div>').addClass('modal-dialog--content');

			view.$dialog.append(view.$dialogTitle).append(view.$dialogBody);

			view.$backdrop.append(view.$dialog);

			view.isOpened = false;

			if (content) {
				view.setContent(content);
			}

			view.$triggers = view.$el
				.find(
					'[data-modal-dialog-trigger="' +
					options.dialogType +
					'"]'
				).addBack(
					'[data-modal-dialog-trigger="' +
					options.dialogType +
					'"]'
				);

			view.$triggers.on('click.modal-dialog-' + view.id, function() {
				view.open();
			});

			view.$dialog.on('click.modal-dialog-' + view.id, '.close-button', function(event) {
				view.close();
			});
		},

		setContent: function(content) {
			var view = this;

			view.$dialogBody.html(content);

			return view;
		},

		open: function() {
			var view = this;

			if (view.isOpened) {
				return false;
			}

			view.render();

			view.$backdrop.addClass('opened');

			view.$body.append(view.$backdrop);

			view.isOpened = true;

			return true;
		},

		close: function() {
			var view = this;

			if (!view.isOpened) {
				return false;
			}

			view.$backdrop.removeClass('opened');

			view.$backdrop.detach();

			view.isOpened = false;

			return true;
		},

		remove: function() {
			var view = this;

			view.$backdrop.remove();

			delete view.$backdrop;

			delete view.$dialog;

			return View.prototype.remove.apply(view, arguments);
		}
	});
});
