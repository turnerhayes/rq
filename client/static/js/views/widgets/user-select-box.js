define(
[
	'dombot',
	'underscore',
	'wrappers/backbone/view',
	'js/apps/users'
],
function(
	$,
	_,
	View,
	UsersApp,
undefined) {
	"use strict";

	return View.extend({
		initialize: function() {
			var view = this;

			view.$input = view.$el.find('.user-select-box-input');

			view._rendered = false;

			View.prototype.initialize.apply(view, arguments);

			return view;
		},

		render: function() {
			var view = this;

			UsersApp.instance.getUsers().done(
				function(users) {
					var options = [];
					var selected;

					// If it has been rendered, remove all items with values (i.e. non
					// default items)
					if (view._rendered) {
						selected = view.$input.val();
						view.$input.children().remove('[value]');
					}
					else {
						// If we haven't rendered before, use the default (if applicable)
						selected = view.$input.data('default');
					}

					
					options.push(view.$input.html());

					users.each(
						function(user) {
							// NOTE: If we use the value attribute to store the user ID, that
							// messes things up; in Chrome, at least, having a value attribute
							// a) causes that value (i.e. the user ID) to show up in the dropdown
							// of the textbox and b) prevents typing from triggering filtered
							// options. We use a data attribute to get around this.
							options.push(
								'<option value="' + user.get('id') + '"' +
									(
										!_.isUndefined(selected) && Number(user.get('id')) === Number(selected) ?
										' selected' :
										''
									) + '>' +
									user.get('displayName') + '</option>'
							);
						}
					);

					view.$input.html(options.join('')).prop('disabled', false);
					
					view._rendered = true;
				}
			);

			View.prototype.render.apply(view, arguments);

			return view;
		}
	});
});
