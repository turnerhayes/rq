define(
[
	'underscore',
	'wrappers/backbone/view',
	'js/apps/notifications',
	'js/lib/socket-manager'
],
function(
	_,
	View,
	NotificationsApp,
	SocketManager,
undefined) {
	"use strict";

	return View.extend({
		/**
		 * Frequency at which the server is polled for new notifications (in milliseconds)
		 */
		NOTIFICATION_CHECK_INTERVAL: 60 * 1000,

		initialize: function() {
			var view = this;

			view.$notificationsButton = view.$el.find('.notifications-button');

			view.listenTo(
				NotificationsApp.instance,
				'collection-synced collection-changed',
				_.bind(view._handleNotificationSync, view)
			);

			window.setInterval(
				function() {
					NotificationsApp.instance.getUserNotifications();
				},
				view.NOTIFICATION_CHECK_INTERVAL
			);
		},

		_updateButtonCount: function(count) {
			var view = this;

			view.$notificationsButton.attr('data-notifications-count', count);
		},

		_handleNotificationSync: function(data) {
			var view = this;

			view._updateButtonCount(_.size(data.notifications.unseen));
		}
	});
});