define(
[
	'dombot',
	'wrappers/backbone/view'
],
function(
	$,
	View,
undefined) {
	"use strict";

	return View.extend({
		events: {
			'click.page-header-view .nav-item': '_handleNavItemClicked',
		},

		_handleNavItemClicked: function(event) {
			$(event.currentTarget).addClass('active').siblings('.nav-item').removeClass('active');
		}
	});
});
