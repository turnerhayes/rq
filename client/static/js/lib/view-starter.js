/* jshint browser: true */

define(
[
	// 'jquery'
	'dombot'
],
function(
	$,
undefined) {
	"use strict";

	function ViewStarter(options) {
		options = options || {};

		var $el = options.$el;

		$el.find('[data-view-classes]').addBack('[data-view-classes]').each(function processViewClassesElement() {
			var $viewClassElement = $(this);
			var viewClasses = $viewClassElement.data('view-classes').split(/\s+/);

			var viewClassArgs = $viewClassElement.data('view-class-arguments') || {};

			viewClasses.forEach(function processViewClass(viewClass) {
				require(
					['js/views/' + viewClass],
					function renderViewClass(ViewClass) {
						var args = viewClassArgs[viewClass] || {};

						args.el = $viewClassElement[0];

						new ViewClass(args).render();
					},
					function(err) {
						console.error(err);
					}
				);
			});
		});
	}

	return ViewStarter;
});
