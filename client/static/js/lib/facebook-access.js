define(
[
	'underscore',
	'Q',
	'facebook',
	'js/models/post',
	'js/models/user',
	'json!js/config/site.json'
],
function(
	_,
	q,
	FB,
	PostModel,
	UserModel,
	SiteConfig,
undefined) {
	"use strict";

	var _initialized = false;

	var Facebook = {};

	function ensureInit() {
		if (!_initialized) {
			FB.init({
				appId  : SiteConfig.facebook.appID,
				version: 'v2.3'
			});

			_initialized = true;
		}
	}


	function postDataToPostModel(postData) {
		var body = postData.message;

		if (postData.type === 'photo') {
			body += '\n<img class="post-photo" src="' + postData.picture + '" />';
		}

		return new PostModel({
			body: body,
			postedBy: {
				facebookId: postData.from.id
			},
			postedOn: new Date(postData.created_time),
			createdOn: new Date(postData.created_time),
			updatedOn: new Date(postData.updated_time),
			facebookId: postData.id,
			parentId: postData.parentId
		});
	}

	function userDataToUserModel(userData) {
		var profilePhotoURL = "https://graph.facebook.com/" + userData.id + "/picture?type=square";

		if (userData.picture && userData.picture.data.is_silhouette) {
			profilePhotoURL = undefined;
		}

		return new UserModel({
			name: {
				first: userData.first_name,
				last: userData.last_name,
				middle: userData.middle_name,
				display: userData.name
			},
			username: userData.username,
			facebookId: +userData.id,
			profilePhotoURL: profilePhotoURL
		});
	}

	Object.defineProperties(
		Facebook,
		{
			_convertResponseToPosts: {
				value: function(response) {
					return _.flatten(
						_.map(
							response.data,
							function(postData) {
								var posts = [postDataToPostModel(postData)];

								if (postData.comments) {
									posts.concat(
										_.map(
											postData.comments.data,
											function(comment) {
												comment.parentId = postData.id;
												return postDataToPostModel(comment);
											}
										)
									);
								}

								return posts;
							}
						)
					);
				}
			},

			_convertResponseToUsers: {
				value: function(response) {
					return _.map(
						response,
						userDataToUserModel
					);
				}
			},

			login: {
				enumerable: true,
				configurable: true,
				value: function(options) {
					ensureInit();

					options = options || {};

					var scope = ['user_groups', 'read_stream'];

					if (_.isArray(options.scope)) {
						scope.push.apply(scope, options.scope);
					}

					var deferred = q.defer();

					FB.login(
						function(response) {
							if (response.error) {
								deferred.reject(response.error);
								return;
							}

							deferred.resolve(response);
						},
						{ scope: scope }
					);

					return deferred.promise;
				}
			},

			getPosts: {
				enumerable: true,
				configurable: true,
				value: function(options) {
					ensureInit();

					options = options || {};

					var deferred = q.defer();

					FB.api('/' + SiteConfig.facebook.groupID + '/feed',
						'GET',
						{
							since: options.start,
							until: options.end
						},
						function(response) {
							if (response.error) {
								deferred.reject(response.error);
								return;
							}

							deferred.resolve(Facebook._convertResponseToPosts(response));
						}
					);

					return deferred.promise;
				}
			},

			getUsers: {
				enumerable: true,
				configurable: true,
				value: function(options) {
					ensureInit();

					options = options || {};

					var deferred = q.defer();

					if (_.size(options.userIds) === 0) {
						deferred.resolve([]);
						return deferred.promise;
					}

					var endpointURL, method, args;

					if (_.isArray(options.userIds)) {
						if (_.size(options.userIds) === 1) {
							endpointURL = '/' + options.userIds[0];
							method = 'GET';
						}
						else if (_.size(options.userIds) > 1) {
							endpointURL = '/';
							method = 'POST';
							args = {
								batch: _.map(
									options.userIds,
									function(id) {
										return { "relative_url": id };
									}
								)
							};
						}
					}

					args.fields = [
						"id",
						"name",
						"username",
						"picture.type(square)",
					];

					args.scope = ['email'];

					FB.api(
						endpointURL,
						method,
						args,
						function(response) {
							var responseData;

							if (response.error) {
								deferred.reject(response.error);
								return;
							}

							if (_.size(options.userIds) > 1) {
								responseData = _.map(
									response,
									function(data) {
										if (data.error) {
											throw new Error(responseData.error);
										}

										return JSON.parse(data.body);
									}
								);
							}
							else {
								responseData = [response];
							}

							deferred.resolve(
								Facebook._convertResponseToUsers(responseData)
							);
						}
					);

					return deferred.promise;
				}
			}
		}
	);

	return Facebook;
});