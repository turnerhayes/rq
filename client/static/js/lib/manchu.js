define(
[
	'js/lib/manchu-builder',
	'underscore',
	'handlebars',
	'moment',
	'Q',
	'js/lib/template-partials'
],
function(
	manchuBuilder,
	_,
	Handlebars,
	moment,
	q,
	partials,
undefined) {
	"use strict";

	var siteConfig = {};
	var Manchu;

	// during optimization, this module will get required by the template
	// loader, but since it's in Node.js then, window will not be defined
	// and this will throw an error
	if (typeof window !== 'undefined' && window.RQ !== undefined) {
		siteConfig = window.RQ.Config;
	}

	function getTemplateFromText(text) {
		var templateSpec = Function( 'return ' + text + ';')();

		return Manchu.template(templateSpec);
	}

	Manchu = manchuBuilder({
		_: _,
		Handlebars: Handlebars,
		moment: moment,
		q: q,
		SiteConfig: siteConfig,
		getTemplate: function getTemplate(name) {
			var deferred = q.defer();

			require(
				['text!/dist/templates/' + name + '.js'],
				function(text) {
					deferred.resolve(getTemplateFromText(text));
				},
				function(err) {
					deferred.reject(err);
				}
			);

			return deferred.promise;
		}
	});

	var partialName;

	for (partialName in partials) {
		Manchu.registerPartial(partialName, getTemplateFromText(partials[partialName]));
	}

	return Manchu;
});