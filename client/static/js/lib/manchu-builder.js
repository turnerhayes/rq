/* jshint browser: true, node: true */

(function(factory) {
	"use strict";

	if (typeof define === 'function' && define.amd) {
		define(
			[
			],
			factory
		);
	} else if (typeof exports === 'object') {
		module.exports = factory(
		);
	}
}(function(
undefined) {
	"use strict";

	return function (dependencies) {
		var propertyName;

		var Handlebars = dependencies.Handlebars;
		var moment = dependencies.moment;
		var q = dependencies.q;
		var SiteConfig = dependencies.SiteConfig;
		var getTemplate = dependencies.getTemplate;

		var Manchu = {};

		for (propertyName in Handlebars) {
			Manchu[propertyName] = Handlebars[propertyName];
		}

		Manchu.ManchuHelpers = {
			StaticPath: function StaticPath(path) {
				return new Manchu.SafeString(SiteConfig["static-path"] + '/' + path.replace(/^\//, ''));
			},

			ISODate: function(options) {
				return new Date(options.fn(this)).toISOString();
			},

			DisplayDate: function(date) {
				return moment(date).from(moment());
			},

			ItemByID: function(options) {
				var collection = options.hash.collection;
				var id = options.hash.id;

				if (!collection || !collection.length) {
					return;
				}

				if (!id) {
					return;
				}

				var item = collection.filter(function(el) {
					return el._id ? el._id === id : el.id === id;
				});

				if (item.length) {
					item = item[0];
				}

				return options.fn(Manchu.createFrame((item.toJSON ? item.toJSON() : item) || {}));
			},

			Serialize: function(obj, options) {
				var excludeKeys = options.hash.exclude || [];
				var key;
				var toSerialize = {};

				if (obj === undefined) {
					return undefined;
				}

				if (typeof obj.toJSON === 'function') {
					obj = obj.toJSON();
				}

				for (key in obj) {
					if (excludeKeys.indexOf(key) >= 0) {
						continue;
					}

					if (typeof obj[key] === 'function') {
						continue;
					}

					toSerialize[key] = obj[key];
				}

				return new Manchu.SafeString(JSON.stringify(toSerialize));
			},

			/**
			 * Returns the arguments passed to it as an array.
			 */
			Array: function() {
				// The last argument is the options paramter, which we don't
				// want to output
				var args = [].slice.call(arguments, 0, -1);
				return args;
			},

			/**
			 * Returns the hash values as an object, or an empty object if there
			 * are no hash values.
			 */
			Object: function(options) {
				return options.hash || {};
			},

			Compare: function(lvalue, operator, rvalue, options) {
				var operators, result;
				
				if (arguments.length < 3) {
					throw new Error("Manchu helper 'compare' needs 2 parameters");
				}
				
				if (options === undefined) {
					options = rvalue;
					rvalue = operator;
					operator = "===";
				}

				operators = {
					'==': function (l, r) { return l == r; },
					'===': function (l, r) { return l === r; },
					'!=': function (l, r) { return l != r; },
					'!==': function (l, r) { return l !== r; },
					'<': function (l, r) { return l < r; },
					'>': function (l, r) { return l > r; },
					'<=': function (l, r) { return l <= r; },
					'>=': function (l, r) { return l >= r; },
					'typeof': function (l, r) { return typeof l == r; },
					'regex': function(l, r) { return (new RegExp(r)).test(l); }
				};
				
				if (!(operator in operators)) {
					throw new Error("Manchu helper 'Compare' doesn't know the operator " + operator);
				}
				
				result = operators[operator](lvalue, rvalue);

				// Being called as a subexpression
				if (!options.fn) {
					return result;
				}

				if (result) {
					return options.fn(this);
				} else if (options.inverse) {
					return options.inverse(this);
				}
			},

			Increment: function(value, step, options) {
				if (arguments.length === 2) {
					options = step;
					step = undefined;
				}

				if (step === undefined) {
					step = 1;
				}

				step = +step;

				if (step !== step) {
					// NaN
					step = 1;
				}

				value = +value;

				if (value !== value) {
					// NaN
					value = 0;
				}

				value = value + step;

				if (options.hash.max !== undefined && options.hash.max >= 0 && value > options.hash.max) {
					value = options.hash.max;
				}

				return value;
			},

			Filter: function(options) {
				var collection = options.hash.collection || [];

				var property = options.hash.property;

				var render = options.hash.render;

				var regex = new RegExp(options.hash.regex);

				var checkForValue = 'value' in options.hash;

				var targetValue = options.hash.value;

				var filtered = collection.filter(function(item) {
					if (!item) {
						return false;
					}

					if (checkForValue) {
						return item[property] === targetValue;
					}

					var value = item[property];

					if (!value) {
						value = '';
					}

					return regex.test(value);
				});

				if (render) {
					return new Manchu.SafeString(
						filtered.map(function(item) {
							return options.fn(Manchu.createFrame(item));
						}).join('')
					);
				}

				return filtered;

			},

			Trim: function(character, value, options) {
				if (!value) {
					return value;
				}

				return value.replace(new RegExp('^' + character + '+|' + character + '+$'), '');
			},

			Split: function(delimiter, value, isRegEx, options) {
				if (arguments.length === 3) {
					options = isRegEx;
					isRegEx = false;
				}

				if (!value) {
					return [];
				}

				var delimiterPattern = isRegEx ? new RegExp(delimiter) : delimiter;

				return value.split(delimiterPattern);
			},

			First: function(collection, options) {
				var item;

				if (!collection || collection.length === 0) {
					item = undefined;
				}
				else {
					item = collection[0];
				}

				if (!item) {
					return;
				}

				var text = options.fn(Manchu.createFrame(item));

				if (text) {
					return new Manchu.SafeString(text);
				}

				return item;
			},

			Last: function(collection, options) {
				var item;

				if (!collection || collection.length === 0) {
					item = undefined;
				}
				else {
					item = collection[collection.length - 1];
				}

				if (!item) {
					return;
				}

				var text = options.fn(Manchu.createFrame(item));

				if (text) {
					return new Manchu.SafeString(text);
				}

				return item;
			},

			With: function(options) {
				var context = this;
				var property;

				for (property in options.hash) {
					context[property] = options.hash[property];
				}

				return new Manchu.SafeString(options.fn(Manchu.createFrame(context)));
			},

			Length: function(collection, options) {
				if (!collection || !('length' in collection || typeof(collection.size) === 'function')) {
					return undefined;
				}

				if ('length' in collection) {
					return collection.length;
				}

				return collection.size();
			},

			ConvertNewlines: function(options) {
				var content = options.fn(this).toString();

				if (content.indexOf('\n') >= 0) {
					content = ('<p>' + content + '</p>').replace(/\n/, '</p><p>');
				}

				return new Manchu.SafeString(content);
			},

			Truncate: function(options) {
				var text = options.fn(this).toString();

				var maxLength = Number(options.hash.length) || 40;

				return new Manchu.SafeString(text.substring(0, maxLength))
			}
		};

		Manchu.registerHelper(Manchu.ManchuHelpers);

		Manchu.getPartial = function getPartial(name, sync) {
			if (name in Manchu.partials) {
				return sync ? Manchu.partials[name] : q(Manchu.partials[name]);
			}

			if (sync) {
				throw new Error('Partial ' + name + ' is not registered');
			}

			return getTemplate(name).then(
				function(template) {
					Manchu.registerPartial(name, template);

					return template;
				}
			);
		};

		return Manchu;
	};
}));