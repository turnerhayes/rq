define(
[
	'socket-io'
],
function(
	socketio,
undefined) {
	"use strict";

	function SocketManager() {
		var manager = this;

		Object.defineProperties(
			manager,
			{
				_socket: {
					writable: true
				}
			}
		);

		Object.seal(manager);
	}

	SocketManager.prototype = Object.create(Object.prototype, {
		getSocket: {
			enumerable: true,
			value: function() {
				var manager = this;

				manager._socket = socketio(document.location.origin, { query: { username: 'thayes' } });

				return manager._socket;
			}
		},
		listen: {
			enumerable: true,
			value: function(eventName, callback) {
				var manager = this;

				manager.getSocket().on(eventName, callback);
			}
		}
	});

	var _instance = new SocketManager();

	Object.defineProperties(SocketManager, {
		instance: {
			enumerable: true,
			value: _instance
		}
	});

	return SocketManager;
});
