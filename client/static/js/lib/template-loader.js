define(
[
	'manchu'
],
function(
	Manchu,
undefined) {
	"use strict";

	return {
		load: function(name, req, onload, config) {
			if (config.isBuild) {
				onload(Manchu.getPartial(name, true));
				return;
			}

			return Manchu.getPartial(name, false).then(
				function(partial) {
					onload(partial);
				}
			);
		},

		normalize: function(name, normalize) {
			return name.replace(/\s+\/?(.*)(?:\.manchu)?\s+/, '$1');
		}
	};
});
