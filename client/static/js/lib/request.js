define(
[
	"jquery",
	"underscore",
	"Q"
],
function(
	$,
	_,
	Q,
undefined) {
	"use strict";

	var hasWorker = !!window.Worker;

	var workerUrl = '/static/workers/ajax.js';

	function _spawnWorker() {
		return new Worker(workerUrl);
	}

	var Request = {};

	Object.defineProperties(
		Request,
		{
			send: {
				enumerable: true,
				value: function(url, options) {
					var deferred;
					var worker;

					options = options || {};

					if (hasWorker) {
						deferred = Q.defer();

						worker = _spawnWorker();

						worker.onmessage = function(message) {
							var results = message.data;

							if (results.status >= 400) {
								deferred.reject(results);
							}

							deferred.resolve(results || {});
						};

						worker.postMessage({url: url, options: options});

						return deferred.promise;
					}

					return Q(
						$.ajax(url, options)
					);
				}
			},
			get: {
				enumerable: true,
				value: function(url, options) {
					return Request.send(url, _.extend({}, options, {type: 'GET'}));
				}
			},
			post: {
				enumerable: true,
				value: function(url, options) {
					return Request.send(url, _.extend({}, options, {type: 'POST'}));
				}
			},
			patch: {
				enumerable: true,
				value: function(url, options) {
					return Request.send(url, _.extend({}, options, {type: 'PATCH'}));
				}
			},
			put: {
				enumerable: true,
				value: function(url, options) {
					return Request.send(url, _.extend({}, options, {type: 'PUT'}));
				}
			},
			'delete': {
				enumerable: true,
				value: function(url, options) {
					return Request.send(url, _.extend({}, options, {type: 'DELETE'}));
				}
			},
			head: {
				enumerable: true,
				value: function(url, options) {
					return Request.send(url, _.extend({}, options, {type: 'HEAD'}));
				}
			}
		}
	);

	return Request;
});
