define(
[
	'underscore',
	'Q',
	'backbone',
	'js/apps/base',
	'js/lib/socket-manager',
	'request',
	'js/collections/users'
],
function(
	_,
	Q,
	Backbone,
	BaseApp,
	SocketManager,
	Request,
	UsersCollection,
undefined) {
	"use strict";

	var UsersApp = BaseApp.extend({
		initialize: function() {
			var app = this;

			app._fetched = false;

			app._allUsers = new UsersCollection();
		},

		getUsers: function() {
			var app = this;

			if (app._fetched) {
				return Q(app._allUsers);
			}
			else {
				return Q(app._allUsers.fetch())
					.then(
						function() {
							app._fetched = true;
							
							return app._allUsers;
						}
					);
			}
		},

		getUsersByIds: function(ids) {
			var app = this;

			return app._allUsers.getByIDs(ids);
		},

		getUserByID: function(id) {
			var app = this;

			return app._allUsers.getByID(id);
		},

		getUserByUsername: function(username) {
			var app = this;

			var user = app._allUsers.findWhere({username: username});

			if (!_.isUndefined(user)) {
				return Q(user);
			}

			return Q(app._allUsers.fetch({ data: {username: username } }))
				.then(
					function() {
						return app._allUsers.findWhere({username: username});
					}
				);
		},

		getCurrentUser: function() {
			var app = this;

			if (app._fetchCurrentUserPromise) {
				return app._fetchCurrentUserPromise;
			}

			app._fetchCurrentUserPromise = app._allUsers.getCurrentUser()
				.finally(
					function() {
						app._fetchCurrentUserPromise = undefined;
					}
				);

			return app._fetchCurrentUserPromise;
		},

		addUsers: function(users) {
			var app = this;

			var newUsersCollection = new UsersCollection(users);

			var userIds = _.pluck(users, 'id');

			return Q(
				Backbone.sync('create', newUsersCollection)
			).then(
				function() {
					app._allUsers.add(newUsersCollection);

					return newUsersCollection.toArray();
				}
			);
		}
	});

	var _instance;

	Object.defineProperties(
		UsersApp,
		{
			instance: {
				enumerable: true,
				get: function() {
					if (!_instance) {
						_instance = new UsersApp();
					}

					return _instance;
				}
			}
		}
	);

	return UsersApp;
});
