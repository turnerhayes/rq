define(
[
	'dombot',
	'underscore',
	'Q',
	'js/apps/base',
	'js/apps/users',
	'js/collections/posts'
],
function(
	$,
	_,
	Q,
	BaseApp,
	UsersApp,
	PostsCollection,
undefined) {
	"use strict";

	function preparePosts(posts) {
		var userIDs;

		if (_.size(posts) === 0) {
			return Q([]);
		}

		userIDs = [];

		_.each(
			posts,
			function(post) {
				if (_.isUndefined(post.get('postedBy'))) {
					return;
				}

				if (_.isObject(post.get('postedBy'))) {
					userIDs.push(post.get('postedBy').id);
				}
			}
		);

		return UsersApp.instance.getUsersByIds(userIDs).then(
			function(users) {
				var userMap = _.groupBy(users, 'id');

				_.each(
					posts,
					function(post) {
						if (!_.isUndefined(post.get('postedBy'))) {
							post.set('postedBy', userMap[post.get('postedBy').id]);
						}
					}
				);

				return posts;
			}
		);
	}

	var PostsApp = BaseApp.extend({
		_collectionPromise: undefined,

		initialize: function() {
			var app = this;

			var pagePostData = [];

			var userIds = {};

			$('#page-layout--main [name="serialized-post"]').each(
				function() {
					var post = JSON.parse($(this).val());
					if (post.postedBy) {
						userIds[post.postedBy.id] = true;
					}
					pagePostData.push(post);
				}
			);

			app._posts = new PostsCollection(pagePostData);

			if (pagePostData.length > 0) {
				app._collectionPromise = UsersApp.instance.getUsersByIds(Object.keys(userIds))
					.then(preparePosts);
			}

		},

		getPosts: function() {
			/* jshint newcap: false */
			var app = this;

			if (app._posts.isEmpty()) {
				return Q(app._posts.fetch())
					.then(
						function() {
							return preparePosts(app._posts);
						}
					);
			}

			return Q(app._posts);
		},

		getPostById: function(id, options) {
			var app = this;

			options = options || {};

			var post = app._posts.findWhere({id: id});

			var data = {};

			if (post) {
				return Q(post);
			}

			data.id = id;

			if (!_.isUndefined(options.full)) {
				data.full = options.full;
			}

			if (!_.isUndefined(options.structured)) {
				data.structured = !options.structured;
			}

			return Q(app._posts.fetch({ data: data }))
				.then(
					function() {
						return preparePosts(app._posts);
					}
				)
				.then(
					function() {
						if (options.structured) {
							return app._posts.asStructured({baseId: id});
						}

						return app._posts.findWhere({id: id});
					}
				);
		},

		getLatestPost: function(options) {
			var app = this;

			options = options || {};

			return app._posts.getLatestPost(options).then(
				function(post) {
					return preparePosts([post]);
				}
			);
		},

		_addPost: function(data) {
			/* jshint newcap: false */
			var app = this;

			var post = app._posts.add(data);

			var savedPromise = Q(post.save()).then(
				function(result) {
					return post;
				}
			);

			if (!post.isAnonymous) {
				savedPromise = savedPromise.then(
					function(post) {
						return UsersApp.instance.getCurrentUser().then(
							function(currentUser) {
								post.set('postedBy', currentUser);

								return post;
							}
						);
					}
				);
			}
			
			return savedPromise;
		},

		addQuestion: function(questionData) {
			return this._addPost(questionData);
		},

		addComment: function(commentData) {
			return this._addPost(commentData);
		},

		getPostsByUser: function(user) {
			return this._posts.getPostsByUser(user).then(preparePosts);
		},

		searchPosts: function(options) {
			return this._posts.searchPosts(options).then(
				function(posts) {
					return preparePosts(posts.toArray());
				}
			);
		}
	});

	var _instance;

	Object.defineProperties(
		PostsApp,
		{
			instance: {
				enumerable: true,
				get: function() {
					if (!_instance) {
						_instance = new PostsApp();
					}

					return _instance;
				}
			}
		}
	);

	return PostsApp;
});
