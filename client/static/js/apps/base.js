define(
[
	'underscore',
	'backbone'
],
function(
	_,
	Backbone,
undefined) {
	"use strict";

	function BaseApp() {
		this.initialize.apply(this, arguments);
	}

	BaseApp.prototype = Object.create(Object.prototype, {
		initialize: {
			enumerable: true,
			configurable: true,
			value: function() {
				return this;
			}
		}
	});

	Object.defineProperties(
		BaseApp,
		{
			extend: {
				enumerable: true,
				configurable: true,
				value: function(properties) {
					function ChildApp() {
						BaseApp.apply(this, arguments);
					}

					properties = _.reduce(
						properties,
						function(propsObject, value, key) {
							propsObject[key] = {
								enumerable: true,
								configurable: true,
								writable: true,
								value: value
							};

							return propsObject;
						},
						{}
					);

					ChildApp.prototype = Object.create(BaseApp.prototype, properties);

					return ChildApp;
				}
			}
		}
	);

	_.extend(BaseApp.prototype, Backbone.Events);

	return BaseApp;
});
