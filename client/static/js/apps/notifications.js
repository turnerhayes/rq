define(
[
	'underscore',
	'Q',
	'js/apps/base',
	'js/lib/socket-manager',
	'request',
	'js/collections/notifications'
],
function(
	_,
	Q,
	BaseApp,
	SocketManager,
	Request,
	NotificationsCollection,
undefined) {
	"use strict";

	var NotificationsApp = BaseApp.extend({
		initialize: function() {
			var app = this;

			Object.defineProperties(
				app,
				{
					notifications: {
						enumerable: true,
						value: new NotificationsCollection()
					},

					_fetchingPromise: {
						writable: true
					},

					_initialSyncDone: {
						writable: true,
						value: false
					}
				}
			);

			function triggerCollectionChangedEvent(eventType, data) {
				app.trigger('collection-changed:' + eventType, _.extend({ notifications: app.notifications }, data));
			}

			app.listenToOnce(app.notifications, 'sync', function() {
				app._initialSyncDone = true;
				app.trigger('collection-synced', {notifications: app.notifications});

				app.listenTo(app.notifications, 'add', function(model) {
					triggerCollectionChangedEvent('add', { model: model});
				});

				app.listenTo(app.notifications, 'remove', function(model) {
					triggerCollectionChangedEvent('remove', { model: model});
				});

				app.listenTo(app.notifications, 'change:seen', function(model) {
					triggerCollectionChangedEvent(model.get('seen') ? 'seen' : 'unseen', { model: model});
				});
			});

			app.getUserNotifications()
				.done();
		},

		getUserNotifications: function() {
			/* jshint newcap: false */
			var app = this;

			if (!app._fetchingPromise) {
				app._fetchingPromise = Q(app.notifications.fetch())
					.then(
						function() {
							if (app._fetchingPromise) {
								app._fetchingPromise = undefined;
							}

							return app.notifications;
						}
					);
			}

			return app._fetchingPromise;
		},

		markAllNotificationsSeen: function() {
			var app = this;

			return app.notifications.markAllSeen();
		}
	});

	var _instance;

	Object.defineProperties(
		NotificationsApp,
		{
			instance: {
				enumerable: true,
				get: function() {
					if (!_instance) {
						_instance = new NotificationsApp();
					}

					return _instance;
				}
			}
		}
	);

	return NotificationsApp;
});
