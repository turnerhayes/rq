define(
[
	'underscore',
	'bower_components/backbone/backbone',
	'request'
],
function(
	_,
	Backbone,
	Request,
undefined) {
	"use strict";

	Backbone.syncMethods = Backbone.syncMethods || {};

	Backbone.syncMethods.builtinSync = Backbone.sync;

	var METHOD_VERB_MAP = {
		create: "POST",
		read: "GET",
		update: "PUT",
		patch: "PATCH",
		delete: "DELETE"
	};

	Backbone.sync = Backbone.syncMethods.request = function(method, model, options) {
		options = options || {};

		var url = _.isFunction(options.url) ? options.url() : options.url;

		if (_.isUndefined(url)) {
			url = _.isFunction(model.url) ? model.url() : model.url;
		}

		var data = options.data || {};

		if (method !== 'read') {
			if (_.isEmpty(data)) {
				data = model.toJSON();
			}
			else {
				_.extend(data, model.toJSON());
			}
		}

		model.trigger('request');

		return Request.send(url, {
			type: METHOD_VERB_MAP[method],
			data: data
		}).then(
			function(response) {
				if (options.success) {
					options.success(response.responseObject);
				}

				return response;
			},
			function(error) {
				if (options.error) {
					options.error(error);
				}

				return error;
			}
		);
	};

	return Backbone;
});