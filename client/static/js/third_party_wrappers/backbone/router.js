define(
[
	'backbone',
	'jquery',
	'js/lib/view-starter'
],
function(
	Backbone,
	$,
	viewStarter,
undefined) {
	"use strict";

	return Backbone.Router.extend({
		initialize: function() {
			var router = this;

			var $body = $('body');

			router.sections = {
				$header: $body.find('.page-layout--header'),
				$middlePanel: $body.find('.page-layout--middle-panel'),
				$leftPanel: $body.find('.page-layout--left-panel'),
				$rightPanel: $body.find('.page-layout--right-panel'),
				$footer: $body.find('.page-layout--footer'),
			};
		},

		updateMiddlePanel: function updateMiddlePanel(content) {
			var router = this;

			router.sections.$middlePanel.html(content);
			viewStarter({
				$el: router.sections.$middlePanel
			});
		},

		updateLeftPanel: function updateLeftPanel(content) {
			var router = this;

			router.sections.$leftPanel.html(content);
			viewStarter({
				$el: router.sections.$leftPanel
			});
		},

		updateRightPanel: function updateRightPanel(content) {
			var router = this;

			router.sections.$rightPanel.html(content);
			viewStarter({
				$el: router.sections.$rightPanel
			});
		},

		updateHeader: function updateHeader(content) {
			var router = this;

			router.sections.$header.html(content);
			viewStarter({
				$el: router.sections.$header
			});
		},

		updateFooter: function updateFooter(content) {
			var router = this;

			router.sections.$footer.html(content);
			viewStarter({
				$el: router.sections.$footer
			});
		}
	});
});
