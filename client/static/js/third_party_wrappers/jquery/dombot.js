/* jshint browser: true */

define(
[
	'jquery',
	'underscore',
	'request'
],
function(
	jQuery,
	_,
	Request,
undefined) {
	"use strict";

	function $() {
		return jQuery.apply(this, arguments);
	}

	$.prototype = _.clone(jQuery.prototype);

	_.extend($, jQuery);

	$.sendForm = function($form, options) {
		options = options || {};

		if (_.isUndefined(options.dataType)) {
			options.dataType = 'application/json';
		}

		var headers = options.headers;

		if (options.accepts) {
			headers = headers || {};

			headers.accepts = options.accepts;
		}

		var data = _.reduce(
			$form.serializeArray(),
			function(accumulator, pair) {
				accumulator[pair.name] = pair.value;

				return accumulator;
			},
			{}
		);

		return Request.send($form.attr('action'), {
			headers: headers,
			contentType: $form.attr('enctype'),
			data: data,
			dataType: options.dataType,
			type: $form.attr('method')
		});
	};

	return $;
});
