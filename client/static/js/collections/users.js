define(
[
	'Q',
	'underscore',
	'wrappers/backbone/collection',
	'js/models/user'
],
function(
	q,
	_,
	Collection,
	UserModel,
undefined) {
	"use strict";

	return Collection.extend({
		model: UserModel,
		url: "/users",

		getCurrentUser: function() {
			var collection = this;

			var currentUser = collection.findWhere({isCurrentUser: true});

			if (!_.isUndefined(currentUser)) {
				return q(currentUser);
			}

			return q(
				collection.fetch({
					url: '/user'
				}).then(
					function(response) {
						return collection.findWhere({id: response.responseObject.id});
					}
				)
			);
		},

		getByID: function(id) {
			var collection = this;

			var user = collection.findWhere({id: id});

			if (user) {
				return q(user);
			}

			collection.add({id: id});

			return q(
				collection.fetch()
			).then(
				function() {
					return collection.findWhere({id: id});
				}
			);
		},

		getByIDs: function(ids) {
			var collection = this;

			if (!ids) {
				return [];
			}

			ids = _.unique(ids);

			if (_.size(ids) === 0) {
				return [];
			}

			var users = collection.filter(function(user) {
				var index = _.indexOf(ids, user.id);

				if (index >= 0) {
					ids.splice(index, 1);
					return true;
				}

				return false;
			});

			if (ids.length === 0) {
				return q(users);
			}

			return q(collection.fetch({ data: {ids: ids } }))
				.then(
					function() {
						return _.union(
							users,
							collection.filter(
								function(model) {
									return _.contains(ids, model.id);
								}
							)
						);
					}
				);
		}
	});
});
