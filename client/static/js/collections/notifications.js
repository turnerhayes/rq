define(
[
	'wrappers/backbone/collection',
	'js/models/notification',
	'request'
],
function(
	Collection,
	NotificationModel,
	Request,
undefined) {
	"use strict";

	return Collection.extend({
		model: NotificationModel,
		url: '/notifications',

		initialize: function() {
			var collection = this;

			Object.defineProperties(
				collection,
				{
					seen: {
						enumerable: true,
						get: function() {
							return collection.where({seen: true});
						}
					},
					unseen: {
						enumerable: true,
						get: function() {
							return collection.where({seen: false});
						}
					}
				}
			);

			Collection.prototype.initialize.apply(collection, arguments);
		},

		markAllSeen: function() {
			var collection = this;

			var changed = [];

			collection.forEach(function(notification) {
				if (!notification.get('seen')) {
					notification.set('seen', true);

					changed.push(notification.get('id'));
				}
			});

			return Request.patch('/notifications/mark-seen', {
				data: {
					ids: changed
				}
			});
		}
	});
});
