define(
[
	'wrappers/backbone/collection',
	'Q',
	'js/models/post'
],
function(
	Collection,
	Q,
	PostModel,
undefined) {
	"use strict";

	var PostsCollection = Collection.extend({
		model: PostModel,
		url: "/posts",

		initialize: function(options) {
			var collection = this;

			options = options || {};

			if (options.facebook) {
				collection.url = "/posts/facebook";
			}

			Collection.prototype.initialize.apply(collection, arguments);
		},

		comparator: function(a, b) {
			if (a.get('postedOn') === b.get('postedOn')) {
				return 0;
			}

			if (_.isUndefined(a.get('postedOn'))) {
				return -1;
			}

			if (_.isUndefined(b.get('postedOn'))) {
				return 1;
			}

			return a.get('postedOn') > b.get('postedOn') ? 1 : -1;
		},

		getLatestPost: function(options) {
			var collection = this;

			options = options || {};

			var data = {};

			if (!_.isUndefined(options.full)) {
				data.full = options.full;
			}

			if (!_.isUndefined(options.structured)) {
				data.structured = !options.structured;
			}

			return Q(collection.fetch({url: collection.url + '/latest', data: data }))
				.then(
					function(response) {
						// There's only one base post (the latest)
						var postId = response.responseObject[0].id;

						if (options.structured) {
							return collection.asStructured({baseId: postId});
						}

						return collection.findWhere({id: postId});
					}
				);
		},

		asStructured: function(options) {
			var collection = this;

			options = options || {};

			var baseId = options.baseId;

			var structuredSkeleton;

			var data = { structured: false };

			var structured = [];

			if (baseId) {
				data.id = baseId;
			}

			function _skeletonToStructure(basePost, skeleton) {
				var children = basePost.get('children');

				if (_.isUndefined(children)) {
					children = [];
					basePost.set('children', children);
				}

				children.push.apply(
					children,
					_.filter(
						_.map(
							skeleton,
							function(childSkeleton, id) {
								return _skeletonToStructure(collection.findWhere({id: +id}), childSkeleton);
							}
						),
						function(post) {
							return !_.contains(children, post);
						}
					)
				);

				basePost.set('children', children.sort(_.bind(collection.comparator, collection)), {silent: true});

				return basePost;
			}

			function _convertSkeletonToStructure() {
				_.each(
					structuredSkeleton,
					function(subSkeleton, rootId) {
						structured.push(_skeletonToStructure(collection.findWhere({id: +rootId}), subSkeleton));
					}
				);

				if (!_.isUndefined(baseId)) {
					return structured[0];
				}

				return structured;
			}

			function _assembleStructure() {
				structuredSkeleton = collection.reduce(
					function(obj, post) {
						var pathItem;

						if (
							!_.isUndefined(baseId) &&
							(
								post.get('id') !== baseId &&
								(
									_.isNull(post.get('path')) ||
									post.get('path').indexOf(',' + baseId + ',') < 0
								)
							)
						) {
							return obj;
						}

						if (_.isNull(post.get('path'))) {
							obj[post.get('id')] = {};
						}
						else {
							pathItem = obj;

							_.each(
								post.get('path').replace(/^,|,$/g, '').split(','),
								function(id) {
									id = parseInt(id, 10);

									if (!(id in pathItem)) {
										pathItem[id] = {};
									}

									pathItem = pathItem[id];
								}
							);

							pathItem[post.get('id')] = {};
						}

						return obj;
					},
					{}
				);

				return _convertSkeletonToStructure();
			}


			return Q(collection.fetch({ data: data }))
				.then(
					_assembleStructure
				);
		},

		getPostsByUser: function(user) {
			var collection = this;

			return Q(
				collection.fetch({
					data: {
						postedBy: user.get('id')
					}
				})
			).then(
				function() {
					return collection.filter(
						function(post) {
							var userID;

							if (_.isUndefined(post.get('postedBy'))) {
								return false;
							}

							userID = _.isObject(post.get('postedBy')) ?
								post.get('postedBy').id :
								post.get('postedBy').get('id');

							return userID === user.get('id');
						}
					);
				}
			);
		},

		searchPosts: function(options) {
			var collection = this;

			return Q(
				collection.fetch({
					url: '/search',
					data: _.extend({}, options, {
						search: true
					})
				})
			).then(
				function(response) {
					return new PostsCollection(response.responseObject);
				}
			);
		}
	});

	return PostsCollection;
});
