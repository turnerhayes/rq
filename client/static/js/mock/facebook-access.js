define(
[
	'underscore',
	'Q',
	'js/models/post',
	'js/models/user',
	'js/apps/users',
	'js/lib/facebook-access',
	'text!js/mock/facebook-posts.json'
],
function(
	_,
	q,
	PostModel,
	UserModel,
	UsersApp,
	MainFacebookAccess,
	facebookPostsText,
undefined) {
	"use strict";

	var Facebook = MainFacebookAccess;

	var currentUserPromise = UsersApp.instance.getCurrentUser();

	var facebookPosts = JSON.parse(facebookPostsText);

	Object.defineProperties(
		Facebook,
		{
			login: {
				enumerable: true,
				value: function(options) {
					options = options || {};

					return q(true);
				}
			},

			getPosts: {
				enumerable: true,
				value: function(options) {
					options = options || {};

					return q(
						Facebook._convertResponseToPosts(facebookPosts)
					);
				}
			},

			getUsers: {
				enumerable: true,
				value: function(options) {
					options = options || {};

					return currentUserPromise.then(
						function(currentUser) {
							return [currentUser];
						}
					);
				}
			}
		}
	);

	return Facebook;
});