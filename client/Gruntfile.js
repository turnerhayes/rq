/* jshint node:true */

"use strict";

var fs       = require('fs');
var path     = require('path');
var readdir  = require('recursive-readdir');
var q        = require('q');
var _        = require('lodash');
var rimraf   = require('rimraf');
var mkdirp   = require('mkdirp');
var Manchu   = require('../server/lib/manchu');

var staticDir = path.join(__dirname, 'static');

var manchuFilePathRegex = /\.manchu$/;
var templatesDirectory = path.join(staticDir, 'templates');

var outputDirectory = path.join(staticDir, 'dist');

var templatesDestinationPath = path.join(outputDirectory, 'templates');

function _precompileTemplate(file) {
	var deferred = q.defer();

	fs.readFile(
		file,
		{
			encoding: "utf8"
		},
		function(err, templateText) {
			if (err) {
				deferred.reject(err);
				return;
			}
			
			deferred.resolve(
				Manchu.precompile(
					templateText,
					{
						knownHelpers: _.keys(Manchu.ManchuHelpers)
					}
				)
			);
		}
	);

	return deferred.promise;
}

/**
 * Precompiles all templates so that they can be loaded on the client
 * without compilation.
 *
 * @returns {Object} map of template paths (relative to the base template
 *    directory) to precompiled template specs
 */
function precompileTemplates() {
	var deferred = q.defer();

	var templates = {};

	readdir(templatesDirectory, function(err, files) {
		if (err) {
			deferred.reject(err);
			return;
		}

		q.all(
			_.map(
				files,
				function(file) {
					var relativePath;

					if (!manchuFilePathRegex.test(file)) {
						return;
					}

					relativePath = path.relative(templatesDirectory, file);

					return _precompileTemplate(file).then(
						function(template) {
							templates[relativePath] = template;
						}
					);
				}
			)
		).then(
			function() {
				deferred.resolve(templates);
			},
			function(err) {
				deferred.reject(err);
			}
		);
	});


	return deferred.promise;
}

function generateTemplatePartials(done) {
	var deferred = q.defer();

	var templates = [];

	readdir(templatesDirectory, function(err, files) {
		var dependencyList, functionArgList, assignmentsList, text;

		if (err) {
			deferred.reject();
			return;
		}

		_.each(
			files,
			function(filePath) {
				var relativePath;
				var relativePathNoExt;

				if (!manchuFilePathRegex.test(filePath)) {
					// Not a .manchu file--ignore
					return;
				}

				relativePath = path.relative(templatesDirectory, filePath);

				templates.push(relativePath);
			}
		);

		_.each(
			templates,
			function(relativePath, index) {
				var variableName = 'template' + index;
				var templateName = relativePath.replace(manchuFilePathRegex, '');

				if (!dependencyList) {
					dependencyList = '';
				}
				else {
					dependencyList += ",\n\t";
				}
				
				dependencyList += '"text!dist/templates/' + templateName + '.js"';

				if (!functionArgList) {
					functionArgList = '';
				}
				else {
					functionArgList += ",\n\t";
				}

				functionArgList += variableName;

				if (!assignmentsList) {
					assignmentsList = '';
				}
				else {
					assignmentsList += ',\n\t\t';
				}

				assignmentsList += "'" + templateName + "': " + variableName;
			}
		);

		text = "define(\n[\n\t" + dependencyList + "],\nfunction(\n\t" +
			functionArgList + ",\nundefined) {\n'use strict';\n" +
			"\treturn {\n\t\t" +
			assignmentsList +
			"\n\t};\n\n});";

		deferred.resolve(text);
	});

	return deferred.promise;
}

module.exports = function(grunt) {
	function writePrecompiledTemplates(templates) {
		return q.all(
			_.map(
				templates,
				function(compiled, relativePath) {
					var deferred = q.defer();
					var destinationPath = path.join(templatesDestinationPath, relativePath)
						.replace(manchuFilePathRegex, '.js');
					var destinationDir = path.dirname(destinationPath);

					grunt.verbose.writeln('creating directory at ' + destinationDir);
					
					mkdirp(
						destinationDir,
						function(err) {
							if (err) {
								throw err;
							}

							grunt.verbose.writeln('writing file at ' + destinationPath);

							fs.writeFile(
								destinationPath,
								compiled.toString(),
								{
									encoding: "utf8"
								},
								function(err) {
									if (err) {
										deferred.reject(err);
									}

									deferred.resolve();
								}
							);
						}
					);


					return deferred.promise;
				}
			)
		);
	}


	grunt.registerTask(
		'generate-template-partials', 
		'Generates the template-partials file that the client uses to get Manchu partials.',
		function() {
			var templatePartialsPath = path.join(staticDir, 'js', 'lib', 'template-partials.js');

			var done = this.async();

			generateTemplatePartials().done(
				function(fileContents) {
					fs.writeFile(
						templatePartialsPath,
						fileContents,
						{
							encoding: "utf8"
						},
						function(err) {
							if (err) {
								grunt.log.error(err);
								done(false);
								return;
							}

							grunt.log.ok("Wrote template partials to " + templatePartialsPath + ".");
							done();
						}
					);
				},
				function(err) {
					grunt.log.error(err);
					done(false);
				}
			);
		}
	);

	grunt.registerTask(
		'precompile-templates',
		'Precompiles Manchu templates into Javascript files.',
		function() {
			var done = this.async();

			precompileTemplates().then(
				function(templates) {
					var deferred = q.defer();

					grunt.verbose.writeln('Removing existing compiled templates');
					rimraf(
						templatesDestinationPath,
						function(err) {
							if (err) {
								deferred.reject(err);
								return;
							}

							writePrecompiledTemplates(templates).done(
								function() {
									deferred.resolve();
								},
								function(err) {
									deferred.reject(err);
								}
							);
						}
					);

					return deferred.promise;
				}
			).done(
				function() {
					done();
				},
				function(err) {
					grunt.log.error(err);

					done(false);
				}
			);
		}
	);

	require('load-grunt-config')(grunt, {
		data: {
			"client_directory": __dirname,
			"static_directory": staticDir,
			"styles_directory": path.join(staticDir, 'styles'),
			"scripts_directory": path.join(staticDir, 'js'),
			"templates_directory": path.join(staticDir, 'templates'),
			"output_directory": outputDirectory
		}
	});
};