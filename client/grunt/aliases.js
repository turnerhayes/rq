"use strict";

var appConfig = require('../../server/config/app.json');

module.exports = {
	"requirejs:development": ["requirejs:development-almond", "requirejs:development-requirejs"],
	"requirejs:production" : ["requirejs:production-almond", "requirejs:production-requirejs"],
	"scripts"              : ["jshint", "requirejs:" + appConfig.environment],
	"styles"               : ["less:" + appConfig.environment],
	"templates"            : ["precompile-templates", "generate-template-partials"],
	"static"               : ["concurrent:static-concurrent"],
	"watchstatic"          : ["concurrent:watchstatic-concurrent"]
};
