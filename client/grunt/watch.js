module.exports = {
	"styles": {
		"files": ["<%= styles_directory %>/**/*.{less,css}", "<%= client_directory %>/grunt/{less,autoprefixer}.js"],
		"tasks": ["styles"]
	},
	"scripts": {
		"files": ["<%= scripts_directory %>/**/*.js", "<%= client_directory %>/grunt/{requirejs,jshint}.js"],
		"tasks": ["scripts"]
	},
	"templates": {
		"files": ["<%= templates_directory %>/**/*.manchu"],
		"tasks": ["templates"]
	}
};
