module.exports = {
	"static-concurrent": {
		"options": {
			"logConcurrentOutput": true
		},
		"tasks": ["scripts", "styles", "templates"]
	},
	"watchstatic-concurrent": {
		"options": {
			"logConcurrentOutput": true
		},
		"tasks": ["watch:scripts", "watch:styles", "watch:templates"]
	}
};
