"use strict";

var _                    = require('lodash');
var LessAutoprefixPlugin = require('less-plugin-autoprefix');
var LessCleanPlugin      = require('less-plugin-clean-css');

var siteConfig           = require('../../server/config/app.json');

var staticDomain         = "//" + siteConfig.static.host;

if (siteConfig.static.port) {
	staticDomain += ":" + siteConfig.static.port;
}

if (siteConfig.static.path) {
	staticDomain += "/" + siteConfig.static.path;
}

var options = {
	"sourceMap": true,
	"sourceMapFilename": "<%= output_directory %>/css/build.css.map",
	"sourceMapURL": "build.css.map",
	"sourceMapBasepath": "<%= styles_directory %>",
	"plugins": [
		new LessAutoprefixPlugin({
			browsers: [
				"last 2 versions",
				"ios >= 7",
				"ie >= 10"
			]
		}),
		new LessCleanPlugin({
			advanced: true
		})
	],
	"modifyVars": {
		"static-path": '"' + staticDomain + '"'
	}
};

var src = ["<%= styles_directory %>/main.less"];
var dest = "<%= output_directory %>/css/build.css";

module.exports = {
	"development": {
		"options": _.extend({}, options, {
			"sourceMap": true,
			"sourceMapFilename": "<%= output_directory %>/css/build.css.map",
			"sourceMapURL": "build.css.map",
			"sourceMapBasepath": "<%= styles_directory %>"
		}),
		"src": src,
		"dest": dest
	},
	"production": {
		"options": options,
		"src"    : src,
		"dest"   : dest
	}
};
