"use strict";

var _               = require('lodash');
var requirejsConfig = require('../static/js/config/requirejs');
var readdir         = require('recursive-readdir-sync');
var path            = require('path');

var remoteDependencyViews = ['js/views/admin/facebook-sync/dashboard'];

var files = readdir(path.resolve(__dirname, '../static/js/views'));

var viewNames = _.map(
	files,
	function(file) {
		return path.relative(path.join(__dirname, '..', 'static'), file).replace(/\.js$/, '');
	}
);

viewNames = _.filter(
	viewNames,
	function(view) {
		return !_.contains(remoteDependencyViews, view);
	}
);

var config = _.extend({}, requirejsConfig, {
	"baseUrl"                : "<%= static_directory %>",
	"out"                    : "<%= output_directory %>/js/build.js",
	"name"                   : "js/main",
	"include"                : viewNames,
	"optimize"               : "uglify2"
});

var configAlmond = _.extend({}, config, {
	"almond": true
});

var configRequirejs = _.extend({}, config, {
	"out": "<%= output_directory %>/js/build-requirejs.js",
	"include": ["requireLib"].concat(remoteDependencyViews)
});

configRequirejs.paths.requireLib = "bower_components/requirejs/require";

module.exports = {
	"development-almond": {
		"options": _.extend(
			{},
			configAlmond,
			{
				"optimize": "none",
				"generateSourceMaps": true,
				"preserveLicenseComments": false
			}
		),
	},
	"development-requirejs": {
		"options": _.extend(
			{},
			configRequirejs,
			{
				"optimize": "none",
				"generateSourceMaps": true,
				"preserveLicenseComments": false
			}
		),
	},
	"production-almond": {
		"options": configAlmond
	},
	"production-requirejs": {
		"options": _.extend({}, configRequirejs, {
			"include": remoteDependencyViews.concat(viewNames)
		})
	}
};
